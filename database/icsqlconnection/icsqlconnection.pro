#-------------------------------------------------
#
# Project created by QtCreator 2014-01-23T09:16:22
#
#-------------------------------------------------
include(../common.pri)

QT       += sql
QT -= gui
TARGET = icsqlconnection
TEMPLATE = lib
CONFIG += plugin

DEFINES += ICSQLCONNECTION_LIBRARY

SOURCES += icsqlconnection.cpp

HEADERS += $$INCLUDEDIR/icsqlconnection.h\
           $$INCLUDEDIR/icsqlconnection_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -L$$PLATFORM_DIR/bin -licsettings -licobject -livkplatform -licdbconnection -liccontainers
