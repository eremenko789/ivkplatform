#-------------------------------------------------
#
# Project created by QtCreator 2014-01-27T15:27:27
#
#-------------------------------------------------
include(../common.pri)

QT       -= gui

TARGET = icdbserver
TEMPLATE = lib
CONFIG += plugin

DEFINES += ICDBSERVER_LIBRARY

SOURCES += icdbserver.cpp

HEADERS += icdbserver.h \
           icdbserver_global.h

INCLUDEPATH += \
               $$PLATFORM_DIR/ictmplugininterface/include \
               $$PLATFORM_DIR/database/include \
               $$PLATFORM_DIR/iccontainers/include

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS+= -L$$DESTDIR -licobject -lictmplugininterface -liccontainers -licdbrequests \
       -licdbconnection -livkplatform -licsettings -licdatadriverinterface

DESTDIR = $$PLATFORM_DIR/bin/plugins
