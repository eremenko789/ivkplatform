#include <icdatadriverinterface.h>

ICDataDriverInterface::ICDataDriverInterface(QObject *parent):ICObject(parent)
{
    connect(this, &ICDataDriverInterface::command, this, &ICDataDriverInterface::onCommand, Qt::QueuedConnection);
    connect(this, &ICDataDriverInterface::needStart, this, &ICDataDriverInterface::start, Qt::QueuedConnection);
}

ICRETCODE ICDataDriverInterface::init(const QString& prefix, ICDbConnPoolManager* poolManager)
{
    m_poolManager = poolManager;
    return ICObject::init(prefix);
}

COMMAND_TYPE_LIST ICDataDriverInterface::commandTypes()
{
    return m_pluginQueryHandlerByType.keys();
}

void ICDataDriverInterface::registerCommandHandler(CommandType ct, handlerFunc f)
{
    m_pluginQueryHandlerByType.insert(ct, f);
}

void ICDataDriverInterface::onCommand(ICCommand cmd)
{
    CommandType t(cmd);
    if(m_pluginQueryHandlerByType.contains(t))
    {
        handlerFunc f = m_pluginQueryHandlerByType[t];
        (this->*f)(cmd);
        if(cmd.isValid() && cmd.status != COMMAND_STATUS_REQUEST)
            emit done(cmd);
    }
}

ICDbConnection* ICDataDriverInterface::connection(const ICDbRequest *request)
{
    QString err;
    auto ret = m_poolManager->connection(request!=nullptr?request->databaseName():"", &err);
    if(ret==nullptr)
        reportError(err);
    return ret;
}

void ICDataDriverInterface::releaseConnection(ICDbConnection *con)
{
    m_poolManager->releaseConnection(con);
}

ICDbInsertRequest& ICDataDriverInterface::merge(ICDbInsertRequest& ins, const ICDbResponse& resp)
{
    if(ins.inserts.records.count() != resp.records.count())
        return ins;
    for(int i=0; i< ins.inserts.records.count(); i++)
        for(auto nm: resp.records[i]->names())
            ins.inserts.records[i]->setValue(nm, resp.records[i]->value(nm));
    return ins;
}
