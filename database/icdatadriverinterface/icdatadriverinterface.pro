#-------------------------------------------------
#
# Project created by QtCreator 2014-01-27T15:27:27
#
#-------------------------------------------------
include(../common.pri)

QT       -= gui

TARGET = icdatadriverinterface
TEMPLATE = lib
CONFIG += plugin

DEFINES += DDI_LIBRARY

SOURCES += icdatadriverinterface.cpp

HEADERS += $$INCLUDEDIR/icdatadriverinterface.h \
           $$INCLUDEDIR/icdatadriverinterface_global.h

INCLUDEPATH += \
               $$PLATFORM_DIR/ictmplugininterface/include \
               $$PLATFORM_DIR/iccontainers/include

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS+= -L$$DESTDIR -licobject -lictmplugininterface -liccontainers -licdbrequests \
       -licdbconnection -livkplatform -licsettings
