TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS += \
    icdbrequests \
    icdbconnection \
    icsqlconnection \
    icdatadriverinterface \
    icgenericdatadriver \
    icdbserver
