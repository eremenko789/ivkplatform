#include <icdbexecrequest.h>

void ICDbExecRequest::serialize(QDataStream &output) const
{
    ICDbRequest::serialize(output);
    output<<m_stmt;
}

void ICDbExecRequest::deserialize(QDataStream &input)
{
    ICDbRequest::deserialize(input);
    input>>m_stmt;
}

