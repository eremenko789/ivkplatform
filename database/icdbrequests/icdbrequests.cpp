#include <icdbrequest.h>

void ICDbRequest::serialize(QDataStream &output) const
{
    output << databaseName() << typeName() << genericContainer();
}

void ICDbRequest::deserialize(QDataStream &input)
{
    input >> m_databaseName >> m_typeName >> m_genericContainer;
}
