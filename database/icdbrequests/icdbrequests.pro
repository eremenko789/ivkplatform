include(../common.pri)

TARGET = icdbrequests
TEMPLATE = lib
CONFIG += plugin
DEFINES += ICDBREQUESTS_LIBRARY
QT -= gui
HEADERS += \
    $$INCLUDEDIR/icdbrequest.h \
    $$INCLUDEDIR/icdbselectrequest.h \
    $$INCLUDEDIR/icdbconditionrequest.h \
    $$INCLUDEDIR/icdbinsertrequest.h \
    $$INCLUDEDIR/icdbupdaterequest.h \
    $$INCLUDEDIR/icdbremoverequest.h \
    $$INCLUDEDIR/icdbrequestgroup.h \
    $$INCLUDEDIR/icdbresponse.h \
    $$INCLUDEDIR/iccondition.h \
    $$INCLUDEDIR/icconditionset.h \
    $$INCLUDEDIR/icdballrequests.h \
    $$INCLUDEDIR/icaggregation.h \
    $$INCLUDEDIR/icdbexecrequest.h

SOURCES += \
    icdbrequests.cpp \
    icdbinsertrequest.cpp \
    icdbupdaterequest.cpp \
    iccondition.cpp \
    icdbconditionrequest.cpp \
    icdbselectrequest.cpp \
    icdbremoverequest.cpp \
    icaggregation.cpp \
    icdbexecrequest.cpp
#    icdbrequestgroup.cpp

LIBS += -L$$DESTDIR -liccontainers
