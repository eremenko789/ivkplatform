#include <icdbconditionrequest.h>

ICDbConditionRequest::ICDbConditionRequest(const QString& dbname)
    : ICDbRequest(dbname)
{
}

ICDbConditionRequest::ICDbConditionRequest(const QString& dbname, const ICConditionSet& val)
    : ICDbRequest(dbname)
{
    m_conditions = val;
}

ICDbConditionRequest::~ICDbConditionRequest()
{
}

void ICDbConditionRequest::serialize(QDataStream &output) const
{
    ICDbRequest::serialize(output);
    output << m_conditions<<m_cascade;
}

void ICDbConditionRequest::deserialize(QDataStream &input)
{
    ICDbRequest::deserialize(input);
    input >> m_conditions>>m_cascade;
}
