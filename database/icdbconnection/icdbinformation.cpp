#include <icdbinformation.h>
#include <iclocalsettings.h>

/*extern*/ QHash<QString,ICDbInformation*> ICDbInformation::m_databases;

#ifdef IC_MTDB
    QMutex g_dbInfoMutex;
#endif

ICDbInformation::ICDbInformation(const QString& dbname)
{
    m_settingsGroup = QString("dbconnection:%1/").arg(dbname);
    ICLocalSettings settings;
    m_serverHostname = settings.value(m_settingsGroup+"server/hostname",IC_DEFAULT_DBHOSTNAME).toString();
    m_serverPort = settings.value(m_settingsGroup+"server/port",IC_DEFAULT_DBPORT).toInt();
    m_username = settings.value(m_settingsGroup+"username",IC_DEFAULT_DBUSERNAME).toString();
    m_password = settings.value(m_settingsGroup+"password",IC_DEFAULT_DBPASSWORD).toString();
    m_driver = settings.value(m_settingsGroup+"driver",IC_DEFAULT_DBDRIVER).toString();
}

ICDbInformation::~ICDbInformation()
{
    qDeleteAll(m_types);
}

bool ICDbInformation::containsType(const QString &typeName) const
{
    return m_types.contains(typeName);
}

void ICDbInformation::addType(const QString &typeName, const QStringList &fields)
{
    IC_LOCK;
    m_types[typeName] = new ICDbTypeInformation(fields);
}

ICDbTypeInformation* ICDbInformation::typeInfo(const QString &typeName) const
{
    return m_types.value(typeName);
}

ICDbInformation* ICDbInformation::instance(const QString &dbname)
{
#ifdef IC_MTDB
    QMutexLocker locker(&g_dbInfoMutex);
    Q_UNUSED(locker)
#endif
    ICDbInformation* result = nullptr;
    if(m_databases.contains(dbname))
        result = m_databases.value(dbname);
    else
    {
        result = new ICDbInformation(dbname);
        m_databases[dbname] = result;
    }
    return result;
}
