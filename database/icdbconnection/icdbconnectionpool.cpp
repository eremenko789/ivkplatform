#include <icdbconnectionpool.h>
#include <icsettings.h>
#include <QPluginLoader>
#include <QDebug>
#include <QThread>

#define DEFAULT_POOL_MAXSIZE    1       ///< максимальный размер пула подключений по-умолчанию
#define DEFAULT_WAIT_TIMEOUT    30000    ///< время ожидания свободного подключения по-умолчанию
#define DEFAULT_BACKEND         "icsqlconnection.dll"

ICDbConnectionPool::ICDbConnectionPool(const QString &dbname, QObject* parent)
    :ICObject(parent),
      m_dbname(dbname)
{

}

ICDbConnectionPool::~ICDbConnectionPool()
{
    auto cons = m_connections.keys();
    for(auto con:cons)
        con->close();
    qDeleteAll(cons);
    int cnt = m_connections.count();
    m_connections.clear();
    m_semaphore.release(cnt);
}

ICRETCODE ICDbConnectionPool::init(const QString& pref)
{
    auto ret = ICObject::init(pref);
    m_maxSize = setting("maxPoolSize", DEFAULT_POOL_MAXSIZE).toInt();
    m_waitTimeout = setting("waitTimeout", DEFAULT_WAIT_TIMEOUT).toInt();
    m_backend = setting(QString("dbconnection:%1/backend").arg(m_dbname), DEFAULT_BACKEND).toString();
    return ret;
}

ICDbConnection* ICDbConnectionPool::get()
{
    if(!m_semaphore.tryAcquire())
    {
        {
            QMutexLocker lock(&m_mutex);
            Q_UNUSED(lock);
            if(m_connections.count()<m_maxSize)
            {
                //если пул подключений можно нарастить, пытаемся создать новое подключение
                if(m_backend.isEmpty())
                    return nullptr;
                //загрузка бэкенда
                QPluginLoader pl(m_backend);
//                bool pluginWasLoaded = pl.isLoaded();
                ICDbConnection* connection = qobject_cast<ICDbConnection*>(pl.instance());
                if(connection == nullptr)
                {
                    reportError(trUtf8("Не удалось загрузить модуль бэкенда:")+m_backend);
                    return nullptr;
                }
//                if(pluginWasLoaded)
                connection = qobject_cast<ICDbConnection*>(connection->metaObject()->newInstance());
                connection->setDatabaseName(m_dbname);
                auto conName = QString("%1%2").arg(m_dbname).arg(m_connections.count());
                connection->setConnectionName(conName);
                if(!connection->open())
                {
                    reportError(trUtf8("Не могу открыть соединение с базой."));
                    pl.unload();
                    return nullptr;
                }
                //занять подключение
                m_connections[connection] = false;
                return connection;
            }
        }
        //ждать, пока не освободится
        if(!m_semaphore.tryAcquire(1, m_waitTimeout))
        {
            reportError(trUtf8("Не удалось дождаться свободного подключения с базой."));
            return nullptr;
        }
    }
    QMutexLocker lock(&m_mutex);
    Q_UNUSED(lock);
    auto cons = m_connections.keys();
    for(ICDbConnection* c:cons)
    {
        if(m_connections.value(c))
        {
            m_connections[c] = false;
            return c;
        }
    }
    return nullptr;
}

void ICDbConnectionPool::release(ICDbConnection *con)
{
    QMutexLocker lock(&m_mutex);
    if(!m_connections.contains(con))
    {
        issueWarning(__FILE__ + __LINE__ + trUtf8("Cоединение для освобождения не найдено."));
        return;
    }
    m_connections[con] = true;
    m_semaphore.release();
}
