#-------------------------------------------------
#
# Project created by QtCreator 2014-03-31T10:19:28
#
#-------------------------------------------------
include(../common.pri)

QT       -= gui

CONFIG += plugin

TARGET = icgenericdatadriver
TEMPLATE = lib

DEFINES += ICGENERICDATADRIVER_LIBRARY

SOURCES += icgenericdatadriver.cpp

HEADERS += ../include/icgenericdatadriver.h\
        ../include/icgenericdatadriver_global.h

INCLUDEPATH += \
               $$PLATFORM_DIR/plugins/icdbserver/include \
               $$PLATFORM_DIR/ictmplugininterface/include

LIBS += -L$$DESTDIR -licobject -lictmplugininterface -licdatadriverinterface \
        -licdbconnection -licdbrequests -liccontainers

unix {
    target.path = /usr/lib
    INSTALLS += target
}
