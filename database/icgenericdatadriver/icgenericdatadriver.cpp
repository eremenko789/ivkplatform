#include "icgenericdatadriver.h"


ICGenericDataDriver::ICGenericDataDriver(QObject *parent):ICDataDriverInterface(parent)
{
}

ICRETCODE ICGenericDataDriver::init(const QString& prefix, ICDbConnPoolManager *poolManager)
{
    auto ret = ICDataDriverInterface::init(prefix, poolManager);
    registerCommandHandler({COMMAND_TYPE_INSERT, "icdbrequest"}, (handlerFunc)&ICGenericDataDriver::handleInsert);
    registerCommandHandler({COMMAND_TYPE_UPDATE, "icdbrequest"}, (handlerFunc)&ICGenericDataDriver::handleUpdate);
    registerCommandHandler({COMMAND_TYPE_DELETE, "icdbrequest"}, (handlerFunc)&ICGenericDataDriver::handleDelete);
    registerCommandHandler({COMMAND_TYPE_SELECT, "icdbrequest"}, (handlerFunc)&ICGenericDataDriver::handleSelect);
    registerCommandHandler({COMMAND_TYPE_UPDATE_GROUP, "icdbrequest"},(handlerFunc)&ICGenericDataDriver::handleGroupUpdate);
    return ret;
}

void ICGenericDataDriver::handleInsert(ICCommand& cmd)
{
    QDataStream rs(&(cmd.attrib), QIODevice::ReadOnly);
    ICDbInsertRequest ireq;
    ireq.deserialize(rs);
    ICDbResponse resp(true); // здесь есть нюанс. Если сюда добавляются указатели из ICDbInsertRequest.inserts.records, то в деструкторе будет повторное освобождение.
    auto conn = connection(&ireq);
    if(conn == nullptr)
    {
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = lastError().toUtf8();
        return;
    }
    conn->beginTransaction(true);
    if(conn->insert(ireq, resp))
    {
        conn->endTransaction(true);
        // подготовим ответ на команду
        cmd.status = COMMAND_STATUS_OK;
        cmd.attrib = QByteArray();
        QDataStream ws(&(cmd.attrib), QIODevice::WriteOnly);
        ws<<resp;
        // подготовим обновленную команду, которую положим в sync
        ICCommand updcom;
        updcom.id = cmd.id;
        updcom.type = cmd.type;
        updcom.status = cmd.status;
        updcom.object = cmd.object;
        updcom.time = cmd.time;
        QDataStream updstr(&updcom.attrib, QIODevice::WriteOnly);
        merge(ireq, resp).serialize(updstr);
        // теперь положим обновленную команду в sync и отправим
        ICCommand com;
        com.type = COMMAND_TYPE_SYNC;
        com.object = cmd.object;
        QDataStream tds(&com.attrib, QIODevice::WriteOnly);
        tds<<updcom;
        emit send(com);
    }
    else
    {
        conn->endTransaction(false);
        QString error=trUtf8("Ошибка добавления объекта(ов) в БД.")+conn->lastError();
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = error.toUtf8();
    }
    releaseConnection(conn);
    qDeleteAll(ireq.inserts.records);
}

void ICGenericDataDriver::handleUpdate(ICCommand& cmd)
{
    QDataStream rs(&(cmd.attrib), QIODevice::ReadOnly);
    ICDbUpdateRequest ureq;
    ureq.deserialize(rs);
    auto conn = connection(&ureq);
    if(conn == nullptr)
    {
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = lastError().toUtf8();
        return;
    }
    conn->beginTransaction(false);
    if(conn->update(ureq))
    {
        conn->endTransaction(true);
        cmd.status = COMMAND_STATUS_OK;
        ICCommand com;
        com.type = COMMAND_TYPE_SYNC;
        com.object = cmd.object;
        QDataStream tds(&com.attrib, QIODevice::WriteOnly);
        tds<<cmd;
        cmd.attrib = QByteArray();
        emit send(com);
    }
    else
    {
        conn->endTransaction(false);
        QString error=trUtf8("Ошибка обновления объекта(ов) в БД.")+conn->lastError();
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = error.toUtf8();
    }
    releaseConnection(conn);
}

void ICGenericDataDriver::handleSelect(ICCommand& cmd)
{
    QDataStream rs(&(cmd.attrib), QIODevice::ReadOnly);
    ICDbSelectRequest sreq;
    sreq.deserialize(rs);
    ICDbResponse resp;
    auto conn = connection(&sreq);
    if(conn == nullptr)
    {
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = lastError().toUtf8();
        return;
    }
    if(conn->select(sreq, resp))
    {
        cmd.status = COMMAND_STATUS_OK;
        cmd.attrib = QByteArray();
        QDataStream ws(&(cmd.attrib), QIODevice::WriteOnly);
        ws<<resp;
    }
    else
    {
        QString error=trUtf8("Ошибка выборки объекта(ов) в БД.")+conn->lastError();
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = error.toUtf8();
    }
    releaseConnection(conn);
}

void ICGenericDataDriver::handleGroupUpdate(ICCommand &cmd)
{
    QDataStream rs(&(cmd.attrib), QIODevice::ReadOnly);
    ICDbUpdateGroupRequest ureq;
    ureq.deserialize(rs);
    auto conn = connection(&ureq);
    if(conn == nullptr)
    {
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = lastError().toUtf8();
        return;
    }
    conn->beginTransaction(false);
    bool result = false;
    for(ICDbUpdateRequest req: ureq.requests())
    {
        result = conn->update(req);
        if(!result)
            break;
    }
    if(result)
    {
        conn->endTransaction(true);
        cmd.status = COMMAND_STATUS_OK;
        // generate syncs
        ICCommand com;
        com.type = COMMAND_TYPE_SYNC;
        com.object = cmd.object;
        ICCommand cmdCopy = cmd;
        cmdCopy.type = COMMAND_TYPE_UPDATE;
        for(ICDbUpdateRequest req: ureq.requests())
        {
            req.allowDestroyRecords(true);
            if(req.changes()->changes().isEmpty())
                continue;
            cmdCopy.attrib = QByteArray();
            QDataStream s(&cmdCopy.attrib,QIODevice::WriteOnly);
            req.serialize(s);
            com.attrib = QByteArray();
            QDataStream tds(&com.attrib, QIODevice::WriteOnly);
            tds<<cmdCopy;
            emit send(com);
        }
        cmd.attrib = QByteArray();
    }
    else
    {
        for(ICDbUpdateRequest req: ureq.requests())
            req.allowDestroyRecords(true);
        conn->endTransaction(false);
        QString error=trUtf8("Ошибка обновления объекта(ов) в БД.")+conn->lastError();
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = error.toUtf8();
    }
    releaseConnection(conn);
}

void ICGenericDataDriver::handleDelete(ICCommand& cmd)
{
    QDataStream rs(&(cmd.attrib), QIODevice::ReadOnly);
    ICDbRemoveRequest rreq;
    rreq.deserialize(rs);
    auto conn = connection(&rreq);
    if(conn == nullptr)
    {
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = lastError().toUtf8();
        return;
    }
    conn->beginTransaction(false);
    if(conn->remove(rreq))
    {
        conn->endTransaction(true);
        cmd.status = COMMAND_STATUS_OK;
        ICCommand com;
        com.type = COMMAND_TYPE_SYNC;
        com.object = cmd.object;
        QDataStream tds(&com.attrib, QIODevice::WriteOnly);
        tds<<cmd;
        cmd.attrib = QByteArray();
        emit send(com);
    }
    else
    {
        conn->endTransaction(false);
        QString error=trUtf8("Ошибка удаления объекта(ов) в БД.")+conn->lastError();
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = error.toUtf8();
    }
    releaseConnection(conn);
}
