#ifndef DDI_GLOBAL_H
#define DDI_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(DDI_LIBRARY)
#  define DDISHARED_EXPORT Q_DECL_EXPORT
#else
#  define DDISHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // DDI_GLOBAL_H
