#ifndef ICCONDITION_H
#define ICCONDITION_H

#include <QString>
#include <QVariant>
#include <icdbrequests_global.h>

/**
 * @brief Класс-контейнер условия выполнения запроса
 * @ingroup database dataaccess gui
 */
class ICDBREQUESTSHARED_EXPORT ICCondition
{
public:
    /**
     * @brief Операторы сравнения
     */
    enum ConditionOperators
    {
        UndefinedOperator=0,  ///< неопределен
        Less=1,               ///< меньше
        Greater=2,            ///< больше
        NotEquals=3,          ///< неравен (Less|Greater)
        Equals=4,             ///< равен
        LessOrEquals=5,       ///< меньше или равно (Less|Equals)
        GreaterOrEquals=6,    ///< больше или равно (Greater|Equals)
        Like=8                ///< похоже на
    }; 

    /**
     * @brief Создает контейнер для условия
     * @param field имя сравниваемого атрибута
     * @param value значение, с которым атрибут сравнивается
     * @param compareOperator оператор сравнения
     */
    ICCondition(const QString& field=QString(),const QVariant& value=QVariant(),ConditionOperators compareOperator=Equals)
        : m_field(field),
          m_compareOperator(compareOperator),
          m_value(value) { }

    //getters/setters:
    inline ConditionOperators compareOperator() const {return m_compareOperator;}
    inline void setCompareOperator(ConditionOperators val){m_compareOperator = val;}
    inline const QString& field() const{ return m_field;}
    inline void setField(const QString& val){m_field = val;}
    inline const QVariant& value() const{ return m_value;}
    inline void setValue(const QVariant& val){m_value = val;}
    /**
     * @brief Конструирует условие из строки.
     * @param expression выражение вида: (field) {=|>|<|<=|>=} (value)
     * @return
     */
    static ICCondition fromString(const QString& expression);
    /**
     * @brief Текстовое представление условия выборки
     * @return строка
     */
    QString toString() const;
    QString expression() const;
    inline bool isEmpty() const {return m_field.isEmpty();}
    //members:
private:
    QString m_field;                                ///<  имя атрибута
    ConditionOperators m_compareOperator = Equals;  ///< оператор сравнения
    QVariant m_value;                               ///<  значение
    QMap<ConditionOperators,QString> m_operDict = {
        {Less,"<"},
        {Greater,">"},
        {NotEquals,"!="},
        {LessOrEquals,"<="},
        {GreaterOrEquals,">="},
        {Equals,"="},
        {Like,"~"}
    };
};

// stream operators:

QDataStream& operator << (QDataStream& stream,const ICCondition& condition);
QDataStream& operator >> (QDataStream& stream,ICCondition& condition);

#endif // ICCONDITION_H
