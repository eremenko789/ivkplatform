#ifndef DBCONNECTION_GLOBAL_H
#define DBCONNECTION_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(DBCONNECTION_LIBRARY)
#  define DBCONNECTIONSHARED_EXPORT Q_DECL_EXPORT
#else
#  define DBCONNECTIONSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // DBCONNECTION_GLOBAL_H
