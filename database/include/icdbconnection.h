#ifndef DBCONNECTION_H
#define DBCONNECTION_H

#include "icdbconnection_global.h"
#include <icobject.h>
#include <icdballrequests.h>
#include <icdbresponse.h>
#include <icdbinformation.h>

#define IC_DEFAULT_DATASOURCE "dbfile"

/**
 * @brief Интерфейс подключения к БД.
 * @ingroup database
 */
class DBCONNECTIONSHARED_EXPORT ICDbConnection : public ICObject
{
    Q_OBJECT
public:
    /**
     * @brief Состояние подключения
     */
    enum State
    {
        Opened, ///< открыто
        Closed  ///< закрыто
    };

    ICDbConnection(QObject* parent=nullptr);

    /**
     * @brief Конструктор подключения.
     *
     * Создает подключение к БД по строке подключения.
     * Если строка подключения пуста, берет ее из конфига.
     * @param dbname имя БД
     * @param connectionString строка подключения
     */
    explicit ICDbConnection(const QString& dbname,const QString& connectionString=QString(), QObject* parent=0);
    virtual ~ICDbConnection(){}

    //setters/getters:

    /**
     * @brief устанавливает строку подключения
     * @param val строка подключения
     */
    inline void setConnectionString(const QString& val){m_connectionString = val;}
    /**
     * @brief Текущее состояние подключения
     * @return
     */
    inline State state() const {return m_state;}
    /**
     * @brief setState Установить текущее состояние
     * @param val новое состояние
     */
    inline void setState(State val){if(m_state==val) return;m_state=val;emit stateChanged(val);}
    /**
     * @brief имя БД
     * @return
     */
    inline QString databaseName() const {return m_dbname;}
    /**
     * @brief устанваливает имя БД
     * @param dbname имя БД
     */
    inline void setDatabaseName(QString dbname) {m_dbname = dbname;}
    /**
     * @brief имя подключения
     * @return
     */
    inline const QString& connectionName() const{return m_conName;}
    /**
     * @brief Уставновить имя подключения
     * @param val новое имя подключения
     */
    inline void setConnectionName(const QString& val){m_conName =val;}
    // operations:

    /**
     * @brief Выполнить запрос на добавление объекта
     * @param request контейнер запроса
     * @param response контейнер ответа
     * @param bulkSize кол-во инсертов, которые нужно объединять в один блочный. Если 0, то все инсерты объединяются в один.
     * @return true - успех, false - ошибка
     */
    bool insert(const ICDbInsertRequest& request, ICDbResponse& response, int bulkSize=1);
    /**
     * @brief Выполнить запрос на выборку объектов
     * @param request контейнер запроса
     * @param response результирующая выборка
     * @return true - успех, false - ошибка
     */
    bool select(const ICDbSelectRequest& request, ICDbResponse& response);
    /**
     * @brief Выполнить запрос на обновление объектов
     * @param request контейнер запроса
     * @return true - успех, false - ошибка
     */
    bool update(const ICDbUpdateRequest& request);
    /**
     * @brief Выполнить запрос на удаление объектов
     * @param request контейнер запроса
     * @return true - успех, false - ошибка
     */
    bool remove(const ICDbRemoveRequest& request);
    /**
     * @brief Выполнить запрос.
     * @param request контейнер запроса
     * @param response контейнер результата
     * @return  true - успех, false - ошибка
     */
    virtual bool execute(const ICDbExecRequest& stmt,ICDbResponse& response);
    /**
     * @brief open открывает подключение
     * @return
     */
    bool open();
    /**
     * @brief закрывает подключение
     */
    void close();
    /**
     * @brief Открыть транзакцию
     * @param sameRequestType выполнять однотипные запросы
     */
    bool beginTransaction(bool sameRequestType = false);
    /**
     * @brief завершить транзакцию
     * @param toCommit true - подтвердить, false - откатить
     */
    bool endTransaction(bool toCommit);

signals:
    /**
     * @brief Сигнал об изменении состояния подключения
     * @param state новое состояние
     */
    void stateChanged(State state);

protected:
    /**
     * @brief Класс информации об атрибуте
     */
    class ICDbFieldInfo
    {
    public:
        QString name;               ///< имя атрибута
        QVariant::Type valueType;   ///< тип значения
    };


    //database backend methods:

    /**
     * @brief open открывает подключение
     * @return
     */
    virtual bool openDB() = 0;
    /**
     * @brief закрывает подключение
     */
    virtual void closeDB() = 0;
    /**
     * @brief Открыть транзакцию
     */
    virtual bool beginDBTransaction(){return true;}
    /**
     * @brief завершить транзакцию
     * @param toCommit true - подтвердить, false - откатить
     */
    virtual bool endDBTransaction(bool toCommit){Q_UNUSED(toCommit);return true;}
    /**
     * @brief Запросить существующие типы объектов БД
     * @return список имен
     */
    virtual QStringList queryTypes() const = 0;
    /**
     * @brief Запросить список имен существующих атрибутов объектов данного типа
     * @param objType тип объекта БД
     * @return список имен атрибутов
     */
    virtual QStringList queryFields(const QString& objType) const = 0;
    /**
     * @brief добавить в БД информацию о данном типе объекта
     * @param typeName имя типа
     * @param fields список атрибутов
     * @return true- успех, false - ошибка
     */
    virtual bool createObjectType(const QString& typeName,const QList<ICDbFieldInfo>& fields) = 0;
    /**
     * @brief добавить атрибут для данного типа объекта БД
     * @param typeName имя типа
     * @param field информация об атрибуте
     * @return true- успех, false - ошибка
     */
    virtual bool createField(const QString& typeName,const ICDbFieldInfo& field) = 0;
    /**
     * @brief запросить объекты из БД
     * @param request запрос
     * @param response контейнер ответа
     * @return true- успех, false - ошибка
     */
    virtual bool selectObjects(const ICDbSelectRequest& request, ICDbResponse& response) = 0;
    /**
     * @brief добавить объекты в БД
     * @param typeName имя типа
     * @param object добавляемая запись
     * @param response контейнер ответа
     * @return true- успех, false - ошибка
     */
    virtual bool insertObject(const QString& typeName,const ICRecord* object, ICDbResponse& response) = 0;
    /**
     * @brief добавить блок объектов в БД
     * @param typeName имя типа
     * @param objects добавляемые записи
     * @param response контейнер ответа
     * @return true- успех, false - ошибка
     */
    virtual bool insertBulk(const QString& typeName,const QList<ICRecord*>& objects, ICDbResponse& response) = 0;
    /**
     * @brief обновить объекты в БД
     * @param request запрос
     * @return true- успех, false - ошибка
     */
    virtual bool updateObjects(const ICDbUpdateRequest& request) = 0;
    /**
     * @brief удалить объекты из БД
     * @param request запрос
     * @return true- успех, false - ошибка
     */
    virtual bool removeObjects(const ICDbRemoveRequest& request) = 0;

    // non-overridable internal methods:

    bool checkState();
private:
    bool createUnknownAttributes(const QString& typeName,const ICRecord* rec); ///< если тип уже есть, но у него нет какого-то атрибута, то метод его добавляет. Нужно, если вдруг меняется структура базы. 
    bool createTypesTree(const ICRecord* rec, const QString &typeAlias="", bool relationTable=false); ///< создает дерево типов
    bool recursiveInsert(const QString& itypeName, QList<ICRecord*> items, int bulkSize, ICDbResponse& resp, bool relationTable=false); ///< рекурсивный алгоритм инсертов
    bool simpleInsert(const QString& typeName, QList<ICRecord*>& inss, int bulkSize, ICDbResponse& resp); ///<  простой инсерт без рекурсивного спуска. Умеет инсертить только простые типы.
    bool valid(const ICDbRequest* request);
    bool hasEmbeddedRecords(const ICRecord* rec, bool checkAllNames=true) const; ///< проверяет, имеет ли объект вложенные подобъекты пользовательского типа, либо контейнеры
    QString getTypeName(const QVariant& val);
    QVariantList makeList(QVariant& v);
//    QList<QPair<QString,QVariant>> findObjectsForInsertion(QMultiHash<QString, const ICRecord*> refobjects /* объекты, среди которых*/, QVariantList fo);

protected:
    QString m_dbname;
    QString m_connectionString;
    State m_state;
    ICDbInformation *m_dbInfo;
    bool m_isTransactionOpened = false;
    inline bool needsToPrepare()
    {
        if(m_isRequestReady)
            return false;
        if(!m_sameRequestType)
            return true;
        m_isRequestReady = true;
        return true;
    }
    bool isTableReference(const QString& fieldName); ///< сообщает,является ли данное поле ссылкой на другую таблицу
    bool isListReference(const QString& fieldName); ///< сообщает,является ли данное поле признаком, что у этого объекта здесь список
    QStringList parseReferenceField(const QString& fieldName); ///< парсит поле, являющееся ссылкой и возвращает пару {typeName, fieldName}
private: 
    bool m_isRequestReady = false;
    bool m_sameRequestType = false;
    QString m_conName;
};

Q_DECLARE_INTERFACE(ICDbConnection, "ivk-center.ICDbConnection/1.0")

#endif // DBCONNECTION_H
