#ifndef ICHIDB_GLOBAL_H
#define ICHIDB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ICHIDB_LIBRARY)
#  define ICHIDBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ICHIDBSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ICHIDB_GLOBAL_H
