#ifndef ICAGGREGATION_H
#define ICAGGREGATION_H

#include <icdbrequests_global.h>
#include <QString>
#include <QList>
#include <QDataStream>

/**
 * @brief Класс-контейнер для агрегирующих функций
 * @ingroup database
 */
class ICDBREQUESTSHARED_EXPORT ICAggregation
{
public:
    /**
     * @brief Типы агрегирующих функций
     */
    enum AggregationFunc
    {
        None,   ///< пустышка
        Min,    ///< минимум
        Max,    ///< максимум
        Avg,    ///< среднее
        Count   ///< количество элементов
    };

    // constructors:

    ICAggregation();
    ICAggregation(AggregationFunc fn, QString fld){func = fn; field = fld;}

    // members:

    AggregationFunc func=None;   ///< тип функции
    QString field;          ///< имя поля, для которого применяется функция
};

//stream operators:

QDataStream& operator << (QDataStream& stream, const ICAggregation& aggreg);
QDataStream& operator >> (QDataStream& stream, ICAggregation& aggreg);

typedef QList<ICAggregation> ICAggregationSet;

#endif // ICAGGREGATION_H
