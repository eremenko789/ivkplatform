#ifndef ICSQLCONNECTION_H
#define ICSQLCONNECTION_H

#include "icsqlconnection_global.h"
#include "icdbconnection.h"
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDebug>

/**
 * @brief SQL-бэкенд подсистемы хранения данных.
 *
 * Реализует интерфейс ICDbConnection, используя библиотеку QtSql.
 * @see ICDbConnection
 * @ingroup database
 */
class ICSQLCONNECTIONSHARED_EXPORT ICSqlConnection : public ICDbConnection
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ivk-center.sqlconnection")
    Q_INTERFACES(ICDbConnection)
public:
    Q_INVOKABLE ICSqlConnection(const QString& dbname=QString(),const QString& conString = QString());
    ~ICSqlConnection();

    bool execute(const ICDbExecRequest &request, ICDbResponse &response);
protected:
    bool openDB() Q_DECL_OVERRIDE;
    void closeDB() Q_DECL_OVERRIDE;

    bool execStmt(const QString& stmt);
    bool execQuery();
    inline void reportDbError()
    {
        auto msg = m_db.lastError().text();
        reportError(msg);
    }
    inline void reportQueryError()
    {
        auto msg = m_query.lastError().text();
        reportError(msg);
        qDebug()<<"SQL error "<<m_query.lastError().number()<<":"<<m_query.lastQuery();
    }

    QStringList queryTypes() const Q_DECL_OVERRIDE;
    QStringList queryFields(const QString &objType) const Q_DECL_OVERRIDE;
    bool createObjectType(const QString &typeName, const QList<ICDbFieldInfo> &fields) Q_DECL_OVERRIDE;
    bool createField(const QString &typeName, const ICDbFieldInfo &field) Q_DECL_OVERRIDE;
    bool selectObjects(const ICDbSelectRequest &request, ICDbResponse &response) Q_DECL_OVERRIDE;
    bool insertObject(const QString &typeName, const ICRecord *object, ICDbResponse &response) Q_DECL_OVERRIDE;
    bool insertBulk(const QString &typeName, const QList<ICRecord*>& objects, ICDbResponse &response) Q_DECL_OVERRIDE;
    bool updateObjects(const ICDbUpdateRequest &request) Q_DECL_OVERRIDE;
    bool removeObjects(const ICDbRemoveRequest &request) Q_DECL_OVERRIDE;

    QString toSqlType(QVariant::Type type) const;
private:
    bool beginDBTransaction() Q_DECL_FINAL;
    bool endDBTransaction(bool toCommit) Q_DECL_FINAL;
    bool prepareQuery(const ICDbConditionRequest* request,const QString& stmtTemplate);
    bool execRequest(const ICDbConditionRequest* request,const QString& stmtTemplate);
    QString makeCondExpression(ICLogicalPair& pair);
    QString makeSelectFields(const ICDbSelectRequest &request);
    void packResults(ICDbResponse& response, bool genericContainer);
    bool needBreakOnSpecificDbError(const QSqlQuery& q);
    //members:
    QString m_recordType;
protected:
    QSqlDatabase m_db;
    QSqlQuery m_query;
    QHash<QVariant::Type,QString> m_knownTypes;
};

#endif // ICSQLCONNECTION_H
