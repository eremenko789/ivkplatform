#ifndef ICLOGICALPAIR_H
#define ICLOGICALPAIR_H

#include <iccondition.h>

/**
 * @brief Пара логический оператор <-> условие
 * @ingroup dataaccess database
 */
class ICLogicalPair
{
public:
    /**
     * @brief Логические операторы
     */
    enum LogicalOperator
    {
        LO_NONE,    ///< не определен
        LO_AND,     ///< и
        LO_OR       ///< или
    };

    ICLogicalPair(const ICCondition& c, LogicalOperator o=LO_NONE) {cond=c; op=o;}
    ICCondition cond;
    LogicalOperator op=LO_NONE;
};

#endif /* ICLOGICALPAIR_H */
