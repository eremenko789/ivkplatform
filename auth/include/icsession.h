#ifndef ICSESSION_H
#define ICSESSION_H

#include <QHash>
#include <icpermission.h>
#include <icrole.h>
#include <icuserinfo.h>

/**
 * @brief Класс описания сессии пользователя.
 * @ingroup auth
 */
class ICAUTHSHARED_EXPORT ICSession
{
public:
    ICSession(){}
    ICSession(const ICUserInfo& user, qint64 expTime=0, ICRole::Policy policy=ICRole::Allow);
    quint64 id() const {return m_sessionId;} ///< уникальный идентификатор сессии
    bool canAccess(quint64 obj, ICPermission::Privileges priv, QString* denyReason=nullptr) const; ///< определяет, имеет ли пользователь доступ к объекту с заданным идентификатором
    bool isActive() const; ///< Возвращает, активна ли еще данная сессия
//    static ICSession* currentSession() {return m_current;}
private:
    quint64 m_sessionId;     
    ICUserInfo m_user;       ///< пользователь сессии
    ICRole::Policy m_policy; ///< политика предоставления доступа (разрешения или запреты)
    QHash<quint64, ICPermission::Privileges> m_priveleges; ///< соответствие id объекта -> разрешения на него
    qint64 m_expirationTime; ///< время, когда сессия завершится.
//    static ICSession* m_current;
};

//QDataStream ICAUTHSHARED_EXPORT & operator>>(QDataStream& in, ICSession& sess);
//QDataStream ICAUTHSHARED_EXPORT & operator<<(QDataStream& out, const ICSession& sess);

#endif // ICSESSION_H
