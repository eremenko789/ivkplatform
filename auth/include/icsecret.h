#ifndef ICSECRET_H
#define ICSECRET_H

#include "icauth_global.h"
#include <QByteArray>
#include <icobjectrecord.h>
#include "icsecretcontainer.h"

/**
 * @brief Базовый класс для создания секретных данных аутентификации пользователя.
 * @ingroup auth
 */
class ICAUTHSHARED_EXPORT ICSecret : public ICObjectRecord
{
    Q_OBJECT
public:
    Q_PROPERTY(const ICSecretContainer* container READ container WRITE setContainer)

    Q_INVOKABLE explicit ICSecret(QObject* parent=nullptr):ICObjectRecord(parent){}
    ICSecret(const ICSecret& s);
    virtual inline ~ICSecret(){}
    virtual const ICSecretContainer* container() const {return &m_container;} ///< Возвращает бинарный контейнер с секретом пользователя 
    virtual void setContainer(const ICSecretContainer* c){m_container=*c;}    ///< Устанавливает готовый контейнер с секретом 
    ICSecret& operator=(const ICSecret& s){m_container=*(s.container()); return *this;}
protected:
    ICSecretContainer m_container;
};

Q_DECLARE_METATYPE(ICSecret)

#endif // ICSECRET_H
