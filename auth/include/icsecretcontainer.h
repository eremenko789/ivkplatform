#ifndef ICSECRETCONTAINER_H
#define ICSECRETCONTAINER_H

#include "icauth_global.h"
#include <icobjectrecord.h>

/**
 * @brief Бинарный контейнер для хранения секрета пользователя.
 *
 * При удаления контейнера данные, хранившиеся в нем вайпируются.
 * @ingroup auth
 */
class ICAUTHSHARED_EXPORT ICSecretContainer : public ICObjectRecord
{
    Q_OBJECT
public:
    Q_PROPERTY(QString dataType READ dataType WRITE setDataType)
    Q_PROPERTY(QByteArray data READ data WRITE setData)

    Q_INVOKABLE explicit ICSecretContainer(QObject* parent=nullptr);
    ICSecretContainer(const ICSecretContainer& c);
    ~ICSecretContainer();

    QByteArray data() const {return m_data;}                  ///< Данные контейнера. 
    void setData(const QByteArray& d) {wipeData(); m_data=d;}
    QString dataType() const {return m_dataType;}             ///< Строка, описывающая тип данных, из которого был получен контейнер.
    void setDataType(const QString& dt) {m_dataType = dt;}
    ICSecretContainer& operator=(const ICSecretContainer& s);

private:
    void wipeData(); ///< вайпирует данные, чтобы злодеи не узнали, что там было
    QString m_dataType;
    QByteArray m_data;
};

Q_DECLARE_METATYPE(ICSecretContainer)

#endif
