#ifndef ICPERMISSION_H
#define ICPERMISSION_H

#include <icauth_global.h>
#include <QFlags>
#include <QList>
#include <QVariant>
#include <icobjectrecord.h>

/**
 * @brief Класс, представляющий право действий с объектом.
 * @ingroup auth
 */
class ICAUTHSHARED_EXPORT ICPermission : public ICObjectRecord
{
    Q_OBJECT
public:
    Q_PROPERTY ( quint64 object READ object WRITE setObject ) 
    Q_PROPERTY ( Rule rule READ rule WRITE setRule )
    Q_PROPERTY ( Privileges privileges READ privileges WRITE setPrivileges )

    /**
     * Действия, которые могут производится с объектом
     */
    enum Privilege
    {
        None = 0,
        Read = 1,
        Write = 2,
        Exec = 4,
        Delete = 8
    };
    /**
     * Разрешения
     */
    enum Rule
    {
        Access,
        Deny
    };
    Q_DECLARE_FLAGS(Privileges, Privilege)
    Q_INVOKABLE explicit ICPermission(QObject* parent=nullptr);
    ICPermission(quint64 objId, ICPermission::Rule r = ICPermission::Access, Privileges at=ICPermission::None, QObject* parent=nullptr):ICObjectRecord(parent){ m_objId = objId; m_rule=r; m_optype=at;}
    quint64 object() const {return m_objId;}                  ///< Возвращает идентификатор объекта права
    void setObject(quint64 o){m_objId = o;}                   ///< устанавливает идентификатор объекта права
    Rule rule() const {return m_rule;}                        ///< Возвращает разрешение или запрещение какого-то действия с объектом
    void setRule(const Rule& r) {m_rule=r;}                   ///< устанавливает разрешение или запрещение какого-то действия с объектом
    Privileges privileges() const {return m_optype;}          ///< Возвращает набор действий, которые можно или нельзя производить с этим объектом 
    void setPrivileges(const Privileges& pr) {m_optype = pr;} ///< устанавливает набор действий, которые можно производить с объектом 

    bool operator==(const ICPermission& p) {return m_objId == p.m_objId && m_rule == p.m_rule && m_optype == p.m_optype;}

private:
    quint64 m_objId=0;
    Rule m_rule=ICPermission::Access;
    Privileges m_optype=ICPermission::None;
};

Q_DECLARE_METATYPE(ICPermission)

Q_DECLARE_OPERATORS_FOR_FLAGS(ICPermission::Privileges)

//QDataStream& ICAUTHSHARED_EXPORT operator<<(QDataStream& out, const ICPermission::Privileges& priv);
//QDataStream& ICAUTHSHARED_EXPORT operator>>(QDataStream& in, ICPermission::Privileges& priv);
/*inline QDataStream& operator<<(QDataStream& out, const ICPermission::Privileges& priv)
{
    out<<QVariant(priv);
    return out;
}

inline QDataStream& operator>>(QDataStream& in, ICPermission::Privileges& priv)
{
    QVariant tmp;
    in>>tmp;
    priv = ICPermission::Privileges(tmp.toInt());
    return in;
}

inline QDataStream& operator<<(QDataStream& out, const ICPermission& p)
{
    out<<p.object()<<p.rule()<<p.privileges();
    return out;
}

inline QDataStream& operator>>(QDataStream& in, ICPermission& p)
{
    quint64 oid;
    int r;
    ICPermission::Privileges pr;
    in>>oid>>r>>pr;
    p.setPrivileges(pr);
    p.setRule((ICPermission::Rule)r);
    p.setObject(oid);
    return in;
}
*/
#endif // ICPERMISSION_H
