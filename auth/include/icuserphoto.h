#ifndef ICUSERPHOTO_H
#define ICUSERPHOTO_H

#include "icauth_global.h"
#include <icobjectrecord.h>

/**
 * @brief Класс содержащий изображение пользователя
 * @group auth
 */
class ICAUTHSHARED_EXPORT ICUserPhoto : public ICObjectRecord
{
    Q_OBJECT
public:
    Q_PROPERTY(QByteArray imageData READ imageData WRITE setImageData NOTIFY imageDataChanged) ///< бинарные данные из изображения

    Q_INVOKABLE explicit ICUserPhoto(QObject* parent=nullptr);
    ICUserPhoto(const ICUserPhoto& c);
    ~ICUserPhoto(){}
    /**
     * @brief Загрузить изображение из файла
     * @param url файла картинки
     * @return true если изображение успешно загружено
     */
    bool load(const QString & urlString);
    /**
    * @brief Возвращает бинарные данные из изображения
    */
    inline QByteArray imageData() const{return m_imageData;}
    /**
    * @brief Устанавливает бинарные данные из изображения
    * @param val новое значение
    */
    void setImageData(QByteArray val);

    ICUserPhoto& operator=(const ICUserPhoto& p);

signals:
    void imageDataChanged();   ///< Сообщает об изменении значения свойства "бинарные данные из изображения"

private:
    QByteArray m_imageData;

};

Q_DECLARE_METATYPE(ICUserPhoto)

#endif // ICUSERPHOTO_H
