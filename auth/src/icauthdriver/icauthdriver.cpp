#include "icauthdriver.h"
#include <icauthrequest.h>
#include <icauthcommandstatuses.h>
#include <icpasswdsecret.h>
#include "icpasswdcomparer.h"
#include <icuserinfo.h>
#include <QMetaProperty>
#include <QThread>

ICAuthDriver::ICAuthDriver(QObject *parent) :
    ICDataDriverInterface(parent)
{
}

ICAuthDriver::~ICAuthDriver()
{
    qDeleteAll(m_comparers);
}

ICRETCODE ICAuthDriver::init(const QString& prefix, ICDbConnPoolManager *poolManager)
{
    auto ret = ICDataDriverInterface::init(prefix, poolManager);
    m_comparers["ICPasswdSecret"]=new ICPasswdComparer(this);
    m_comparers["ICPasswdSecret"]->init(prefix);
    registerCommandHandler({COMMAND_TYPE_LOGIN,  "ICCredentials"}, (handlerFunc)&ICAuthDriver::handleLogin);
    registerCommandHandler({COMMAND_TYPE_LOGOUT, "ICCredentials"}, (handlerFunc)&ICAuthDriver::handleLogout);
    registerCommandHandler({COMMAND_TYPE_AUTH,   "ICAuthRequest"}, (handlerFunc)&ICAuthDriver::handleCheckRights);
    registerCommandHandler({COMMAND_TYPE_INSERT, "ICUserInfo"}, (handlerFunc)&ICAuthDriver::handleAddUser);
    registerCommandHandler({COMMAND_TYPE_DELETE, "ICUserInfo"}, (handlerFunc)&ICAuthDriver::handleDelUser);
    // ICUserInfo uinf;
    // QVariant val = uinf.metaObject()->property(uinf.metaObject()->indexOfProperty("roles")).read(&uinf);
    // qDebug()<<"Type of ICUserInfo in QVariant is "<<val.type();
    // TODO: нужные настройки:
    // 1. время автозавершения сессии пользователя при неактивности
    // 2. политика доступа (запрещено все, что не разрешено, или наоборот)
    return ret;
}

void ICAuthDriver::checkUserExists()
{
    ICDbSelectRequest sr;
    sr.setTypeName("ICUserInfo");
    ICDbConnection* c = connection(&sr);
    ICDbResponse resp;
    c->select(sr, resp);
    if(resp.records.isEmpty())
    {
        ICUserInfo ui2;
        ICPasswdSecret etalon2("test");
        ui2.setName("test");
        ui2.setSecret(etalon2.container());
        ICDbInsertRequest ir;
        ir.inserts.records.append(&ui2);
        ir.setTypeName("ICUserInfo");
        ICDbResponse resp2;
        c->insert(ir, resp2);
        qDeleteAll(resp2.records);
    }
    releaseConnection(c);
    qDeleteAll(resp.records);
}

void ICAuthDriver::handleLogin(ICCommand& cmd)
{
    checkUserExists(); // FIXME: удалить нах!

    // запросить из базы данные о пользователе и его ролях
//    QThread::msleep(500);
    // создать сессию пользователя и вернуть ее идентификатор
    ICCredentials* cred = qobject_cast<ICCredentials*>(ICObject::fromPacked(cmd.attrib));
    const ICSecretContainer* querySecret = cred->secret();
    QString secretTypeName(querySecret->dataType());
    if(!m_comparers.contains(secretTypeName))
    {
        cmd.attrib = trUtf8("Данный тип аутентификации не поддерживается.").toUtf8();
        cmd.status = AUTH_STATUS_AUTHFAIL;
        delete cred;
        return;
    }
    // TODO: здесь надо получить из базы секрет этого пользователя, а пока вот так вот
//    ICPasswdSecret etalon("test");
    ICDbSelectRequest sr;
    sr.setTypeName("ICUserInfo");
    sr.setConditions({ICCondition("name",cred->name())});
    ICDbConnection* c = connection(&sr);
    ICDbResponse resp;
    c->select(sr, resp);
    releaseConnection(c);
    if(resp.records.isEmpty())
    {
        cmd.attrib = trUtf8("Пользователь %1 не найден.").arg(cred->name()).toUtf8();
        cmd.status = AUTH_STATUS_AUTHFAIL;
        delete cred;
        return;
    }
    ICUserInfo* ui = dynamic_cast<ICUserInfo*>(resp.records.first());
    const ICSecretContainer* etalon = ui->secret();

    ICSecCompareInterface* comparator = m_comparers[secretTypeName];
    if(comparator->compare(*querySecret, /* *(etalon.container())*/*etalon))
    {
        ICSession session(*ui);
        m_activeSessions.insert(session.id(), session);
        // надо упаковать в ответ инфу о пользователе без информации о секрете
        ICUserInfo cpy = *ui;
        ICPasswdSecret dummySecret;
        cpy.setSecret(dummySecret.container());
        cmd.attrib = cpy.pack();
        cmd.sessionId = session.id();
        cmd.status=COMMAND_STATUS_OK;
    }
    else
    {
        cmd.attrib = trUtf8("Некорректное имя пользователя или пароль.").toUtf8();
        cmd.status = AUTH_STATUS_AUTHFAIL;
    }
    delete cred;
    qDeleteAll(resp.records);
}

void ICAuthDriver::handleLogout(ICCommand& cmd)
{
    QDataStream str(cmd.attrib);
    quint64 session;
    str>>session;
    m_activeSessions.remove(session);
    cmd.status = COMMAND_STATUS_OK;
    QThread::msleep(500);
}

void ICAuthDriver::handleCheckRights(ICCommand& cmd)
{
    QDataStream str(cmd.attrib);
    ICAuthRequest req;
    str>>req;
    if(!m_activeSessions.contains(req.sessionId))
    {
        cmd.status = AUTH_STATUS_NOAUTH;
        cmd.attrib = trUtf8("Пользователь не авторизован, либо время сессии истекло").toUtf8();
        return;
    }
    if(!m_activeSessions[req.sessionId].isActive())
    {
        cmd.status = AUTH_STATUS_NOAUTH;
        cmd.attrib = trUtf8("Пользователь не авторизован, либо время сессии истекло").toUtf8();
        return;
    }
    QString res;
    if(!m_activeSessions[req.sessionId].canAccess(req.objectId, req.priv, &res))
    {
        cmd.status = AUTH_STATUS_DENY;
        cmd.attrib = res.toUtf8();
    }
    else
        cmd.status = COMMAND_STATUS_OK;
}

void ICAuthDriver::handleAddUser(ICCommand& cmd)
{
    // TODO: а тут надо сначала проверить, запрашивающий юзер тварь ли дрожжащая или право на добавление имеет?!!

    ICUserInfo* ui = qobject_cast<ICUserInfo*>(ICObject::fromPacked(cmd.attrib));
    if(ui==nullptr)
    {
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = trUtf8("Ошибка получения данных пользователя при добавлении!").toUtf8();
        return;
    }
    ICDbInsertRequest ir;
    ir.inserts.records.append(ui);
    ir.setTypeName("ICUserInfo");
    ICDbResponse resp;
    ICDbConnection* c = connection(&ir);
    if(!c->insert(ir, resp))
    {
        releaseConnection(c);
        cmd.status = COMMAND_STATUS_ERROR;
        cmd.attrib = trUtf8("Ошибка добавления пользователя %1!").arg(ui->name()).toUtf8();
        return;
    }
    releaseConnection(c);
    cmd.status=COMMAND_STATUS_OK;
    cmd.attrib = QByteArray();
    QDataStream ds(&cmd.attrib, QIODevice::WriteOnly);
    resp.serialize(ds);
    qDeleteAll(resp.records);
    delete ui;
}

void ICAuthDriver::handleDelUser(ICCommand& cmd)
{
    Q_UNUSED(cmd)
    // TODO: удаление пользователя будет просто по имени? Или как?
}
