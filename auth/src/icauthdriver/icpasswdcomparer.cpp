#include "icpasswdcomparer.h"
#include <icpasswdsecret.h>

bool ICPasswdComparer::compare(const ICSecretContainer& query, const ICSecretContainer& etalon) const
{
    ICPasswdSecret squery, setalon;
    squery.setContainer(&query);
    setalon.setContainer(&etalon);
    return squery==setalon;
}
