#ifndef ICAUTHPLUGIN_H
#define ICAUTHPLUGIN_H

#include <icauthdriver_global.h>
#include <icdatadriverinterface.h>
#include <icsession.h>
#include "icseccompareinterface.h"

/**
 * @brief Драйвер данных для ICDbServer, отвечающий за аутентификацию пользователя.
 * @ingroup auth
 */
class ICAUTHDRSHARED_EXPORT ICAuthDriver : public ICDataDriverInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ivk-center.icauthdriver")
    Q_INTERFACES(ICDataDriverInterface)

public:
    ICAuthDriver(QObject *parent = nullptr);
    ~ICAuthDriver();
    ICRETCODE init(const QString& prefix, ICDbConnPoolManager *poolManager);

private:
    void handleLogin(ICCommand& cmd);       ///< обработчик команды входа в систему
    void handleLogout(ICCommand& cmd);      ///< обработчик команды выхода из системы
    void handleCheckRights(ICCommand& cmd); ///< обработчик команды проверки прав на объект
    void handleAddUser(ICCommand& cmd);     ///< обработчик команды добавления пользователя
    void handleDelUser(ICCommand& cmd);     ///< обработчик команды удаления пользователя
    void checkUserExists();

    QHash<quint64, ICSession> m_activeSessions; // хэш активных сессий
    QHash<QString, ICSecCompareInterface*> m_comparers;
};

#endif // ICAUTHPLUGIN_H
