#include "icpasswdsecret.h"
#include <QCryptographicHash>
#include <icobjectmacro.h>

IC_REGISTER_METATYPE(ICPasswdSecret)
IC_REGISTER_ICRECORDTYPE(ICPasswdSecret)

//IC_REGISTER_METATYPE_STREAM_OPERATORS(ICPasswdSecret)

ICPasswdSecret::ICPasswdSecret(const QString& passwd) : ICSecret()
{
    m_container.setDataType(QString(metaObject()->className()));
    m_container.setData(QCryptographicHash::hash(passwd.toUtf8(), QCryptographicHash::Sha3_512));
}

bool ICPasswdSecret::operator==(const ICPasswdSecret& other)
{
    return m_container.data()==other.container()->data();
}

//ICSecret* ICPasswdSecret::make_copy() const
//{
//    ICPasswdSecret* ret = new ICPasswdSecret();
//    ret->setData(m_data);
//    return ret;
//}

void ICPasswdSecret::setPassword(const QString& p)
{
    m_container.setData(QCryptographicHash::hash(p.toUtf8(), QCryptographicHash::Sha3_512));
}

//QDataStream &operator<<(QDataStream &out, const ICPasswdSecret &myObj)
//{
//    out<<myObj.pack();
//    return out;
//}

//QDataStream &operator>>(QDataStream &in, ICPasswdSecret &myObj)
//{
//    QByteArray arr;
//    in>>arr;
//    ICPasswdSecret* obj = qobject_cast<ICPasswdSecret*>(ICPasswdSecret::unpack(arr));
//    myObj = *obj;
//    delete obj;
//    return in;
//}
