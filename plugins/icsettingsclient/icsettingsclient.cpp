#include "icsettingsclient.h"
#include <QCoreApplication>
#include <QMutexLocker>
#include <icsettingscontainer.h>
#include <QDebug>
#include <QPluginLoader>

QMutex ICSettingsClient::g_mutex;
ICSettingsClient *ICSettingsClient::g_instance = nullptr;

ICSettingsClient::ICSettingsClient()
{
    g_instance = this;
    //подписать метод выхода к сигналу приложения
    connect(QCoreApplication::instance(),&QCoreApplication::aboutToQuit,this,&ICSettingsClient::aborting,Qt::QueuedConnection);
}

ICSettingsClient::~ICSettingsClient()
{
    m_resendTimer->deleteLater();
    aborting();
}

ICSettingsClient* ICSettingsClient::instance()
{
    QMutexLocker lock(&g_mutex);
    Q_ASSERT(g_instance!=nullptr);
    return g_instance;
}

void ICSettingsClient::abort()
{
    g_instance->aborting();
    g_instance->deleteLater();
    g_instance=nullptr;
}

void ICSettingsClient::aborting()
{
    QMutexLocker locker(&m_mutex);
    Q_UNUSED(locker);
    m_toAbort=true;
    m_updateEvent.wakeAll();
}

QVariant ICSettingsClient::read(const QString &name)
{
    //в бесконечном цикле
    forever
    {
        {
            //заблокировать мьютекс
            QMutexLocker locker(&m_mutex);
            Q_UNUSED(locker);
            //запросить хэш
            auto val = g_instance->query(name);
            //если значение найдено или нужно заврешиться выходим
            if(m_cache.contains(name))
                return val;
            if(m_toAbort)
                return QVariant();
            Q_ASSERT_X(thread()!=QThread::currentThread(),"ICSettingsClient::read()","Synchronous reading of global settings from the plugin's' thread is denied!");
        }
        //ждем обновления
        QMutex mut;
        m_updateEvent.wait(&mut);
        mut.unlock();
    }
}

QVariant ICSettingsClient::beginRead(const QString &name)
{
    QMutexLocker locker(&m_mutex);
    Q_UNUSED(locker);
    return g_instance->query(name);
}

void ICSettingsClient::write(const QString &name, const QVariant &value)
{
    //заблокировать мьютекс
    QMutexLocker locker(&m_mutex);
    Q_UNUSED(locker);
    //сфоримровать команду записи настройки
    ICCommand cmd = newCommand();
    cmd.type = COMMAND_TYPE_UPDATE;
    cmd.object = "settings";
    QDataStream ds(&cmd.attrib,QIODevice::WriteOnly);
    ICSettingsContainer sett(name,value);
    ds<<sett;
    //отправить плагину сервера настроек
    request(cmd);
}

ICRETCODE ICSettingsClient::init(const QString& prefix)
{
    auto res = ICTMPluginInterface::init(prefix);
    int ri = setting("resendInterval",REPEAT_INTERVAL).toInt();
    if(ri!=0)
    {
        m_resendTimer = new QTimer(this);
        m_resendTimer->setSingleShot(false);
        m_resendTimer->setInterval(REPEAT_INTERVAL);
        connect(m_resendTimer,&QTimer::timeout,this,&ICSettingsClient::onResend,Qt::QueuedConnection);
        m_resendTimer->start();
    }
    //зарегистрировать обработчик синков
    registerCommandHandler({COMMAND_TYPE_SYNC,"settings"},(pluginFunc)&ICSettingsClient::handleSync);
    qRegisterMetaType<ICGlobalSettings>("ICGlobalSettings");
    return res;
}

void ICSettingsClient::handleSync(ICCommand &cmd)
{
    //десериализовать сообщение
    QDataStream ds(cmd.attrib);
    ICSettingsContainer sett;
    ds >> sett;
    //сравнить значение с кэшем и если не изменилось выйти
    auto nm = sett.key();
    if(m_cache.contains(nm))
    {
        auto exist = m_cache.value(nm);
        if(sett.value()==exist)
            return;
    }
    //обновить кэш
    m_cache[nm] = sett.value();
    //разбудить ждущие потоки
    m_updateEvent.wakeAll();
    //послать сигнал изменения
    emit valueChanged(nm,sett.value());
}

QVariant ICSettingsClient::query(const QString &name)
{
    //если в кэше есть значение для данного имени конфига
    if(m_cache.contains(name))
        //вернуть это значение
        return m_cache.value(name);
    //сформировать команду запроса настройки
    ICCommand cmd = newCommand();
    cmd.type = COMMAND_TYPE_SELECT;
    cmd.object = "settings";
    QDataStream ds(&cmd.attrib,QIODevice::WriteOnly);
    ds << ICSettingsContainer(name,QVariant());
    //отправить плагину сервера настроек
    request(cmd, make_callback(&ICSettingsClient::handleReadAnswer));
    return QVariant();
}

void ICSettingsClient::handleReadAnswer(quint64, ICCommand &cmd)
{
    if(cmd.status==COMMAND_STATUS_OK)
        return;
    //сругнуться
    if(cmd.status==COMMAND_STATUS_ERROR)
        reportError(QString(cmd.attrib));
    enqueueToResend(cmd);
}

void ICSettingsClient::enqueueToResend(ICCommand &cmd)
{
    reportError("Сервер глобальных настроек еще не загружен.");
    if(m_resendTimer==nullptr)
        return;
    cmd.status = COMMAND_STATUS_REQUEST;
    //поставить команду в очередь на переотправку
    m_resendMutex.lock();
    m_resendQueue.enqueue(cmd);
    m_resendMutex.unlock();
}

void ICSettingsClient::onResend()
{
    m_resendMutex.lock();
    QQueue<ICCommand> resendQueue = m_resendQueue;
    m_resendQueue.clear();
    m_resendMutex.unlock();
    int curCnt = resendQueue.count();
    for(int i=0;i<curCnt;i++)
    {
        auto cmd = resendQueue.dequeue();
        ICCommand newcmd = newCommand();
        newcmd.type = cmd.type;
        newcmd.object = cmd.object;
        newcmd.attrib = cmd.attrib;
        request(newcmd,make_callback(&ICSettingsClient::handleReadAnswer));
    }
}
