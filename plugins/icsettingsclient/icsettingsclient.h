#ifndef ICSETTINGSCLIENT_H
#define ICSETTINGSCLIENT_H

#include "icsettingsclient_global.h"
#include <ictmplugininterface.h>
#include <QHash>
#include <QVariant>
#include <QWaitCondition>
#include <icglobalsettings.h>
#include <QQueue>

/**
 * @brief Класс плагина, реализующего взаимодействие локальных классов с сервером глобальных конфигов.
 * 
 * Реализует инкапсуляцию и маршрутизацию запросов на чтение/запись глобальных конфигов,
 * а также информирует классы об измененении конфигурации, используя механизм сигналов и слотов.
 * @ingroup settings
 */
class ICSETTINGSCLIENTSHARED_EXPORT ICSettingsClient : public ICTMPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ivk-center.icsettings-client")
    Q_INTERFACES(ICPluginInterface)

public:
    Q_INVOKABLE ICSettingsClient();
    ~ICSettingsClient();
    //user methods:
    static ICSettingsClient* instance();
    /**
     * @brief Запросить настройку с указанным именем.
     * 
     * Метод блокирует вызывающий поток до получения результата.
     * @param name имя настройки
     * @return Возвращает значение настройки.В случае неудачи - Invalid.
     */
    QVariant read(const QString& name);
    /**
     * @brief Асинхронный запрос настройки с указанным именем.
     * 
     * Метод не блокирует вызывающий поток. Полученный результат будет сообщен сигналом.
     * @param name имя настройки
     * @return Возвращает значение настройки.В случае неудачи - Invalid.
     */
    QVariant beginRead(const QString& name);
    /**
     * @brief Записать значение конфига.
     * 
     * Метод не блокирует вызывающий поток.
     * @param name имя настройки
     * @param value новое значение
     */
    void write(const QString& name,const QVariant& value);
    /**
     * @brief Выйти из всех блокирующих функций
     */
    static void abort();

    //overriden methods:
    ICRETCODE init(const QString& prefix) final;
    ICPluginInfo pluginInfo() final{return {"icsettings-client",{0,0,1},{0,0,0}};}

    //signals:
signals:
    /**
     * @brief Сигнал об изменении конфига
     * @param name имя конфига, включая префикс
     * @param value значение
     */
    void valueChanged(QString name,QVariant value);
private slots:
//    void onStartResendTimer();
    void aborting();

    //command handlers
    void handleSync(ICCommand& cmd);
    void handleReadAnswer(quint64 addr,ICCommand& cmd);
    void onResend();
protected:
    //internal methods:
    QVariant query(const QString& name);
private:
    void enqueueToResend(ICCommand& cmd);

    //members:
    static ICSettingsClient *g_instance;///< статический инстанс класса-одиночки
    QHash<QString,QVariant> m_cache;    ///< кэш значений конфигов
    QWaitCondition m_updateEvent;       ///< событие изменения конфига
    static QMutex g_mutex;              ///< мьютекс, синхронизирующий доступ к статическому инстансу
    QMutex m_mutex;
    bool m_toAbort = false;
    QQueue<ICCommand> m_resendQueue;
    QTimer *m_resendTimer=nullptr;
    const int REPEAT_INTERVAL = 1000;
    QMutex m_resendMutex;
};

#endif // ICSETTINGSCLIENT_H
