#include "luaplugin.h"
#include <QDebug>
#include <iclocalsettings.h>

LuaPlugin::LuaPlugin() : m_Lua(nullptr)
{
    
}

LuaPlugin::~LuaPlugin()
{
    if(m_Lua != nullptr)
    {
	lua_close(m_Lua);
    }
}

void LuaPlugin::init(const QString& prefix)
{
    qDebug()<<"Lua plugin init";
    // здесь прочитаем из конфига, какой скрипт грузить, проинициализируем машину Lua, дадим API скрипту.
    ICLocalSettings sett;
    sett.beginGroup(prefix);
    QString path = sett.value("script",QVariant("")).toString();
    if(path.isEmpty())
    {
	// TODO: обработать ошибку того, что путь к скрипту не задан. 
	return;
    }
    m_Lua = luaL_newstate();
    if(luaL_loadfile(m_Lua, path.toUtf8().data()))
    {
	// TODO: обработать ошибку того, что файл скрипта не грузится.
	return;
    }
    scriptInit();
}

ICPluginInfo LuaPlugin::pluginInfo()
{
    const char* name = "luaplugin";
    ICPluginInfo info;
    quint16 major=0, minor=1;
    quint32 build=0;

    lua_getglobal(m_Lua, "pluginInfo");
    if(lua_istable(m_Lua, -1))
    {
	lua_pushstring(m_Lua, "pluginName");
 	lua_gettable(m_Lua, -2);
	if(lua_isstring(m_Lua, -1))
	    name = lua_tostring(m_Lua, -1);
	lua_pop(m_Lua, 1);

	lua_pushstring(m_Lua, "major");
	lua_gettable(m_Lua, -2);
	if(lua_isnumber(m_Lua, -1))
	    major = lua_tounsigned(m_Lua, -1);
	lua_pop(m_Lua, 1);

	lua_pushstring(m_Lua, "minor");
	lua_gettable(m_Lua, -2);
	if(lua_isnumber(m_Lua, -1))
	    minor = lua_tounsigned(m_Lua, -1);
	lua_pop(m_Lua, 1);

	lua_pushstring(m_Lua, "build");
	lua_gettable(m_Lua, -2);
	if(lua_isnumber(m_Lua, -1))
	    build = lua_tounsigned(m_Lua, -1);
	lua_pop(m_Lua, 1);
    }
    lua_pop(m_Lua, 1);
    
    info.pluginName = name;
    info.pluginVersion = {major, minor, build};
    info.platformCompat = {0,1,0};
    return info;
}

void LuaPlugin::processMessage(quint64 srcAddr, QByteArray data)
{
    scriptProcessMessage(srcAddr, data);
}

void LuaPlugin::uninit()
{
    scriptUninit();
}

void LuaPlugin::onPluginsChanged()
{
    scriptOnPluginsChanged();
}

void LuaPlugin::scriptInit()
{

}

void LuaPlugin::scriptUninit()
{

}

void LuaPlugin::scriptProcessMessage(quint64 srcAddr, QByteArray data)
{

}

void LuaPlugin::scriptOnPluginsChanged()
{

}
