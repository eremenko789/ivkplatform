#include "icsettingsserver.h"

ICSettingsServer::ICSettingsServer()
{
}

ICRETCODE ICSettingsServer::init(const QString &prefix)
{
    auto res = ICTMPluginInterface::init(prefix);
    //зарегистрировать обработчики команд
    registerCommandHandler({COMMAND_TYPE_SELECT,"settings"},(pluginFunc)&ICSettingsServer::handleRead);
    registerCommandHandler({COMMAND_TYPE_UPDATE,"settings"},(pluginFunc)&ICSettingsServer::handleWrite);
    return res;
}

void ICSettingsServer::handleRead(ICCommand &cmd)
{
    //десериализовать команду
    ICSettingsContainer sc;
    QDataStream ds(cmd.attrib);
    ds >> sc;
    //запросить значение настройки
    auto sett = settings();
    if(sett == nullptr)
    {
        reportAndSendError(cmd,"Указанный бэкенд для доступа к настройкам не зарегистрирован");
        return;
    }
    auto val = sett->value(sc.key(),sc.value());
    sc.setValue(val);
    sendSync(sc);
    cmd.status = COMMAND_STATUS_OK;
}

void ICSettingsServer::handleWrite(ICCommand &cmd)
{
    //десериализовать команду
    ICSettingsContainer sc;
    QDataStream ds(cmd.attrib);
    ds >> sc;
    //записать настройки
    auto sett = settings();
    if(sett == nullptr)
    {
        reportAndSendError(cmd,"Указанный бэкенд для доступа к настройкам не зарегистрирован");
        return;
    }
    sett->setValue(sc.key(),sc.value());
    sendSync(sc);
}

ICSettings* ICSettingsServer::settings()
{
    //если бэкенд инициализирован, вернуть
    if(m_settings != nullptr)
        return m_settings;
    //прочитать из локальных конфигов имя класса для доступа к глобальным
    auto gsBackendName = setting("settingsBackend",IC_DEFAULT_BACKEND).toString().toUtf8().constData();
    auto typeId = QMetaType::type(gsBackendName);
    Q_ASSERT(typeId!=QMetaType::UnknownType);
    if(typeId==QMetaType::UnknownType)
        return nullptr;
    //создать класс бэкенда
    m_settings = reinterpret_cast<ICSettings*>(QMetaType::create(typeId));
    Q_ASSERT(m_settings);
    //вернуть
    return m_settings;
}

void ICSettingsServer::reportAndSendError(ICCommand &cmd,const char* msg)
{
    QString str = trUtf8(msg);
    reportError(str);
    cmd.status = COMMAND_STATUS_ERROR;
    cmd.attrib = str.toUtf8();
}

void ICSettingsServer::sendSync(const ICSettingsContainer &sc)
{
    //сформировать синк
    auto syncCmd = newCommand();
    syncCmd.type = COMMAND_TYPE_SYNC;
    syncCmd.object = "settings";
    QDataStream wds(&syncCmd.attrib,QIODevice::WriteOnly);
    wds << sc;
    //отправить
    request(syncCmd);
}
