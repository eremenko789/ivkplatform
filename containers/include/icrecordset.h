#ifndef ICRECORDSET_H
#define ICRECORDSET_H

#include <QDataStream>
#include <icrecord.h>

/**
 * @brief Класс-контейнер набора объектов.
 * @ingroup core database
 */
class ICCONTAINERSSHARED_EXPORT ICRecordSet
{
public:
    ICRecordSet(bool destroyRecords = false) : m_needDestroyRecords(destroyRecords){}
    ICRecordSet(QList<ICRecord*>& r, bool destroyRecords = false) : m_needDestroyRecords(destroyRecords){records=r;}
    ~ICRecordSet();
    QList<ICRecord*> records;    ///< список контейнеров объектов
    /**
     * @brief сериализовать объект
     * @param output поток
     */
    void serialize(QDataStream& output) const;
    /**
     * @brief десериализовать объект
     * @param input поток
     */
    void deserialize(QDataStream& input);
    /**
     * @brief Разрешить/запретить автоматическое уничтожения записей
     * @param toAllow true (по-умолчанию) - разрешить, иначе - запретить
     */
    inline void allowDestroyRecords(bool toAllow=true){m_needDestroyRecords = toAllow;}
    inline bool isRecordDestructionAllowed() const {return m_needDestroyRecords;}
    ICRecordSet& operator=(const ICRecordSet& rs);
    /**
     * @brief deepCopy Копирует объект с полным копированием внутренних данных.
     * @param rs Источник для копирования.
     * @return Возвращает ссылку на самого себя.
     */
    ICRecordSet& deepCopy(const ICRecordSet& rs);
private:
    bool m_needDestroyRecords = false;
};

// stream operators

ICCONTAINERSSHARED_EXPORT QDataStream& operator <<(QDataStream& output,const ICRecordSet& rs);
ICCONTAINERSSHARED_EXPORT QDataStream& operator >>(QDataStream& input,ICRecordSet& rs);

#endif // ICRECORDSET_H
