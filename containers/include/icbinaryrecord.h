#ifndef ICBINARYRECORD_H
#define ICBINARYRECORD_H

#include <icrecord.h>

/**
 * @brief Реализация класса-контейнера объекта через QByteArray
 * @ingroup core database
 */
class ICCONTAINERSSHARED_EXPORT ICBinaryRecord : public ICRecord
{
    static QStringList g_names;
public:
    ICBinaryRecord(const QByteArray& content=QByteArray());
    QStringList names() const Q_DECL_FINAL { return g_names;}
    QVariant value(const QString&) const Q_DECL_FINAL {return m_content;}
    void setValue(const QString &name, const QVariant &value) Q_DECL_FINAL;
    QByteArray content() const{return m_content;}

    void serializeContent(QDataStream &stream) const Q_DECL_FINAL;
    void deserializeContent(QDataStream &stream) Q_DECL_FINAL;
    QString className() const Q_DECL_FINAL {return "ICBinaryRecord";}
    ICRecord* newInstance() const Q_DECL_FINAL { return new ICBinaryRecord();}
private:
    QByteArray m_content;
//    QStringList m_names;
};

#endif // ICBINARYRECORD_H
