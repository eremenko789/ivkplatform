#include "ickeyvaluepair.h"

ICKeyValuePair::ICKeyValuePair(const QString& key,const QVariant& value)
    : m_key(key),
      m_value(value)
{
}

QDataStream& operator <<(QDataStream& ds,const ICKeyValuePair& keyval)
{
    return ds << keyval.key()<<keyval.value();
}

QDataStream& operator >>(QDataStream& ds,ICKeyValuePair& keyval)
{
    QString k;
    QVariant v;
    ds>>k>>v;
    keyval.setKey(k);
    keyval.setValue(v);
    return ds;
}
