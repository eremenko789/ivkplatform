#include <icrecord.h>
#include <icvaluemap.h>
#include <icbinaryrecord.h>
#include <icmetaobjectrecord.h>

void ICRecord::serialize(QDataStream &ds,const ICRecord* record)
{
    if(record!=nullptr)
    {
        ds << record->className();//ivk::icTypeIndex(record->className());
        record->serializeContent(ds);
    }
}

ICRecord* ICRecord::deserialize(QDataStream &input)
{
//    int tp=0;
//    input >> tp;
//    ICRecord* rec = ivk::icNewInstance(tp);
    QString typeName;
    input>>typeName;
    auto rec = ivk::icNewInstanceByName(typeName);
    rec->deserializeContent(input);
    return rec;
}

ICRecord* ICRecord::makeCopy() const
{
    QByteArray arr;
    QDataStream istr(&arr, QIODevice::WriteOnly);
    serialize(istr, this);
    QDataStream ostr(&arr, QIODevice::ReadOnly);
    return deserialize(ostr);
}
