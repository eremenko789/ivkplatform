include(../containers_common.pri)

TARGET = iccontainers
TEMPLATE = lib
CONFIG += plugin
DEFINES += ICCONTAINERS_LIBRARY
QT -= gui
HEADERS += \
    ../include/icrecord.h \
    ../include/icrecordset.h \
    ../include/iccontainers_global.h \
    ../include/icvaluemap.h \
    ../include/icbinaryrecord.h \
    ../include/icmetaobjectrecord.h \
    ../include/ickeyvaluepair.h \
    ../include/icsettingscontainer.h \
    ../include/icrecordtypescollector.h

SOURCES += \
    ./icrecordset.cpp \
    ./icvaluemap.cpp \
    ./icrecord.cpp \
    ./icbinaryrecord.cpp \
    ./icmetaobjectrecord.cpp \
    ./ickeyvaluepair.cpp \
    ./icrecordtypescollector.cpp

INCLUDEPATH+= ../include
