#include <icvaluemap.h>

IC_REGISTER_ICRECORDTYPE(ICValueMap)

void ICValueMap::serializeContent(QDataStream &stream) const
{
    stream << m_values;
}

void ICValueMap::deserializeContent(QDataStream &stream)
{
    stream >> m_values;
}
