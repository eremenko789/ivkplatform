include(../containers_common.pri)

TARGET = icobjectrecord
TEMPLATE = lib
CONFIG += plugin
DEFINES += ICOBJECTRECORD_LIBRARY
QT -= gui
HEADERS += \
    ../include/icobjectrecord.h \
    ../include/icobjectrecord_global.h

SOURCES += ./icobjectrecord.cpp

INCLUDEPATH+= ../include

LIBS += -L$$DESTDIR -liccontainers -licobject
