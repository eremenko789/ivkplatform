#include <icobjectrecord.h>
#include <QMetaProperty>

ICObjectRecord::ICObjectRecord(QObject* parent) : ICObject(parent), ICRecord()
{
    connect(this,&ICObject::unpacked,this,&ICObjectRecord::setChanges);
}

ICObjectRecord::ICObjectRecord(const ICObjectRecord& c) : ICObjectRecord(c.parent())
{
    copy(&c);
}

ICObjectRecord::~ICObjectRecord()
{
}

QStringList ICObjectRecord::names() const
{
    QStringList props;
    int cnt = metaObject()->propertyCount();
    for(int i = 0; i<cnt; i++)
    {
        auto p = metaObject()->property(i);
        if(!p.isStored(this))
            continue;
        QString pn = p.name();
        if(pn != QString("objectName"))
            props<<pn;
    }
    for(auto dp: dynamicPropertyNames())
        props<<dp;
    return props;
}

bool ICObjectRecord::contains(const QString& nm) const 
{
    return names().contains(nm);
}

QVariant ICObjectRecord::value(const QString& name) const 
{
    return property(name.toUtf8().constData());
}

void ICObjectRecord::setValue(const QString& name,const QVariant& value) 
{
    setProperty(name.toUtf8().constData(), value);
}

void ICObjectRecord::serializeContent(QDataStream& stream) const 
{
    stream<<pack();
}

void ICObjectRecord::deserializeContent(QDataStream& stream) 
{
    QByteArray arr;
    stream>>arr;
    unpack(arr);
    m_changes.removeOne("rowid");
    m_changes.removeOne("updCounter");
//    commit();
}

ICRecord* ICObjectRecord::newInstance() const 
{
    ICRecord* ret = dynamic_cast<ICRecord*>(metaObject()->newInstance());
    if(ret==nullptr)
        qDebug()<<QString("Невозможно создать экземпляр класса %1. Возможно, конструктор не объявлен Q_INVOKABLE").arg(metaObject()->className());
    return ret;
}

QString ICObjectRecord::className() const 
{
    return metaObject()->className();
}

void ICObjectRecord::commit()
{
    for(int i=0; i<metaObject()->propertyCount();i++)
    {
        auto nm = metaObject()->property(i).name();
        if(nm==QString("updCounter") || nm==QString("rowid"))
            continue;
        if(!metaObject()->property(i).isStored(this))
            continue;
        m_commitedValues[nm] = property(nm);
    }
    for(QByteArray dp: dynamicPropertyNames())
        m_commitedValues[dp] = property(dp.constData());
    auto commitRecord = [&](QVariant& v){
          auto rec = v.value<ICObjectRecord*>();
          if(rec)
              rec->commit();
    };
    for(QVariant v: m_commitedValues.values())
    {
        if(v.type()==QVariant::UserType)
            commitRecord(v);
        else if(v.type()==QVariant::List)
        {
            auto vl = v.toList();
            for(auto el:vl)
                commitRecord(el);
        }
    }
}

void ICObjectRecord::rollback()
{
    auto rollbackRecord = [&](QVariant& v){
          auto rec = v.value<ICObjectRecord*>();
          if(rec)
              rec->rollback();
    };
    for(QString nm: m_commitedValues.keys())
    {
        auto v = m_commitedValues[nm];
        setProperty(nm.toUtf8().constData(),v);
        if(v.type()==QVariant::UserType)
            rollbackRecord(v);
        else if(v.type()==QVariant::List)
        {
            auto vl = v.toList();
            for(auto el:vl)
                rollbackRecord(el);
        }
    }
}

void ICObjectRecord::restoreCommitState()
{
    for(auto nm: m_changes)
        m_commitedValues.remove(nm);
}

void ICObjectRecord::copy(const ICObjectRecord *cpy)
{
    for(int i=0; i<cpy->metaObject()->propertyCount();i++)
    {
        auto nm = cpy->metaObject()->property(i).name();
        setProperty(nm,cpy->property(nm));
    }
//    commit();
}

bool ICObjectRecord::isPropSerializable(const QString &pname) const
{
    bool res = ICObject::isPropSerializable(pname);
    if(!res)
        return false;
    return m_commitedValues.contains(pname) ?
                m_commitedValues[pname] != property(pname.toUtf8().constData())
              : true;
}
