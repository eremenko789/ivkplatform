QT += testlib
TARGET = ictest
TEMPLATE = lib
CONFIG += plugin
DEFINES+= ICTEST_LIBRARY

TESTING_DIR = $$PWD/../..
DESTDIR = $$TESTING_DIR/../bin

INCLUDEPATH += $$TESTING_DIR/include
HEADERS += \
    $$TESTING_DIR/include/integrationtest.h

SOURCES += \
    integrationtest.cpp
