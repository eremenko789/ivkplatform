#include "integrationtest.h"
#include <QtTest/QTest>

IntegrationTest::IntegrationTest(QObject *parent) :
    QObject(parent)
{
}

void IntegrationTest::begin(int testCase)
{
    m_currentTest = testCase;
    m_success = false;
}

void IntegrationTest::verify(int timeout)
{
    QTRY_VERIFY_WITH_TIMEOUT(m_success,timeout);
}
