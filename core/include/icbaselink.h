#ifndef ICBASELINKSESSION_H
#define ICBASELINKSESSION_H
#include "icnetworkdetails.h"
#include "icmessages.h"
#include "icbaselinkprivate.h"

class ICConnection;

namespace icrouting {


class ICBaseLink: public ICObject
{
    Q_OBJECT
public:

    virtual ~ICBaseLink();

    void setConnection(ICConnection* c){m_priv->setConnection(c);}

    ICConnection* connection()const{return  m_priv->connection();}

    void open(){m_priv->open();}

    void close(){m_priv->close();}

    ICRemoteNode* node()const{return m_priv->node();}

    ICNetworkDetails serverDetails()const{return m_priv->serverDetails();}

    void setServerDetails(const ICNetworkDetails& addr){m_priv->setServerDetails(addr);}

    int priority()const{return m_priv->priority();}

    bool isNodeUp()const{return m_priv->isNodeUp();}


signals:

    void nodeUp();

    void nodeDown();

    void closed();

    void aborted();

    void pluginsChanged();

    void pluginMessage(quint64 src, quint64 dest, const QByteArray& data);

protected:
    ICBaseLink(ICBaseLinkPrivate*priv);

    ICBaseLinkPrivate* impl()const{return m_priv;}

private:
    ICBaseLink();

    ICBaseLinkPrivate*m_priv = nullptr;

};

typedef std::unique_ptr<ICBaseLink> ICBaseLinkSessionU;

bool operator ==(const ICBaseLink& first,const ICBaseLink& second);

std::size_t hash_value(const icrouting::ICBaseLink &session);

}



#endif // ICBASELINKSESSION_H
