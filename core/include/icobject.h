#ifndef ICOBJECT_H
#define ICOBJECT_H

//#define IC_ORGANIZATION_NAME "ivk-center"

#include <icerrornotifier.h>
#include <QVariant>
#include "iclocalsettings.h"
#include "qscopedpointer.h"
#include <icobject_global.h>
#include <icobjectsettings.h>
#include <icobjectmacro.h>
#include <icretcode.h>
#include <icobject_global.h>

/**
 * @brief Класс-прослойка между QObject и классами, требующими поддержку механизма сигналов/слотов.
 * 
 * Класс позволяет применять общие политики ко всем компонентам системы. В частности, обобщен механизм обработки ошибок.
 * @ingroup core
 */
class ICOBJECTSHARED_EXPORT ICObject : public ICErrorNotifier

{
    Q_OBJECT
public:
    explicit ICObject(QObject *parent = 0);
    virtual ~ICObject();

    /**
     * @brief Метод для удобного доступа к настройкам.
     * 
     * Использует ICSettings для чтения из конфига.
     * @param name наименование настройки
     * @param defaultValue значение по-умолчанию
     * @param locally читать из локального хранилища?
     * @return возвращает QVariant
     */
    QVariant setting(const QString& name, const QVariant &defaultValue=QVariant(), bool locally=true) const;
    /**
     * @brief Записывает значение настройки
     * @param name имя настройки
     * @param value значение
     * @param locally в локальное хранилище?
     */
    Q_INVOKABLE void saveSetting(const QString& name,const QVariant& value,bool locally=true);
    /**
     * @brief Возвращает префикс группы настроек
     * @return
     */
    QString settingsPrefix() const {return m_settingsPrefix;}
    /**
     * @brief Метод, выполняющий инициализацию класса.
     * 
     * Для корректной работы (вызова переопределенных методов) должен быть вызван после конструктора.
     * @param pref префикс настроек
     * @return статус инициализации
     */
    virtual ICRETCODE init(const QString& pref);
    /**
     * @brief Регистрирует список свойств в подсистеме управления настройками.
     * @param names список имен свойств
     * @param local хранить локально/глобально?
     */
    Q_INVOKABLE void registerSettings(const QStringList& names,bool local);
    /**
     * @brief pack сериализует данные класса в QByteArray. Сериализуются только те данные, которые объявлены как Q_PROPERTY
     * @return Сериализованные данные
     */
    Q_INVOKABLE QByteArray pack() const;

    /**
     * @brief fromPacked Создает объект из сериализованных данных через QMetaObject
     * @param data Сериализованные данные
     * @return Указатель на созданный объект
     */
    Q_INVOKABLE static ICObject* fromPacked(QByteArray &data);

    /**
     * @brief unpack Десериализует упакованные данные в существующий объект.
     * @param data Сериализованные данные
     * @return true в случае удачи при распаковке
     */
    Q_INVOKABLE bool unpack(QByteArray& data);
    Q_INVOKABLE bool unpack(QDataStream& str);
signals:
    void unpacked(QStringList fieldNames);
protected:
    virtual QStringList  localSettings() const{return QStringList();}
    virtual QStringList  globalSettings() const{return QStringList();}
    virtual bool isPropSerializable(const QString& pname) const;
private:
    void recursivePack(const QVariant& v, QDataStream& str) const;
    QVariant recursiveUnpack(QDataStream& str);
    QScopedPointer<ICLocalSettings> m_s;
    QString m_settingsPrefix;
    QSharedPointer<ICObjectSettings> m_settingsHub;
};

Q_DECLARE_METATYPE(const ICObject*)

#endif // ICOBJECT_H
