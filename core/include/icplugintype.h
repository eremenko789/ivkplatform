#ifndef ICPLUGINTYPEINFO_H
#define ICPLUGINTYPEINFO_H
#include "QList"
#include "QString"

struct ICPluginType
{
    QString name;
    QList<quint32> nodeIDs;
};

#endif // ICPLUGINTYPEINFO_H
