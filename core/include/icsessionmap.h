#ifndef ICSESSIONMAP_H
#define ICSESSIONMAP_H
#include "icbaselink.h"
#include "icnetworkdetails.h"
#include "boost/ptr_container/ptr_unordered_map.hpp"
using boost::ptr_unordered_map;


namespace icrouting {

class ICLinkMap: public ptr_unordered_map<ICNetworkDetails, ICBaseLink>
{
public:
    bool contains(ICNetworkDetails address);
};

}


#endif // ICSESSIONMAP_H
