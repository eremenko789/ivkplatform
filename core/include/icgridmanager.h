#ifndef ICREMOTENODESMANAGER_H
#define ICREMOTENODESMANAGER_H
#include "icnetworkdetails.h"
#include "icobject.h"
#include "QTimer"
#include "icnetwork.h"
#include "icnodeswitch.h"
#include "vector"
#include "memory"
#include "icremotenode.h"
#include "iclocalnode.h"
#include "icbaselink.h"
#include "icsessionmap.h"
#include "icplatform_global.h"

//#include "boost/ptr_container/ptr_unordered_map.hpp"

using std::vector;
using std::move;
using std::unique_ptr;
//using boost::ptr_unordered_map;

namespace icrouting {

class ICGridManager: public ICObject
{
    Q_OBJECT

public:

    static ICGridManager* instance(){if(!m_staticInstance) m_staticInstance = new ICGridManager;return m_staticInstance;}

    void setLocalNode(const ICLocalNode*ln){m_localNode = ln;}

    ICRETCODE init(const QString& pref);

    void start();

    ICNodeInfo localNodeInfo()const{return m_localNode->info();}

    ICLinkVerificationInfo linkVerificationInfo()const;

    ICDiscoveryInfo discoveryInfo(const ICNetworkDetails& requestingLinkAddr)const;

    bool sessionValid(ICBaseLink* s);

    bool nodeNameValid(QString name);


private slots:

    void onDiscoveryInfoAvailable(const vector<ICNetworkDetails>&);

    void onPeerServerAddressResolved(const ICNetworkDetails& serverAddr, const ICNetworkDetails& tempAddr);

    void onLinkClosed();

    void onLinkAborted();

    void onNodeUp();

    void onNodeDown();

    void onIncomingConnection(ICConnection*connection);

private:

    typedef ptr_unordered_map<ICNetworkDetails, ICBaseLink>::iterator SessionIter;
    typedef ptr_unordered_map<ICNetworkDetails, ICBaseLink>::const_iterator SessionIterC;
    typedef ptr_unordered_map<ICNetworkDetails, ICBaseLink> SessionMap;

    ICGridManager(){}

    ICConnection* createConnection(const ICNetworkDetails& addr)const;

    void initPrimarySession(ICNetworkDetails addr);

    vector<ICNetworkDetails> serverAddressesFromConfig(int numAddr);

    bool isaBetterAddress(const ICBaseLink& s);

    void startSession(ICBaseLink* s, ICConnection* c);

signals:

    void newNode(const ICRemoteNode *);
    void nodeLost(const ICRemoteNode*);
    void remotePluginsChanged();
    void remoteMessage(quint64 src, quint64 dest, const QByteArray& data);

private:

    static ICGridManager*m_staticInstance;

    QSet<ICNetworkDetails>m_primaryAddresses;

    ICLinkMap m_activeLinks;

    ICLinkMap m_unresolvedLinks;

    const ICLocalNode* m_localNode;

    ICTcpServer m_listener;
};

QDataStream& operator<<(QDataStream& stream, const ICNetworkDetails& details);
QDataStream& operator>>(const QDataStream& stream, ICNetworkDetails& details);

}

#endif // ICREMOTENODESMANAGER_H
