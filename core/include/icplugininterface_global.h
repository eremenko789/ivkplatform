#ifndef ICPLUGININTERFACE_GLOBAL_H
#define ICPLUGININTERFACE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ICPLUGININTERFACE_LIBRARY)
#  define ICPLUGININTERFACESHARED_EXPORT Q_DECL_EXPORT
#else
#  define ICPLUGININTERFACESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ICPLUGININTERFACE_GLOBAL_H
