#ifndef ICOBJECT_GLOBAL_H
#define ICOBJECT_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ICOBJECT_LIBRARY)
#  define ICOBJECTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ICOBJECTSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // ICOBJECT_GLOBAL_H
