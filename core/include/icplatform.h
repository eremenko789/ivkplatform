#ifndef ICPLATFORM_H
#define ICPLATFORM_H

#include <QObject>
#include <QString>
#include <icpluginloader.h>
#include "icrouter.h"
#include "QScopedPointer"
#include <icplatformloader.h>
#include "icplatform_global.h"

/**
 * @defgroup core Платформа
 */

/** 
 * @brief Класс платформы.
 * 
 * Для использования достаточно просто создать объект на стеке функции main и запустить обработку цикла событий.
 * @ingroup core
 */

class ICPLATFORMSHARED_EXPORT ICPlatform: public QObject
{

    Q_OBJECT
public:
    ICPlatform();
    ~ICPlatform();

private slots:
    void initPlatform();       ///< Инициализирует платформу, загружает нужные плагины и запускает нужные инстансы.
    void onPlatformLoaded(ICPluginLoader* loader);   ///< Запускается, когда загружены и проинициализированы все плагины.

private:
    QString m_prefix;         ///< группа плагинов, которую загружает данный инстанс платформы.
    QThread* m_mainThread;    ///< указатель на главный поток. Нужен для того, чтобы некоторые плагины перемещать в него, в то время как сама платформа будет в другом потоке. Например, это нужно для плагинов с GUI.
    QThread* m_platformThread;///< Указатель на поток, в котором работает платформа.
    ICPluginLoader* m_loader; ///< указатель на класс-загрузчик плагинов.
    QScopedPointer<icrouting::ICRouter> m_router; ///< routes plugin messages
    QScopedPointer<ICPlatformLoader> m_platformLoader;    ///< загрузчик платформы

signals:
    void init();
};

#endif //ICPLATFORM
