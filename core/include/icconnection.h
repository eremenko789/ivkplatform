#ifndef ICCONNECTION_H
#define ICCONNECTION_H

#include "icnetwork_global.h"
#include "icdataencoder.h"
#include "icdatastream.h"
#include "ickeygenerator.h"
#include <icpacket.h>
#include <icobject.h>
#include <QTimer>
#include <QQueue>
#include "memory"
#include "QDebug"

using std::unique_ptr;

/**
  * @brief Класс соединения.
  *
  * Реализует интерфейс для установления соединения и обмена сообщениями между двумя
  * узлами системы.
  * @ingroup core
  */
class ICNETWORKSHARED_EXPORT ICConnection : public ICObject
{
    Q_OBJECT
public:
    /**
     * @brief Конструктор класса ICConnection.
     * @param  connectionAttribute Атрибут соединения. Тип и формат атрибута определяется исполнителем,
     * т.е. наследником интерфейса ICDataStream.
     * Например, это может быть строка в формате "<ip>:<port>" или дескриптор сокета.
     * Необходимо учитывать, что тип передаваемого аргумента должен поддерживаться классом QVariant,
     * например, передавая дескриптор сокета необходима явно преобразовать его к qulonglong.
     * @param parent родитель
     */
    ICConnection(QVariant connectionAttribute = QVariant(),QObject* parent = 0);

    //enums

    /**
     * @brief Перечисление, описывающее возможные состояния подключения.
     */
    enum ICConnectionState{
        ConnectionOpenningState,///< Подключение устанавливается
        ConnectionAuthState,    ///< Подключение аутентифицируется
        ConnectionReadyState,   ///< Подключение установлено
        ConnectionClosedState   ///< Подключение разорвано
    };

    //methods

   /**
    * @brief Назначить объект, осуществляющий доступ к каналу передачи данных.
    * 
    * После передачи объекта, за его освобождение отвечает ICConnection.
    * @param  dataStream Объект класса, реализующего доступ к каналу передачи данных
    */
    void setDataStream(ICDataStream* dataStream);
    /**
     * @brief Назначить объект, осуществляющий кодирование/декодирование данных.
     * 
     * После передачи объекта, за его освобождение отвечает ICConnection.
     * @param  dataEncoder кодировщик/декодировщик данных
     */
    void setDataEncoder(ICDataEncoder* dataEncoder );
    /**
     * @brief Назначить объект, через который реализуется алгоритм генерации сеансового ключа.
     * 
     * После передачи объекта, за его освобождение отвечает ICConnection.
     * @param  keyGenerator Объект-генератор сеансовых ключей
     */
    void setKeyGenerator(ICKeyGenerator* keyGenerator);
public slots:
    /**
     * @brief Открыть соединение.
     */
    void open();
public:
    /**
     * @brief Закрыть соединение
     */
    void close();
    /**
     * @brief Отправить сообщение по данному соединению.
     * @param  message Сообщение
     * @param ttl Время жизни сообщения (кол-во допустимых повторов)
     */
    void send (QByteArray message,int ttl=IC_DEFAULT_TTL);

    QString peerPortAddress()const{return m_dataStream->peerPortAddress();}

    quint32 localAddress()const{return m_dataStream->localAddress();}

    /**
     * @brief Возвращает текущее состояние соединения
     * @return ICConnectionState
     */
    inline ICConnectionState state ( ) const
    {
        return m_state;
    }
private:
    /**
     * @brief Установить новое состояние соединения
     * @param  state новое состояние соединения
     */
    void setState (ICConnectionState state)
    {
        if(m_state==state)
            return;
        m_state = state;
        emit stateChanged(state);
        if(m_state == ConnectionReadyState)
            emit connected();
        else if(m_state == ConnectionClosedState)
        {
            emit disconnected();
         }
    }
public:
    /**
     * @brief Переустанавливать ли подключение при потере связи?
     * 
     * Актуально только для исходящих подключений.
     * @return true - да, иначе - нет.
     */
    inline bool autoReconnect() const { return m_autoReconnect && m_outgoing;}
    /**
     * @brief Установить значение флага автоматической переустановки подключения.
     * 
     * Для исходящих подключений.
     * @param val значение флага.
     */
    inline void setAutoReconnect(bool val) {if(m_autoReconnect!=val) m_autoReconnect=val;}
    /**
     * @brief Исходящее ли соединение?
     * @return true, если данный узел должен устанавливать подключение, false - если принимать.
     */
    inline bool outgoing() const {return m_outgoing;}
    /**
     * @brief Установить значение флага исходящего подключения.
     * @param val
     */
    inline void setOutgoing(bool val){if(m_outgoing!=val) m_outgoing=val;}
    /**
     * @brief Изменяет атрибут подключения.
     *
     * Если dataStream был ранее назначен, то для применения нового атрибута подключения,
     * необходимо переназначить dataStream.
     * @param val новое значение атрибута подключения
     * Например, это может быть строка в формате "<ip>:<port>" или дескриптор сокета.
     * Необходимо учитывать, что тип передаваемого аргумента должен поддерживаться классом QVariant,
     * например, передавая дескриптор сокета необходима явно преобразовать его к qulonglong.
     */
    void setConnectionAttribute(QVariant val);

    QVariant connectionAttribute()const{return m_connectionAttribute;}
private slots:
    /**
     * @brief Слот-обработчик сигнала о пополнении входного буфера
     */
    void onReadyRead ( );
    /**
     * @brief Обработчик сигнала открытия потока
     */
    void onStreamOpened();
    /**
     * @brief Обработчик сигнала закрытия потока
     */
    void onStreamClosed();
    /**
     * @brief Обработчик сигнала на отправку публичного ключа
     * @param request пакет с публичным ключом
     */
    void onKeyRequestSending(ICPACKET request);
    /**
     * @brief Обработчик сигнала на отправку ответа
     * @param response пакет с ответом
     */
    void onKeyResponseSending(ICPACKET response);
    /**
     * @brief Слот-обработчик сигнала о генерации сеансового ключа
     * @param key сеансовый ключ
     */
    void onKeyGenerated(QByteArray key);
    /**
     * @brief Слот-обработчик таймера отправки отложенных сообщений
     */
    void onSendingTimerShot();


signals:  
    /**
     * @brief Сигнал о получении нового сообщения
     * @param message Полученное сообщение
     */
    void received (QByteArray message );
    /**
     * @brief Сигнал об изменении состояния соединения
     * @param newState новое состояние
     */
    void stateChanged ( ICConnection::ICConnectionState newState);
    /**
     * @brief connected
     * Convenience signal
     */
    void connected();
    /**
     * @brief disconnected
     * Convenience signal
     */
    void disconnected();

private:
    void sendOrDelay(ICPACKET& packet);
    bool trySend(ICPACKET& packet);
    void enqueuePacket(const ICPACKET& packet);

    // Состояние соединения
    ICConnectionState m_state;
    // Переустанавливать исходящие подключения в случае потери?
    bool m_autoReconnect;
    // Подключение является исходящим?
    bool m_outgoing;
    QVariant m_connectionAttribute;
    ICDataStream* m_dataStream;
    ICDataEncoder* m_dataEncoder;
    ICKeyGenerator* m_keyGenerator;
    QTimer m_resendTimer;
    QQueue<ICPACKET> m_sendQueue;
    QByteArray m_readBuffer;

    const int g_defaultReconnectDelay = 500;
    const int g_defaultResendInterval = 500;

    friend class ConnectionTest;
};

typedef unique_ptr<ICConnection> ICConnectionU;
typedef QSharedPointer<ICConnection> ICConnectionSh;

#endif // ICCONNECTION_H
