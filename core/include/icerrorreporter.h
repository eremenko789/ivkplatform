#ifndef ICERRORREPORTER_H
#define ICERRORREPORTER_H

#include <icerrornotifier.h>
#include <icobject_global.h>

namespace ivk
{

/**
 * @brief Класс для генерации сообщений об ошибках из классов, не наследующих ICObject
 * @ingroup core
 */
class ICOBJECTSHARED_EXPORT ICErrorReporter: public ICErrorNotifier
{
    Q_OBJECT
public:
    ICErrorReporter(const QString& msg)
        : ICErrorNotifier()
    {
        reportError(msg);
    }
};
}

#endif // ICERRORREPORTER_H
