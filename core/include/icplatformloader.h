#ifndef ICPLATFORMLOADER_H
#define ICPLATFORMLOADER_H

#include <QString>
#include <QList>
#include <QMap>
#include <QThread>
#include "icpluginloader.h"

/**
 * @brief Класс, реализующий загрузку платформы
 * @ingroup core
 */
class ICPlatformLoader : public QObject
{
    Q_OBJECT
public:
    ICPlatformLoader();
    ~ICPlatformLoader();
    ICPlatformLoader(QThread* mainThread);
    void loadPlatform(QString &group); ///< Загружает платформу.

signals:
    void platformLoaded(ICPluginLoader* loader); ///< сигнализирует о том, что платформа и все плагины загружены

private:
    ICPluginLoader* m_pl=nullptr;
    void loadInstances(const QStringList& sl);
    void setPluginContext(ICPluginInterface* pi, const QString& groupPrefix);
    quint64 calcAddrFromName(const QString& name);
    QThread* m_mainThread;
    int m_waitedThreads=0;
    QMutex m_waitedThreadsLocker;
    bool m_platformLoadedFlag = false;

private slots:
    void onPluginThreadStarted();
};

#endif //ICPLATFORMLOADER
