/**
 * @file icplugininterface.h
 * @brief Описание интерфейса плагинов
 * @copyright ivk-center.ru
 */

#ifndef ICPLUGININTERFACE_H
#define ICPLUGININTERFACE_H

#include "icversion.h"
#include <QThread>
#include <icplatformlayer.h>
#include <icretcode.h>
#include "icplatform_global.h"

/**
 * @brief Структура информации о плагине.
 * @ingroup core
 */
typedef struct _ICPluginInfo
{
    QString pluginName;        ///< имя плагина
    ICVersion pluginVersion;   ///< версия плагина
    ICVersion platformCompat;  ///< версия платформы, с которой совместим плагин
} ICPluginInfo;

/**
 * @brief Интерфейсный класс для клиентских и серверных плагинов.
 * @ingroup core
 */
class ICPLATFORMSHARED_EXPORT ICPluginInterface : public ICPlatformLayer
{
    Q_OBJECT
public:
    ICPluginInterface(QObject* parent=nullptr);
    virtual ~ICPluginInterface(){}

    virtual ICPluginInfo pluginInfo()=0;                ///< Возвращает имя и версию плагина
    void pushMessage(quint64 srcAddr, const QByteArray& data); ///< функция вызывается платформой, когда нужно передать сообщение плагину.
    void pushStart();                                   ///< функция вызывается платформой, когда модулю можно начинать работу.

public slots:
    void pushPluginsChanged();                           ///< вызывается платформой, когда меняется список доступных плагинов.
    ICRETCODE init(const QString& pref) override;        ///< Инициализация плагина. Здесь плагин как максимум может прочитать свои настройки из конфига, т.к. его контекст еще не установлен и с платформой он работать не может.
    virtual void uninit()=0;                             ///< Деинициализация плагина

protected slots:
    virtual void start()=0;                                          ///< Вызывается, когда модулю пора начинать работу
    virtual void processMessage(quint64 srcAddr, const QByteArray& data)=0; ///< Вызывается при поступлении сообщения плагину
    virtual void onPluginsChanged(){}                                ///< Вызывается при изменении списка плагинов системы (при установлении соединения с новым узлом, либо разрыве соединения). Кроме того, вызов этой функции подразумевает, что плагин может уже начать какую-то свою работу.

signals:
    void uninitialized();
    void message(quint64 srcAddr, const QByteArray& data);
    void pluginsChanged();
    void needstart();
};

Q_DECLARE_INTERFACE(ICPluginInterface, "ivk-center.ICPluginInterface/1.0")

#endif // ICPLUGININTERFACE_H
