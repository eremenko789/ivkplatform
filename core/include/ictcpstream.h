#ifndef ICTCPSTREAM_H
#define ICTCPSTREAM_H

#include"icnetwork_global.h"
#include "icnetworkstream.h"
#include <QTcpSocket>

/**
 * @brief Класс, реализующий интерфейс обмена данными по TCP-сокету.
 * @ingroup core
 */
class ICNETWORKSHARED_EXPORT ICTcpStream: public ICNetworkStream
{
    Q_OBJECT
public:
    ICTcpStream(QObject* parent = 0);
    virtual ~ICTcpStream();
    void initialize();
private slots:
    void onStateChanged(QAbstractSocket::SocketState state);
private:
    QAbstractSocket* socket()const{return m_socket;}
    QTcpSocket* m_socket;
};

#endif // ICTCPSTREAM_H
