#ifndef ICCONNECTIONFACTORY_H
#define ICCONNECTIONFACTORY_H

#include <icconnection.h>
#include <QList>
#include "icnetwork_global.h"
/**
  * class ICConnectionFactory
  * @brief Интерфейс фабрики подключений.
  * 
  * Описывает интерфейс классов, создающих подключения определенной конфигурации и
  * сообщающих об их создании подписчикам.
  * @ingroup core
  */
class ICNETWORKSHARED_EXPORT ICConnectionFactory : public ICObject
{
    Q_OBJECT
public:    
    ICConnectionFactory( QObject *parent=0) : ICObject(parent){}
    virtual  ~ICConnectionFactory ( ){}
public slots:
    /**
     * @brief Стартовать работу фабрики
     */
    virtual void start(){}
    /**
     * @brief Остановить работу фабрики
     */
    virtual void stop(){}
signals:
    /**
     * @brief Сигнал, сообщающий о создании нового подключения
     * @param  connection Созданное подключение
     */
    void newConnection (ICConnection* connection );
};

#endif // ICCONNECTIONFACTORY_H
