#ifndef ICDISCOVERYSESSIONPRIVATE_H
#define ICDISCOVERYSESSIONPRIVATE_H
#include "icbaselinkprivate.h"

namespace icrouting {

class ICDiscoveryLinkPrivate: public ICBaseLinkPrivate
{
    Q_OBJECT
public:

    ICDiscoveryLinkPrivate(ICBaseLink* s): ICBaseLinkPrivate(s){}
    void provideDiscoveryInfo(const vector<ICNetworkDetails>& addrList){emit discoveryInfoAvailable(addrList);}
signals:
    void discoveryInfoAvailable(const vector<ICNetworkDetails>& addrList);
};

}


#endif // ICDISCOVERYSESSIONPRIVATE_H
