#include <icerrornotifier.h>
#include <icerrorhandler.h>

ICErrorNotifier::ICErrorNotifier(QObject *parent)
    :QObject(parent)
{
    auto errorHandler = ICErrorHandler::instance();
    Q_ASSERT(errorHandler);
    connect(this, &ICErrorNotifier::error,errorHandler,&ICErrorHandler::onError);
}
