#include <icerrorhandler.h>
#include <QDebug>
#include <icobjectmacro.h>

IC_REGISTER_METATYPE(ICError)

ICErrorHandler ICErrorHandler::m_instance;

void ICErrorHandler::onError(ICError err)
{
    qDebug()<<err.message();
    //TODO: обработать ошибку
    // записать лог
    // для консольных приложений вывести сообщение в консоль
    // для GUI - вывести форму
    // сообщить вышестоящим обработчикам
    emit error(err);
}
