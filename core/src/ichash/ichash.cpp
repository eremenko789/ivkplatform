#include <ichash.h>

#include <QCryptographicHash>

uint icHash(const QString& str)
{
    QByteArray res = QCryptographicHash::hash(str.toUtf8(),QCryptographicHash::Md5); // 128 bits = 16 bytes
    uint ret=0;
    uchar* rdata = (uchar*)&ret;
    uchar *p0=rdata, *p1=rdata+1, *p2=rdata+2, *p3=rdata+3;
    *p0= res[0]^res[4]^res[8]^res[12];
    *p1= res[1]^res[5]^res[9]^res[13];
    *p2= res[2]^res[6]^res[10]^res[14];
    *p3= res[3]^res[7]^res[11]^res[15];
    return ret;
}
