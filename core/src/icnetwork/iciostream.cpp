#include <iciostream.h>
#include <QCoreApplication>

ICIOStream::ICIOStream(QObject* parent)
    :ICDataStream(parent)
{
    // Инициализировать таймер подключения
    m_connectTimer.setSingleShot(false);    // на всякий случай
    connect(&m_connectTimer,&QTimer::timeout, this, &ICIOStream::onConnectTimeout);
    connect(this,&ICIOStream::opened,&m_connectTimer, &QTimer::stop);
}

void ICIOStream::initialize()
{
    // подписать обработчик сигнала readyRead
    auto dev=device();
    Q_ASSERT(dev);
    connect(dev, &QIODevice::readyRead, this, &ICIOStream::readyRead);
}

bool ICIOStream::open(bool outgoing)
{
    if(!outgoing)
        return true;
    //Для исходящих подключений, стартовать таймер неответа
    bool ok = false;
    int timeout = setting("connectionPolicy/connectTimeout",g_defaultConnectTimeout).toInt(&ok);
    Q_ASSERT(ok);
    if(!ok)
        timeout = g_defaultConnectTimeout;
    m_connectTimer.start(timeout);
    return true;
}

quint64 ICIOStream::write(const QByteArray &data)
{
    auto dev=device();
    Q_ASSERT(dev);
    if(!dev)
        return 0;
    return dev->write(data);
}

QByteArray ICIOStream::read(quint64 bytesToRead)
{
    auto dev=device();
    Q_ASSERT(dev);
    if(!dev)
        return QByteArray();
    if(bytesToRead==0)
        return dev->readAll();
    return dev->read(bytesToRead);
}

void ICIOStream::onConnectTimeout()
{
    close();
    emit openTimeout();
}
