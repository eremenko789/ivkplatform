include(../../../platform_common.pri)

TARGET = icnetwork
TEMPLATE = lib
CONFIG += plugin
QT-= gui
QT += network
CONFIG += crypto

HEADERS += \
    $$CORE_INCLUDE/ictcpstream.h \
    $$CORE_INCLUDE/ictcpserver.h \
    $$CORE_INCLUDE/ictcpclientsloader.h \
    $$CORE_INCLUDE/ickeygenerator.h \
    $$CORE_INCLUDE/icdiffiehellmankeygenerator.h \
    $$CORE_INCLUDE/icdatastream.h \
    $$CORE_INCLUDE/icdataencoder.h \
    $$CORE_INCLUDE/icconnectionfactory.h \
    $$CORE_INCLUDE/icconnection.h \
    $$CORE_INCLUDE/icchipherencoder.h \
    $$CORE_INCLUDE/iciostream.h \
    $$CORE_INCLUDE/icnetworkstream.h \
    $$CORE_INCLUDE/iccompressencoder.h \
    $$CORE_INCLUDE/sosemanuk.h \
    $$CORE_INCLUDE/icsecurestreamencoder.h \
    $$CORE_INCLUDE/icpacket.h \
    icnetwork_global.h

SOURCES += \
    icchipherencoder.cpp \
    iccompressencoder.cpp \
    icconnection.cpp \
    icdataencoder.cpp \
    icdatastream.cpp \
    icdiffiehellmankeygenerator.cpp \
    iciostream.cpp \
    ickeygenerator.cpp \
    icnetworkstream.cpp \
    icsecurestreamencoder.cpp \
    ictcpclientsloader.cpp \
    ictcpserver.cpp \
    ictcpstream.cpp \
    sosemanuk.cpp

win32:LIBS+=-lqca
!win32:LIBS += -lqca-qt5
LIBS += -L$$DESTDIR -licobject
INCLUDEPATH += C:\work\qca\include
INCLUDEPATH += C:\work\qca-build
QMAKE_LFLAGS += -Wl,--rpath=C:\work\qca-build\bin
LIBS += -LC:\work\qca-build\bin -lqca
DEFINES += ICNETWORK_LIBRARY
CONFIG += debug
