#include "icsecurestreamencoder.h"

ICSecureStreamEncoder::ICSecureStreamEncoder(QObject* parent)
    :ICDataEncoder(parent)
{
}

ICRETCODE ICSecureStreamEncoder::init(const QString &pref)
{
    ICRETCODE ret = ICDataEncoder::init(pref);
    m_chipherEncoder.init(pref);
    m_compressEncoder.init(pref);
    return ret;
}

void ICSecureStreamEncoder::setKey(const QByteArray &key)
{
    ICChipherEncoder* chipherEnc = chipherEncoder();
    if(chipherEnc)
        chipherEnc->setKey(key);
}

QByteArray ICSecureStreamEncoder::encode(const QByteArray &data)
{
    // сжать
    ICCompressEncoder* compressEnc = compressEncoder();
    QByteArray result;
    if(compressEnc)
        result = compressEnc->encode(data);
    // добавить префикс
    result.prepend("IC");
    // зашифровать
    ICChipherEncoder* chipherEnc = chipherEncoder();
    if(chipherEnc)
        result = chipherEnc->encode(result);
    return result;
}

QByteArray ICSecureStreamEncoder::decode(const QByteArray &data,bool* ok)
{
    // дешифровать
    ICChipherEncoder* chipherEnc = chipherEncoder();
    QByteArray result;
    if(chipherEnc)
        result = chipherEnc->decode(data,ok);
    if(ok!=nullptr)
        if(!(*ok))
        return QByteArray();
    // проверить префикс
    if(result.startsWith("IC"))
    {
        result.remove(0,2);
        // если проверка успешна, распаковать
        ICCompressEncoder* compressEnc = compressEncoder();
        if(compressEnc)
            result = compressEnc->decode(result,ok);
    }
    else if(ok!=nullptr)
        *ok = false;
    return result;
}
