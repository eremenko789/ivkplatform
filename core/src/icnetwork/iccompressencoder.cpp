#include "iccompressencoder.h"

#define IC_COMPRESSION_LEVEL    2   // степень сжатия

ICCompressEncoder::ICCompressEncoder(QObject* parent)
    : ICDataEncoder(parent)
{
}

QByteArray ICCompressEncoder::encode(const QByteArray &data)
{
    // сжать данные
    return qCompress(data,IC_COMPRESSION_LEVEL);
}

QByteArray ICCompressEncoder::decode(const QByteArray &data,bool *ok)
{
    if(ok)
        *ok=true;
    // распаковать данные
    return qUncompress(data);
}
