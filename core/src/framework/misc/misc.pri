
HEADERS += \
    $$CORE_INCLUDE/icpluginloader.h \
    $$CORE_INCLUDE/icplatform.h \
    $$CORE_INCLUDE/icplatformloader.h \
    $$CORE_INCLUDE/icplatform_global.h

SOURCES += \
    misc/icplatform.cpp \
    misc/icplatformloader.cpp \
    misc/icpluginloader.cpp

DEFINES += ICPLATFORM_LIBRARY
