#include <icpluginloader.h>
#include <iclocalsettings.h>
#include <QDebug>

ICPluginLoader::~ICPluginLoader()
{
    for(auto pl:m_plugins.keys())
    {
        auto ldr = m_plugins.value(pl);
        if(ldr->instance()==pl)
            unloadPlugin(pl);
        else
            pl->deleteLater();
    }
}

ICPluginInterface* ICPluginLoader::loadPlugin(const QString &plname)
{
    QPluginLoader* pl = new QPluginLoader(plname);
    bool pluginWasLoaded = pl->isLoaded();
    ICPluginInterface* pi = qobject_cast<ICPluginInterface*>(pl->instance());
    if(pi != nullptr)
    {
        if(pluginWasLoaded) // если этот модуль уже был загружен то pl->instance() нам вернет ранее созданный объект, а нам нужен новый.
            pi = qobject_cast<ICPluginInterface*>(pi->metaObject()->newInstance());
        if(pi)
            m_plugins[pi] = pl;
        else
            qDebug()<<"cannot create another plugin instance, probably Q_INVOKABLE constructor missing";
    }
    else
    {
        qDebug()<<"can't load plugin:"<<plname<<pl->errorString();
        delete pl;
    }
    return pi;
}

bool ICPluginLoader::unloadPlugin(ICPluginInterface* pi)
{
    bool res = true;
    if(m_plugins.contains(pi))
    {
        QPluginLoader* loader = m_plugins.take(pi);
        if(loader->isLoaded())
            res = loader->unload();
        delete loader;
    }
    else
        qDebug()<<"Plugin is not loaded!\n";
    return res;
}

QStringList ICPluginLoader::pluginPathsForLoading(const QString &group)
{
    ICLocalSettings sett;
    sett.beginGroup("Plugins");
    if(!group.isEmpty())
        sett.beginGroup(group);
    QStringList plugins = sett.childGroups();
    QStringList pluginPaths;
    for(auto pl = plugins.begin(); pl!=plugins.end(); pl++)
    {
        sett.beginGroup(*pl);
        QString path = sett.value("path",QVariant("")).toString();
        pluginPaths.append(path);
        sett.endGroup();
    }
    return pluginPaths;
}

QStringList ICPluginLoader::pluginsForLoading(const QString &group)
{
    ICLocalSettings sett;
    sett.beginGroup("Plugins");
    if(!group.isEmpty())
        sett.beginGroup(group);
    QStringList plugins = sett.childGroups();
    return plugins;
}

QList<ICPluginInterface*> ICPluginLoader::loadedPlugins()
{
    return m_plugins.keys();
}
