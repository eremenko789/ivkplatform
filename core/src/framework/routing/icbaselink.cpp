#include "icbaselink.h"
#include "icsessionstate.h"
#include "icconnection.h"
#include "iclocker.h"
#include "icgridmanager.h"

namespace icrouting {

ICBaseLink::ICBaseLink(ICBaseLinkPrivate*priv)
{
    m_priv = priv;
    connect(m_priv, &ICBaseLinkPrivate::nodeUp, this, &ICBaseLink::nodeUp);
    connect(m_priv, &ICBaseLinkPrivate::nodeDown, this, &ICBaseLink::nodeDown);
    connect(m_priv, &ICBaseLinkPrivate::closed, this, &ICBaseLink::closed);
    connect(m_priv, &ICBaseLinkPrivate::aborted, this, &ICBaseLink::aborted);
    connect(m_priv, &ICBaseLinkPrivate::pluginsChanged, this, &ICBaseLink::pluginsChanged);
    connect(m_priv, &ICBaseLinkPrivate::pluginMessage, this, &ICBaseLink::pluginMessage);
}

ICBaseLink::~ICBaseLink()
{
    m_priv->deleteLater();
}

bool operator ==(const ICBaseLink& first,const ICBaseLink& second)
{
    return (first.serverDetails() == second.serverDetails());
}

std::size_t hash_value(const ICBaseLink &session)
{
    return qHash(session.serverDetails());
}

}


