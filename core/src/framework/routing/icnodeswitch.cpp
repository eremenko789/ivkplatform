#include "icnodeswitch.h"
#include "QReadLocker"
#include "QWriteLocker"
#include "iclocker.h"

namespace icrouting {

ICNodeSwitch::ICNodeSwitch(QObject *parent):ICObject(parent)
{

}


QList<quint64> ICNodeSwitch::pluginIDs(QString pluginName, quint64 senderID)const
{
    QList<quint64> pluginIDsList;

    QReadLocker rlocker(ICLocker::routeTableLock());
    auto nodeList = m_routeTable.values();
    for(auto node : nodeList)
    {
        QList<quint32> pluginType = node->pluginTypeID(pluginName);
        if(!pluginType.isEmpty())
        {
            quint64 pluginID = node->id();
            pluginID <<= 32;
            pluginID |= pluginType.at(0);
            if(pluginID != senderID)
                pluginIDsList<<pluginID;
        }
    }
    return pluginIDsList;
}

QList<quint64> ICNodeSwitch::pluginIDs(quint64 senderID)const
{
    QList<quint64> pluginIDsList;

    QReadLocker rlocker(ICLocker::routeTableLock());
    auto nodeList = m_routeTable.values();

    for(auto node : nodeList)
    {
        QList<quint32> pluginTypeIDs = node->pluginTypeIDs();
        quint64 nodeID = node->id();
        nodeID <<= 32;

        for(quint32 pluginTypeID : pluginTypeIDs)
        {
            quint64 pluginID = nodeID;
            pluginID |= pluginTypeID;
            if(pluginID != senderID)
                pluginIDsList<<pluginID;
        }
    }
    return pluginIDsList;
}

QString ICNodeSwitch::nodeName(quint64 pluginID)const
{
    quint32 nodeID = pluginID>>32;

    QReadLocker rlocker(ICLocker::routeTableLock());
    auto node = m_routeTable.value(nodeID);
    if(!node)
        return QString();
    return m_routeTable.value(nodeID)->name();

}

QString ICNodeSwitch::pluginName(quint64 pluginID)const
{
    QReadLocker rlocker(ICLocker::routeTableLock());
    auto nodeList = m_routeTable.values();

    for(auto node : nodeList)
    {
        quint32 pluginTypeID = pluginID;
        QString pluginName = node->pluginName(pluginTypeID);
        if(!pluginName.isEmpty())
            return pluginName;
    }
    return QString();
}


void ICNodeSwitch::addNode(const ICAbstractNode*node)
{
    QWriteLocker wlocker(ICLocker::routeTableLock());
    m_routeTable.insert(node->id(),node);
}

void ICNodeSwitch::removeNode(const ICAbstractNode*node)
{
    QWriteLocker wlocker(ICLocker::routeTableLock());
    Q_ASSERT(m_routeTable.contains(node->id()));
    m_routeTable.remove(node->id());
}

void ICNodeSwitch::route(quint64 src, quint64 dest, const QByteArray& message)const
{
    const ICAbstractNode* node = m_routeTable.value(dest>>32);
    if(node)
        node->deliverMessage(src,dest,message);
}

bool ICNodeSwitch::isRoutable(quint64 dest)const
{
    QReadLocker rlocker(ICLocker::routeTableLock());
    const ICAbstractNode* node = m_routeTable.value(dest>>32);
    if(node)
        return node->hasPlugin(dest);
    return false;
}


}
