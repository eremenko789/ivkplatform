#include "iclocalnode.h"
#include "iclocker.h"
#include "QWriteLocker"
#include <ichash.h>

namespace icrouting {


ICLocalNode::ICLocalNode(QObject *parent):ICAbstractNode(parent)
{

}

ICLocalNode::CollisionStatus ICLocalNode::checkNodeNameOrHashCollisions(QString nodeName)const
{
    Q_UNUSED(nodeName);
    return CollisionStatus();
}

ICNodeInfo ICLocalNode::info()const
{
    ICNodeInfo ad;
    ad.m_nodeName = m_name;
    QStringList pluginNames;
    for(ICPluginInterface*plugin : m_localPlugins.values())
        pluginNames<<plugin->context()->pluginName();
    ad.m_pluginNames = pluginNames;
    return ad;
}

void ICLocalNode::addPlugins(const QList<ICPluginInterface *> & list)
{
    QWriteLocker wlocker(ICLocker::routeTableLock());
    for(ICPluginInterface *plugin : list)
        m_localPlugins.insert(icHash(plugin->context()->pluginName()), plugin);
}

void ICLocalNode::removePlugins(const QList<ICPluginInterface *> & list)
{
    QWriteLocker wlocker(ICLocker::routeTableLock());
    for(ICPluginInterface *plugin : list)
        m_localPlugins.remove(icHash(plugin->context()->pluginName()));
}


//virtuals
QList<quint32> ICLocalNode::pluginTypeIDs()const
{
    return m_localPlugins.keys();
}

QList<quint32> ICLocalNode::pluginTypeID(QString name)const
{
    QList<quint32> pId;
    return pId<<icHash(name);
}



QString ICLocalNode::pluginName(quint32 pluginID)const
{
    return m_localPlugins.value(pluginID)->pluginInfo().pluginName;
}

bool ICLocalNode::hasPlugin(quint32 pluginID) const
{
    return m_localPlugins.contains(pluginID);
}

void ICLocalNode::deliverMessage(quint64 srcPluginID, quint64 destPluginID, const QByteArray& message)const
{
    quint32 destId = destPluginID;
    ICPluginInterface* destPlugin = m_localPlugins.value(destId);
    if(destPlugin)
        destPlugin->pushMessage(srcPluginID, message);
}

}


