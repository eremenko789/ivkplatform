#include "iclocker.h"

namespace icrouting {

QScopedPointer<QReadWriteLock> ICLocker::m_routeTableLock;

ICLocker::ICLocker()
{

}

QReadWriteLock* ICLocker::routeTableLock()
{
    if(!m_routeTableLock)
    {
        m_routeTableLock.reset(new QReadWriteLock);
        return m_routeTableLock.data();
    }
    return m_routeTableLock.data();
}

}
