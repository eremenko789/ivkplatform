import qbs 1.0
import "../icmodule.qbs" as ICModule
import "../iclibrary.qbs" as ICLibrary

ICModule {
    id: core
    name: "core"
    pubincludes: "include/"
    ICLibrary {
        name: "ivkplatform"
        incpath: pubincludes
        property string miscsrc: "src/framework/misc/"
//        property string netwsrc: "src/framework/network/"
        property string pintsrc: "src/framework/plugininterfaces/"
        property string routsrc: "src/framework/routing/"
        files: [
            miscsrc+"*.cpp",
            pintsrc+"*.cpp",
            routsrc+"*.cpp",
// базовое
            incpath+"ictyperegistrator.h",
            incpath+"icpluginloader.h",
            incpath+"icplatform.h",
            incpath+"icplatformloader.h",
            incpath+"icplatform_global.h",
// интерфейс модулей
            incpath+"icplatformlayer.h",
            incpath+"icplugincontext.h",
            incpath+"icplugininterface.h",
            incpath+"icversion.h",
            incpath+"icretcode.h",
// маршрутизация
            incpath+"icrouter.h",
        incpath+"icmessages.h",
            incpath+"icgridmanager.h",
            incpath+"icnodeswitch.h",
            incpath+"iclocalnode.h",
            incpath+"icremotenode.h",
            incpath+"icnetworkdetails.h",
            incpath+"icabstractnode.h",
            incpath+"iclocker.h",
        incpath+"utilities.h",

        incpath+"icsessionstate.h",
        incpath+"icsessionmap.h",
        incpath+"icbaselinkprivate.h",
        incpath+"icdiscoverylink.h",
        incpath+"icdiscoverylinkprivate.h",
        incpath+"icprimarylink.h",
        incpath+"icprimarylinkprivate.h",
        incpath+"icserverlink.h",
        incpath+"icserverlinkprivate.h",
        incpath+"icsimplelink.h",
        incpath+"icsimplelinkprivate.h",
        incpath+"icbaselink.h"
        ]
        cpp.defines: ["ICPLATFORM_LIBRARY"]
        Depends { name: "Qt.network" }
        Depends { name: "icobject"}
        Depends { name: "icsettings"}
        Depends { name: "icnetwork"}
        Depends {name: "ichash"}
        cpp.includePaths: qbs.targetOS.contains("windows")?[incpath, icobj.includes, "C:/Boost"]:[incpath]
    }

    ICLibrary {
        name: "icsettings"
        incpath: pubincludes
        srcpath: "src/icsettings/"
        files: [srcpath+"iclocalsettings.cpp",
            incpath+"iclocalsettings.h",
            incpath+"icsettings.h",
            incpath+"icsettings_global.h"
        ]
        cpp.includePaths: [incpath]
        cpp.defines: ["ICSETTINGS_LIBRARY"]
    }

    ICLibrary {
        id: icobj
        name: "icobject"
        incpath: pubincludes
        srcpath: "src/icobject/"
        files: [
            srcpath+"icobject.cpp",
            srcpath+"icerrorhandler.cpp",
            srcpath+"icerrornotifier.cpp",
            srcpath+"icobjectsettings.cpp",
            incpath+"icerrorcode.h",
            incpath+"icobject.h",
            incpath+"icobject_global.h",
            incpath+"icerror.h",
            incpath+"icerrorhandler.h",
            incpath+"icerrornotifier.h",
            incpath+"icobjectmacro.h",
            incpath+"icobjectsettings.h",
            incpath+"icerrorreporter.h"
        ]
        Depends { name: "icsettings" }
        cpp.defines: ["ICOBJECT_LIBRARY"]
    }

    ICLibrary {
        name: "icnetwork"
        incpath: pubincludes
        srcpath: "src/icnetwork/"
        files: [
            incpath+"ic*encoder.h",
            incpath+"icconnection.h",
            incpath+"icconnectionfactory.h",
            incpath+"icdatastream.h",
            incpath+"icdiffiehellmankeygenerator.h",
            incpath+"iciostream.h",
            incpath+"ickeygenerator.h",
            incpath+"icnetworkstream.h",
            incpath+"ictcpclientsloader.h",
            incpath+"ictcpserver.h",
            incpath+"ictcpstream.h",
            incpath+"sosemanuk.h",
            incpath+"icnetwork_global.h",
            srcpath+"*.cpp"
        ]
        Depends { name: "icobject"}
        Depends { name: "Qt.network" }
        cpp.dynamicLibraries: qbs.targetOS.contains("linux")?["qca-qt5"]:["qca"]
        cpp.defines: ["ICNETWORK_LIBRARY"]
    }
    ICLibrary {
        name: "ichash"
        incpath: pubincludes
        srcpath: "src/ichash/"
        files: [
            incpath+"ichash.h",
            srcpath+"ichash.cpp"
        ]
//        Depends { name: "Qt.core"}
        cpp.defines: ["ICHASH_LIBRARY"]
    }
}
