#include "localsettings.h"
#include <QString>
#include <stdlib.h>


void LocalSettings::initTestCase()
{
    writeTestSettings();
    ICLocalSettings sett;
    QString fn = sett.fileName();
    qDebug() << fn;
}

void LocalSettings::cleanupTestCase()
{
    ICLocalSettings* sett = new ICLocalSettings();
    QString fn = sett->fileName();
    delete sett;
    QFile::remove(fn);
}

void LocalSettings::writeTestSettings()
{
    QFile f(QDir::homePath().append("/.config/Unknown Organization/localsettings.ini"));
    f.open(QIODevice::WriteOnly);
    f.write(QString("[Include]\n\
                    inc1=%1/.config/Unknown Organization/1.ini\n\
                    inc2=%1/.config/Unknown Organization/2.ini\n\
                    [General]\nval1=1\n\n[group1]\nval3=3\n").arg(QDir::homePath()).toUtf8());
    f.close();
    QFile f1(QDir::homePath().append("/.config/Unknown Organization/1.ini"));
    f1.open(QIODevice::WriteOnly);
    f1.write(QString("[General]\nval2=2\n").arg(QDir::homePath()).toUtf8());
    f1.close();
    QFile f2(QDir::homePath().append("/.config/Unknown Organization/2.ini"));
    f2.open(QIODevice::WriteOnly);
    f2.write(QString("[General]\nval4=4\n").arg(QDir::homePath()).toUtf8());
    f2.close();
    f2.setPermissions(QFileDevice::ReadOwner);
    m_etalons["val1"] = 1;
    m_etalons["val2"] = 2;
    m_etalons["val3"] = 3;
    m_etalons["val4"] = 4;
}

void LocalSettings::testCreatingFile()
{
    ICLocalSettings* icls = new ICLocalSettings("ivk-center", "iclocalsettings");
    QString fn = icls->fileName();
    delete icls;
    QFile::remove(fn);
    icls = new ICLocalSettings("ivk-center", "iclocalsettings");
    icls->setValue("val", QVariant(3));
    delete icls;
    QFile f(fn);
    QVERIFY(f.exists());
    QFile::remove(fn);
}

void LocalSettings::testAllKeys()
{
    ICLocalSettings file;
    QStringList vals = file.allKeys();
    QCOMPARE(vals.size(), 6);
}
    
void LocalSettings::testChildGroups()
{
    ICLocalSettings file;
    QStringList vals = file.childGroups();
    QCOMPARE(vals.size(), 2);
    QCOMPARE(vals[1], QString::fromUtf8("group1"));
}

void LocalSettings::testChildKeys()
{
    ICLocalSettings file;
    QStringList vals = file.childKeys();
    QCOMPARE(vals.size(), 3);
    QCOMPARE(file.value(vals[0], 0).toInt(), m_etalons[vals[0]]);
    QCOMPARE(file.value(vals[1], 0).toInt(), m_etalons[vals[1]]);
}

void LocalSettings::testChildKeysInGroup()
{
    ICLocalSettings file;
    file.beginGroup("group1");
    QStringList vals = file.childKeys();
    QCOMPARE(vals.size(), 1);
    QCOMPARE(vals[0], QString::fromUtf8("val3"));
    QCOMPARE(file.value(vals[0], 0).toInt(), m_etalons[vals[0]]);
}

void LocalSettings::testAddRemoveKey()
{
    ICLocalSettings file;
    file.remove("val1");
    QStringList vals = file.childKeys();
    QCOMPARE(vals.size(), 2);
    file.setValue("val1", m_etalons["val1"]);
    vals = file.childKeys();
    QCOMPARE(vals.size(), 3);
}

void LocalSettings::testReadOnlyKey()
{
    ICLocalSettings file;
    file.remove("val4");
    QStringList vals = file.childKeys();
    QCOMPARE(vals.size(), 3);
    file.setValue("val4", m_etalons["val4"]+1);
    QCOMPARE(file.value("val4").toInt(), m_etalons["val4"]);
    vals = file.childKeys();
    QCOMPARE(vals.size(), 3);
}

QTEST_MAIN(LocalSettings)
#include "moc_localsettings.cpp"
