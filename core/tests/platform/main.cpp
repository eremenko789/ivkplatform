#include <QCoreApplication>
#include <stdio.h>
#include <QThread>
#include <icplatformloader.h>
#include <icplatform.h>
#include <QDebug>
#include <icpluginloader.h>
#include <QFile>
#include <QTextStream>

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    app.setOrganizationName("ivk-center");
    printf("Platform sample\n");
    ICPlatform p;
    app.exec();
}
