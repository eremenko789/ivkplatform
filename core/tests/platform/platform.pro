QT += core

TARGET=platform

TEMPLATE=app

PLATFORM_DIR = "../.."
PLATFORM_INC = $$PLATFORM_DIR/include
PLATFORM_SRC = $$PLATFORM_DIR/src

INCLUDEPATH += $$PLATFORM_DIR/include

HEADERS += $$PLATFORM_INC/icplugininterface.h \
           $$PLATFORM_INC/icplatformlayer.h \
           $$PLATFORM_INC/icplatformloader.h \
           $$PLATFORM_INC/iclocalsettings.h \
           $$PLATFORM_INC/icpluginloader.h \
           $$PLATFORM_INC/icplugincontext.h \
           $$PLATFORM_INC/icplatform.h

SOURCES += main.cpp \
           $$PLATFORM_SRC/icplatformlayer.cpp \
           $$PLATFORM_SRC/icplatformloader.cpp \
           $$PLATFORM_SRC/iclocalsettings.cpp \
           $$PLATFORM_SRC/icpluginloader.cpp \
           $$PLATFORM_SRC/icplugininterface.cpp \
           $$PLATFORM_SRC/icplatform.cpp

QMAKE_CXXFLAGS += -std=c++0x

QMAKE_LFLAGS += -Wl,--no-undefined
