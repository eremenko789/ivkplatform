#-------------------------------------------------
#
# Project created by QtCreator 2014-03-14T15:23:58
#
#-------------------------------------------------

#QT       -= gui
CONFIG += plugin

TARGET = ictestplugin
TEMPLATE = lib

DEFINES += ICTESTPLUGIN_LIBRARY

SOURCES += ictestplugin.cpp

HEADERS += ictestplugin.h\
        ictestplugin_global.h

LIBS += -L$$PLATFORM_DIR/bin -livkplatform -licobject -licsettings

unix {
    target.path = /usr/lib
    INSTALLS += target
}

