QT += testlib

HEADERS += \
    icnodeswitchtest.h \
    ../../include/icnodeswitch.h \
    icmocknode.h \
    icnodetest.h \
    ../../include/icabstractnode.h \
    ../../include/icremotenode.h \
    ../../include/icobject.h \
    ../../include/icerror.h \
    ../../include/icerrorhandler.h \
    ../../include/icerrorprovider.h \
    ../../include/iclocalsettings.h \
    icmockplugininterface.h \
    ichandshaketest.h \
    icmockconnection.h \
    ../../include/ichandshake.h \
    ../../include/icwritecriticalsection.h \
    ../../include/icreadcriticalsection.h \
    ../../include/icsemaphore.h \
    ../../include/icmessages.h

SOURCES += \
    ../../src/routing/icnodeswitch.cpp \
    ../../src/routing/icremotenode.cpp \
    icroutingtestmain.cpp \
    ../../src/misc/icobject.cpp \
    ../../src/misc/icerrorhandler.cpp \
    ../../src/misc/icerrorprovider.cpp \
    ../../src/misc/iclocalsettings.cpp \
    icnodetest.cpp \
    icnodeswitchtest.cpp \
    ../../../../../../usr/src/gmock/src/gmock-all.cc \
    ../../../../../../usr/src/gtest/src/gtest-all.cc \
    ichandshaketest.cpp \
    ../../src/routing/icwritecriticalsection.cpp \
    ../../src/routing/icreadcriticalsection.cpp \
    ../../src/routing/icsemaphore.cpp \
    ../../src/routing/icmessages.cpp

INCLUDEPATH += /usr/src/gmock
INCLUDEPATH += /usr/src/gtest
