#include "icnodeswitchtest.h"
#include "QTest"

using ::testing::Return;

using ::testing::_;

using ::testing::DefaultValue;

using ::testing::NiceMock;

namespace icrouting {

const quint64 FAKE_SENDER = 0xFFFFFFFF;

void ICNodeSwitchTest::cleanup()
{
    foreach(ICAbstractNode*node,nodeList)
        nodeSwitch.removeNode(node);
    qDeleteAll(nodeList);
    nodeList.clear();
}

void ICNodeSwitchTest::testNodeName()
{
    testExistingNodeName();
    cleanup();
    testNonExistingNodeName();
}

void ICNodeSwitchTest::testExistingNodeName()
{
    for(quint32 i = 0; i < numNodes; ++i)
    {
        ICMockNode* node = new ICMockNode;
        nodeList<<node;
        EXPECT_CALL(*node, id()).WillRepeatedly(Return(i));
        EXPECT_CALL(*node, name()).WillOnce(Return(QString("node%1").arg(i)));
        nodeSwitch.addNode(node);
    }

    for(quint32 i = 0; i < numNodes; ++i)
    {
        QString nameToSearch("node%1");
        nameToSearch = nameToSearch.arg(i);
        quint64 pluginID = i;
        pluginID = pluginID<<32;
        QString name = nodeSwitch.nodeName(pluginID);
        QVERIFY(name == nameToSearch);
    }
}

void ICNodeSwitchTest::testNonExistingNodeName()
{
    quint64 pluginID = 0xFFFF;
    QString name = nodeSwitch.nodeName(pluginID);
    QVERIFY(name == "");
}

void ICNodeSwitchTest::testPluginIDs()
{
    QList<quint32> ids;
    QList<quint64> idsList;
    int gapDelta = -1;
    int gap = 0;
    for(quint32 i = 0; i < numNodes; ++i)
    {
        ICMockNode* node = new ICMockNode;
        quint32 startingNum = i + gap;
        quint64 nodeID = i;
        nodeID <<= 32;

        for(quint32 c = startingNum; c < startingNum + (i + 1); ++c)
        {
            quint64 pluginID = nodeID;
            ids<<c;
            pluginID |= c;
            idsList<<pluginID;
        }
        gapDelta++;
        gap += gapDelta;
        nodeList<<node;
        EXPECT_CALL(*node, id()).WillRepeatedly(Return(static_cast<quint32>(i)));
        EXPECT_CALL(*node, pluginTypeIDs()).WillOnce(Return(ids));
        nodeSwitch.addNode(node);
        ids.clear();
    }

    QList<quint64> idsToVerify = nodeSwitch.pluginIDs(FAKE_SENDER);
    QVERIFY(idsList.count() == idsToVerify.count());

    for(quint64 id : idsList)
    {
        QVERIFY2(idsToVerify.contains(id), QString("No match for id = %1").arg(id).toLatin1().constData());
    }
}

void ICNodeSwitchTest::testPluginIDsByName()
{
    QList<quint64> idsList;
    for(quint32 i = 0; i < numNodes; ++i)
    {
        ICMockNode* node = new ICMockNode;
        nodeList<<node;

        EXPECT_CALL(*node, id()).WillRepeatedly(Return(i));
        if(i%2)
        {
            quint64 pluginID = i;
            quint32 pluginTypeID = 0xFFFF;
            pluginID <<= 32;
            pluginID |= pluginTypeID;
            idsList<<pluginID;

            QList<quint32> list;
            list<<pluginTypeID;
            EXPECT_CALL(*node, pluginTypeID(_)).WillOnce(Return(list));
        }
        else
            EXPECT_CALL(*node, pluginTypeID(_)).WillOnce(Return(QList<quint32>()));
        nodeSwitch.addNode(node);
    }

    QList<quint64> idsToVerify = nodeSwitch.pluginIDs("dummy",FAKE_SENDER);
    QVERIFY(idsList.count() == idsToVerify.count());

    for(quint64 id : idsList)
    {
        QVERIFY2(idsToVerify.contains(id), QString("No match for id = %1").arg(id).toLatin1().constData());
    }
}


void ICNodeSwitchTest::testPluginName()
{
    testExistingPluginName();
    cleanup();
    testNonExistingPluginName();
}

void ICNodeSwitchTest::testExistingPluginName()
{
    quint64 pluginID;
    for(quint32 i = 0; i < numNodes; ++i)
    {
        NiceMock<ICMockNode>* node= new NiceMock<ICMockNode>;
        nodeList<<node;
        EXPECT_CALL(*node, id()).WillRepeatedly(Return(i));
        if(i == TEST_NODE_ID)
        {
            pluginID = i;
            pluginID <<= 32;
            pluginID |= 0xFFFF;
            EXPECT_CALL(*node, pluginName(0xFFFF)).WillOnce(Return(QString("name")));
        }
        else
            ON_CALL(*node,pluginName(_)).WillByDefault(Return(QString("")));
        nodeSwitch.addNode(node);
    }
    QString pluginName = nodeSwitch.pluginName(pluginID);
    QVERIFY(pluginName == "name");
}

void ICNodeSwitchTest::testNonExistingPluginName()
{
    for(quint32 i = 0; i < numNodes; ++i)
    {
        ICMockNode* node = new ICMockNode;
        nodeList<<node;
        EXPECT_CALL(*node, id()).WillRepeatedly(Return(i));
        EXPECT_CALL(*node, pluginName(_)).WillOnce(Return(QString("")));
        nodeSwitch.addNode(node);
    }
    QString pluginName = nodeSwitch.pluginName(0xFFFF);
    QVERIFY(pluginName == "");
}

void ICNodeSwitchTest::testRoute()
{
    testExistingRoute();
    cleanup();
    testNonExistingRoute();
}

void ICNodeSwitchTest::testExistingRoute()
{
    quint64 destPluginID;
    for(quint32 i = 0; i < numNodes; ++i)
    {
        ICMockNode* node= new ICMockNode;
        nodeList<<node;
        EXPECT_CALL(*node, id()).WillRepeatedly(Return(i));
        if(i == TEST_NODE_ID)
        {
            destPluginID = i;
            destPluginID <<= 32;
            destPluginID |= 0xFFFF;
            EXPECT_CALL(*node, deliverMessage(1,destPluginID,QByteArray())).Times(1);
            EXPECT_CALL(*node, hasPlugin(destPluginID)).WillOnce(Return(true));
        }
        nodeSwitch.addNode(node);
    }
    quint64 src = 1;
    QVERIFY(nodeSwitch.isRoutable(destPluginID));
    nodeSwitch.route(src, destPluginID, QByteArray());
}

void ICNodeSwitchTest::testNonExistingRoute()
{
    testNonExistingNodeRoute();
    cleanup();
    testNonExistingPluginRoute();
}

void ICNodeSwitchTest::testNonExistingNodeRoute()
{
    quint64 src = 1;
    quint64 nonExistingNodeDest = 15;
    nonExistingNodeDest <<=32;
    quint64 nonExistingPluginDest = 5;
    nonExistingPluginDest <<= 32;
    for(quint32 i = 0; i < numNodes; ++i)
    {
        ICMockNode* node= new ICMockNode;
        nodeList<<node;
        EXPECT_CALL(*node, id()).WillRepeatedly(Return(i));
        EXPECT_CALL(*node, deliverMessage(src,nonExistingNodeDest,QByteArray())).Times(0);
        EXPECT_CALL(*node, hasPlugin(nonExistingNodeDest)).Times(0);
        nodeSwitch.addNode(node);
    }
    QVERIFY(!nodeSwitch.isRoutable(nonExistingNodeDest));
    nodeSwitch.route(src, nonExistingNodeDest, QByteArray());
}

void ICNodeSwitchTest::testNonExistingPluginRoute()
{
    quint64 nonExistingPluginDest = 5;
    nonExistingPluginDest <<= 32;
    quint64 src = 1;
    for(quint32 i = 0; i < numNodes; ++i)
    {
        ICMockNode* node= new ICMockNode;
        nodeList<<node;
        EXPECT_CALL(*node, id()).WillRepeatedly(Return(i));
        if(i == TEST_NODE_ID)
        {
            EXPECT_CALL(*node, hasPlugin(nonExistingPluginDest)).WillOnce(Return(false));
            EXPECT_CALL(*node, deliverMessage(src,nonExistingPluginDest,QByteArray())).Times(1);
        }
        nodeSwitch.addNode(node);
    }
    QVERIFY(!nodeSwitch.isRoutable(nonExistingPluginDest));
    nodeSwitch.route(src, nonExistingPluginDest, QByteArray());
}


}
