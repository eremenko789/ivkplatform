#include <QCoreApplication>
#include <QtTest/QTest>
#include "icnodeswitchtest.h"
#include "icmocknode.h"


int main(int argc,char* argv[])
{
    QCoreApplication app(argc,argv);
    ::testing::GTEST_FLAG(throw_on_failure) = true;
    ::testing::InitGoogleMock(&argc, argv);


    icrouting::ICNodeSwitchTest nswtest;
    QTest::qExec(&nswtest);

}
