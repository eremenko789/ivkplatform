#include <QCoreApplication>
#include <icplatform.h>
#include <iclocalsettings.h>
#include <QDir>
#include <QtTest/QTest>
#include <icobjecttest.h>
#include <globalsettingstest.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    a.setOrganizationName("ivk-center");
    //create config
    ICLocalSettings sett;
    sett.setValue("separateThread",true);
    sett.beginGroup("Plugins/test");
    auto dirname = QDir::currentPath();
    sett.setValue("client/separateThread",true);
    sett.setValue("client/path",dirname+"/plugins/libicsettingsclient.so");
    sett.setValue("server/separateThread",true);
    sett.setValue("server/path",dirname+"/plugins/libicsettingsserver.so");
    sett.endGroup();
    ICPlatform platform;
    Q_UNUSED(platform)
    GlobalSettingsTest gsTest;
    QTest::qExec(&gsTest);
    ICObjectTest objTest;
    return QTest::qExec(&objTest);
}
