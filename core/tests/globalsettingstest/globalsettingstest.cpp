#include "globalsettingstest.h"
#include <QtTest/QTest>
#include <testconst.h>

GlobalSettingsTest::GlobalSettingsTest(QObject *parent) :
    ICObject(parent)
{
    m_manager = new GlobalSettingsManager();
    m_manager->init(SETTINGS_GROUP);
    connect(this,&GlobalSettingsTest::scheduleWritting,m_manager,&GlobalSettingsManager::write,Qt::QueuedConnection);
    m_anotherThread = new QThread();
    //перенести в фоновый поток класс-менеджер
    m_manager->moveToThread(m_anotherThread);
}

GlobalSettingsTest::~GlobalSettingsTest()
{
    m_manager->deleteLater();
}

void GlobalSettingsTest::initTestCase()
{
    init(SETTINGS_GROUP);
    QTRY_VERIFY_WITH_TIMEOUT(QMetaType::type("ICGlobalSettings")!=QMetaType::UnknownType,3000);
    QThread::msleep(1000);
    //стартовать фоновый поток
    m_anotherThread->start();
}


void GlobalSettingsTest::cleanupTestCase()
{
    //остановить фоновый поток
    if(m_anotherThread!=nullptr)
    {
        m_anotherThread->exit();
        QTRY_VERIFY_WITH_TIMEOUT(m_anotherThread->isFinished(),1000);
        m_anotherThread->deleteLater();
    }
}

void GlobalSettingsTest::testSyncRead()
{
    //стартовать таймер, по истечении которого в фоновом потоке изменится тестовая настройка
    emit scheduleWritting();
    //вызвать синхронный метод запроса тестовой настройки
    auto val = setting(SETTING_NAME,INIT_STATUS,false);
    QCOMPARE(val.toString(),QString(READY_STATUS));
}

#include <moc_globalsettingstest.cpp>
