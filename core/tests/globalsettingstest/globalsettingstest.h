#ifndef GLOBALSETTINGSTEST_H
#define GLOBALSETTINGSTEST_H

#include <icobject.h>
#include <QThread>
#include <globalsettingsmanager.h>

class GlobalSettingsTest : public ICObject
{
    Q_OBJECT
public:
    explicit GlobalSettingsTest(QObject *parent = 0);
    ~GlobalSettingsTest();

    // test slots:
private slots:
    void initTestCase();
    void testSyncRead();
    void cleanupTestCase();
signals:
    void scheduleWritting();
private:
    // members:
    GlobalSettingsManager* m_manager;
    QThread *m_anotherThread = nullptr;
};

#endif // GLOBALSETTINGSTEST_H
