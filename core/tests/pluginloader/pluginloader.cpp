#include "pluginloader.h"
#include <iclocalsettings.h>
#include "QString"

void PluginLoaderTest::initTestCase()
{
    m_pl = new ICPluginLoader();
}

void PluginLoaderTest::cleanupTestCase()
{
    if(m_pl != NULL)
	delete m_pl;
}

void PluginLoaderTest::pluginsForLoading()
{
    ICLocalSettings sett;
    sett.clear();
    sett.beginGroup("Plugins");
    sett.beginGroup("icluascript1");
    sett.setValue("path", "icluascript.so");
    sett.setValue("args","script.lua");
    sett.endGroup();
    sett.beginGroup("icluascript2");
    sett.setValue("path", "icluascript.so");
    sett.setValue("args","otherscript.lua");
    sett.endGroup();
    sett.beginGroup("icluascript3");
    sett.setValue("path", "icluascript.so");
    sett.setValue("args","newscript.lua");
    sett.sync();
    QString prefix("");
    QStringList plugins = ICPluginLoader::pluginsForLoading(prefix);
    QCOMPARE(plugins.size(), 3); 
}

void PluginLoaderTest::loadCallUnloadPlugin()
{
    ICPluginInterface* pi = m_pl->loadPlugin("./dummyplugin/dummyplugin.so");
    QVERIFY(pi != NULL);
    pi->init("");
    pi->pushMessage(0, QByteArray());
    pi->uninit();
    qDebug()<<"unloading plugin";
    bool res = m_pl->unloadPlugin(pi);
    qDebug()<<"plugin unloaded";
    QCOMPARE(res, true);
}

QTEST_MAIN(PluginLoaderTest)
#include "moc_pluginloader.cpp"
