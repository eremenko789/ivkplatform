#ifndef DUMMYPLUGIN_H
#define DUMMYPLUGIN_H

#include <icplugininterface.h>
#include "dummyplugin_global.h"

class DUMMYPLUGINSHARED_EXPORT DummyPlugin: public ICPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ivk-center.dummyplugin")
    Q_INTERFACES(ICPluginInterface)

public:
    DummyPlugin(){}
    virtual ~DummyPlugin(){}
    virtual ICPluginInfo pluginInfo();

public slots:
    virtual void init(QString& prefix);
    virtual void uninit();

protected slots:
    virtual void processMessage(quint64 srcAddr, QByteArray data);
};

#endif // DUMMYPLUGIN_H
