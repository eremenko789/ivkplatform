#include "clientloadertest.h"
#include <QtTest/QTest>
#include <iclocalsettings.h>
#include <QDir>

#define IC_TEST_MESSAGE "Test!"

ClientLoaderTest::ClientLoaderTest(QObject *parent) :
    QObject(parent),
    m_clientConnection(0),
    m_serverConnection(0),
    m_success(false)
{
    connect(&m_clientsLoader,&ICTcpClientsLoader::newConnection,this,&ClientLoaderTest::onNewClientConnection);
    connect(&m_server,&ICTcpServer::newConnection,this,&ClientLoaderTest::onNewServerConnection);
    m_server.init("");
    m_server.start();
}

void ClientLoaderTest::onNewClientConnection(ICConnection *connection)
{
    if(m_clientConnection)
        m_clientConnection->deleteLater();
    m_clientConnection = connection;
    m_clientConnection->send(IC_TEST_MESSAGE);
}

void ClientLoaderTest::onNewServerConnection(ICConnection *connection)
{
    if(m_serverConnection)
        m_serverConnection->deleteLater();
    m_serverConnection = connection;
    connect(m_serverConnection,&ICConnection::received,this,&ClientLoaderTest::onMessageRecieved);
}

void ClientLoaderTest::onMessageRecieved(QByteArray message)
{
    if(message == QByteArray(IC_TEST_MESSAGE))
        m_success = true;
}

///////////////     TESTS   /////////////////////

void ClientLoaderTest::testLoading_data()
{
    QTest::addColumn<QString>("configuration");
    QTest::newRow("OPEN") << ":/configurations/open.ini";
    QTest::newRow("SECURE") << ":/configurations/secure.ini";
}

void ClientLoaderTest::testLoading()
{
    m_success = false;
    QFETCH(QString,configuration);
    auto settings = new ICLocalSettings();
    auto filename = settings->fileName();
    delete settings;
    QFile::remove(filename);
    QFile testFile(configuration);
    QVERIFY(testFile.open(QFile::ReadOnly));
    auto content = testFile.readAll();
    int offset = filename.lastIndexOf("/");
    QDir dir;
    if(offset>0)
        QVERIFY(dir.mkpath(filename.left(offset)));
    QFile newfile(filename);
    QVERIFY(newfile.open(QFile::Append));
    newfile.write(content);
    newfile.close();
    testFile.close();
    m_clientsLoader.start();
    QTRY_VERIFY_WITH_TIMEOUT(m_success,3000);
}

#include "moc_clientloadertest.cpp"
