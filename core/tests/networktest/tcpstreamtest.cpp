#include "tcpstreamtest.h"
#include <QtTest/QTest>

#define IC_TEST_IP "127.0.0.1"
#define IC_TEST_STRING "test"
#define IC_BEGIN_TEST(num) {m_success = false;m_currentTest=num;}
#define IC_END_TEST {QTRY_VERIFY_WITH_TIMEOUT(success(),3000);}

TcpStreamTest::TcpStreamTest(QObject *parent) :
    QObject(parent)
{
    m_stream.init("");
    m_currentTest = -1;
    m_serverSocket = 0;
    // Создать сервер, слушающий тестовый TCP-порт
    connect(&m_server,SIGNAL(newConnection()),SLOT(onServerAcceptConnection()));
    m_server.listen(QHostAddress(IC_TEST_IP),testPort);
    // Подписать обработчики сигналов клиента
    connect(&m_stream,SIGNAL(opened()),SLOT(onClientOpened()));
    connect(&m_stream,SIGNAL(closed()),SLOT(onClientClosed()));
    connect(&m_stream,SIGNAL(readyRead()),SLOT(onClientReadyRead()));
    connect(&m_stream,SIGNAL(openTimeout()),this,SLOT(onClientConnectTimeout()));
}

void TcpStreamTest::onServerAcceptConnection()
{
    m_serverSocket = m_server.nextPendingConnection();
    if(m_currentTest == HandleConnectionLostTest)
    {
        m_serverSocket->abort();
        return;
    }
    // Подписать обработчики сигналов серверного сокета
    connect(m_serverSocket,SIGNAL(readyRead()),SLOT(onServerReadyRead()));
}

void TcpStreamTest::onServerReadyRead()
{
    // прочитать данные из буфера
    Q_ASSERT(m_serverSocket!=nullptr);
    auto actualData = m_serverSocket->readAll();
    //т.к. тестовая строка достаточно мала, она должна прийти целиком
    QByteArray expectedData = QString(IC_TEST_STRING).toUtf8();
    // сравнить содержимое с ожидаемой строкой
    m_success = (actualData == expectedData);
}

void TcpStreamTest::onClientOpened()
{
    if(m_currentTest==OpenTest)
        m_success = true;
}

void TcpStreamTest::onClientClosed()
{
    if(m_currentTest==CloseTest || m_currentTest==HandleConnectionLostTest)
        m_success = true;
}

void TcpStreamTest::onClientReadyRead()
{
    // прочитать данные из буфера
    QByteArray actualData = m_stream.read();
    // сравнить содержимое с ожидаемой строкой
    QByteArray expectedData = QString(IC_TEST_STRING).toUtf8();
    m_success = (actualData == expectedData);
}

void TcpStreamTest::onClientConnectTimeout()
{
    if(m_currentTest==TryConnectUnavailableTest)
        m_success = true;
}

/////////////////// Tests ////////////////////

void TcpStreamTest::initializeConnection()
{
    IC_BEGIN_TEST(InitTest)
    // Инициализировать тестового клиента
    m_stream.setConnectionAttribute(QString("%1:%2").arg(IC_TEST_IP).arg(testPort));
    m_stream.initialize();
    // Проверить корректность ip и порта
    QCOMPARE(m_stream.hostname(),QString(IC_TEST_IP));
    QCOMPARE(m_stream.port(),testPort);
}

void TcpStreamTest::openClientConnection()
{
    IC_BEGIN_TEST(OpenTest)
    m_stream.open(true);
    IC_END_TEST
}

void TcpStreamTest::writeString()
{
    IC_BEGIN_TEST(WriteTest)
    auto data = QString(IC_TEST_STRING).toUtf8();
    int bytesWritten = m_stream.write(data);
    QCOMPARE(bytesWritten,data.length());
    IC_END_TEST
}

void TcpStreamTest::readString()
{
    IC_BEGIN_TEST(ReadTest)
    if(m_serverSocket)
        m_serverSocket->write(QString(IC_TEST_STRING).toUtf8());
    IC_END_TEST
}

void TcpStreamTest::closeClientConnection()
{
    IC_BEGIN_TEST(CloseTest)
    m_stream.close();
    IC_END_TEST
}

void TcpStreamTest::handleConnectionLost()
{
    IC_BEGIN_TEST(HandleConnectionLostTest)
    m_stream.open(true);
    IC_END_TEST
}

void TcpStreamTest::tryConnectUnavailableHost()
{
    IC_BEGIN_TEST(TryConnectUnavailableTest)
    m_server.close();
    m_stream.open(true);
    IC_END_TEST
}

#include "moc_tcpstreamtest.cpp"
