QT += testlib \
        network

CONFIG += crypto
LIBS += -lqca

HEADERS += tcpstreamtest.h \
    ../../include/icdatastream.h \
    ../../include/ictcpstream.h \
    ../../include/icnetworkstream.h \
    ../../include/iciostream.h \
    ../../include/iclocalsettings.h \
    diffiehellmantest.h \
    ../../include/icdiffiehellmankeygenerator.h \
    ../../include/ickeygenerator.h \
    encodertest.h \
    ../../include/iccompressencoder.h \
    ../../include/icchipherencoder.h \
    ../../include/icdataencoder.h \
    ../../include/sosemanuk.h \
    ../../include/icsecurestreamencoder.h \
    ../../include/icerrornotifier.h \
    ../../include/icerrorhandler.h \
    ../../include/icerror.h \
    ../../include/icconnection.h \
    ../../include/icobject.h \
    connectiontest.h \
    ../../include/icconnectionfactory.h \
    ../../include/ictcpserver.h \
    ../../include/ictcpclientsloader.h \
    clientloadertest.h \
    ../../include/icobjectsettings.h \
    ../../include/icsettings.h \

SOURCES += tcpstreamtest.cpp \
    networktest.cpp \
    ../../src/framework/network/ictcpstream.cpp \
    ../../src/framework/network/iciostream.cpp \
    ../../src/framework/network/icnetworkstream.cpp \
    ../../src/icsettings/iclocalsettings.cpp \
    diffiehellmantest.cpp \
    ../../src/framework/network/icdiffiehellmankeygenerator.cpp \
    ../../src/framework/network/ickeygenerator.cpp \
    encodertest.cpp \
    ../../src/framework/network/icdataencoder.cpp \
    ../../src/framework/network/iccompressencoder.cpp \
    ../../src/framework/network/icchipherencoder.cpp \
    ../../src/framework/network/sosemanuk.cpp \
    ../../src/framework/network/icsecurestreamencoder.cpp \
    ../../src/icobject/icerrornotifier.cpp \
    ../../src/icobject/icerrorhandler.cpp \
    ../../src/framework/network/icconnection.cpp \
    connectiontest.cpp \
    ../../src/icobject/icobject.cpp \
    ../../src/framework/network/ictcpserver.cpp \
    ../../src/framework/network/ictcpclientsloader.cpp \
    clientloadertest.cpp \
    ../../src/icobject/icobjectsettings.cpp


QMAKE_CXXFLAGS += -std=c++0x

OTHER_FILES += \
    configurations/open.ini \
    configurations/secure.ini

RESOURCES += \
    networktest.qrc
