#ifndef ICOBJECTTEST_H
#define ICOBJECTTEST_H

#include <QObject>
#include <QThread>
#include <settingswritter.h>

class ICObjectTest : public QObject
{
    Q_OBJECT
public:
    ICObjectTest();
    ~ICObjectTest();
    void incSucceeded(){m_succeeded++;}
private slots:
    void initTestCase();
    void cleanupTestCase();
    /**
     * @brief Тест интерфейса доступа к настройкам
     */
    void testSettings();
    void testReading();
signals:
    void update();
private:
    int m_succeeded = 0;
    SettingsWritter* m_writter = nullptr;
    QThread *m_writeThread = nullptr;
};

#endif // ICOBJECTTEST_H
