SOURCES += \
    $$PWD/icobjecttest.cpp \
    $$PWD/settingswatcher.cpp \
    $$PWD/settingswritter.cpp

HEADERS += \
    $$PWD/icobjecttest.h \
    $$PWD/settingswatcher.h \
    $$PWD/settingswritter.h \
    $$PWD/testconst.h

SETTINGS_TESTDIR = $$PWD
