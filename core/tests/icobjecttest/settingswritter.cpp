#include "settingswritter.h"
#include "testconst.h"

SettingsWritter::SettingsWritter(QObject *parent) :
    ICObject(parent)
{
}

void SettingsWritter::updateStatus()
{
    saveSetting(SETTING_NAME,READY_STATUS
#ifndef TEST_LOCAL
    ,false
#endif
                            );
}
