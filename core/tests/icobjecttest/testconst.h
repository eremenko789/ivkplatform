#ifndef TESTCONST_H
#define TESTCONST_H

#define WATCHER_CNT 3
#define SETTINGS_GROUP "test"
#define SETTING_NAME "status"
#define INIT_STATUS "waiting"
#define READY_STATUS "ready"

#endif // TESTCONST_H
