#include "settingswatcher.h"
#include <icobjecttest.h>
#include <QDebug>

SettingsWatcher::SettingsWatcher(QObject *parent) :
    ICObject(parent)
{
    connect(this,&SettingsWatcher::statusChanged,this,&SettingsWatcher::onStatusChanged);
}

void SettingsWatcher::onStatusChanged()
{
    auto tst = qobject_cast<ICObjectTest*>(parent());
    if(tst==nullptr)
        return;
    if(status()==READY_STATUS)
        tst->incSucceeded();
}
