#ifndef MYPLATFORM_H

#include <QObject>
#include <icpluginloader.h>
#include <QTimer>

class MyPlatform : public QObject
{
    Q_OBJECT
public:
    MyPlatform();
private:
    void platformInit();
       
private slots:
    void onPlatformLoaded(ICPluginLoader* loader);
    void onTimer();

private:
    ICPluginInterface* plugin;
    QTimer* m_timer;
    QString m_prefix;

signals:
    void init();
};


#endif // MYPLATFORM_H
