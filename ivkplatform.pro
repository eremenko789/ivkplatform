TEMPLATE=subdirs
CONFIG+=ordered
DESTDIR=bin
SUBDIRS += core \
	   ictmplugininterface \
           containers \
           database \
           auth \
           plugins \
           launcher \
           ui
