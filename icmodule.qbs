import qbs 1.0

Project
{
    property string subfolder // путь к модулю относительно места, где лежит файл проекта
    property string pubincludes // путь к публичным инклудам относительно subfolder
    property string includes: subfolder+pubincludes // путь к публичным инклудам модуля для использования снаружи
}
