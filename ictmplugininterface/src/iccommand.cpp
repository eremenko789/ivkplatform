#include "iccommand.h"
#include <QHash>
#include <icobjectmacro.h>
#include <ichash.h>

IC_REGISTER_METATYPE(ICCommand)

QDataStream & operator<< ( QDataStream & out, const ICCommand & c )
{
    out << c.address << c.attrib << c.id << c.object << c.status << c.type << c.time << c.sessionId;
    return out;
}

QDataStream & operator>> ( QDataStream & in, OPERATION_TYPE & op)
{
    int ct =0;
    in >> ct;
    op = (OPERATION_TYPE)ct;
    return in;
}

QDataStream & operator>> ( QDataStream & in, ICCommand & c )
{
    in >> c.address >> c.attrib >> c.id >> c.object >> c.status >> c.type >> c.time >> c.sessionId;
    return in;
}

QDataStream & operator<< ( QDataStream & out, const COMMAND_TYPE_LIST & c )
{
    out << c.length();
    foreach (CommandType type, c)
    {
        out << type.object << type.type << type.priority;
    }
    return out;
}

QDataStream & operator>> ( QDataStream & in, COMMAND_TYPE_LIST & c )
{
    int length;
    in >> length;
    for (int i = 0; i < length; i++)
    {
        CommandType type;
        in >> type.object >> type.type >> type.priority;
        c.append(type);
    }
    return in;
}


bool operator <(const CommandType & c1, const CommandType & c2)
{
    if (c1.object < c2.object)
        return true;
    else if ((c1.object == c2.object) && (c1.type < c2.type))
        return true;
    else
        return false;
}

bool operator==(const CommandType &e1, const CommandType &e2)
{
    return e1.object == e2.object
           && e1.type == e2.type;
}

uint qHash(const CommandType &key)
{
    return qHash(key.object) ^ key.type;
}

QString ICCommand::toString() const
{
    QString res;
    res.append(QString("addr: %1, ").arg(address));
    int len = attrib.length();
    res.append(QString("attrib.length: %1, ").arg(len));
    res.append(QString("id: %1, ").arg(id));
    res.append(QString("object: %1, ").arg(object).toLatin1());
    res.append(QString("status: %1, ").arg(status));
    res.append(QString("type: %1, ").arg(type));
    res.append(QString("time: %1, ").arg(QDateTime::fromMSecsSinceEpoch(time).toString()));
    return res;
}
