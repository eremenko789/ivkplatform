#include <ictmplugininterface.h>
#include <QDateTime>
#include <QDataStream>
#include <QDebug>

#define DEFAULT_ANSWER_WAIT_TIME_MS (15000)
#define DEFAULT_ANSWER_CHECK_INTERVAL (1000)

ICTMPluginInterface::ICTMPluginInterface(QObject *parent): ICPluginInterface(parent), m_answerCheckTimer(this)
{
    m_operatorString[COMMAND_TYPE_SELECT]  = "select";
    m_operatorString[COMMAND_TYPE_INSERT]  = "insert";
    m_operatorString[COMMAND_TYPE_UPDATE]  = "update";
    m_operatorString[COMMAND_TYPE_EXECUTE] = "execute";
    m_operatorString[COMMAND_TYPE_DELETE]  = "delete";
    m_operatorString[COMMAND_TYPE_SYNC]    = "sync";
    m_operatorString[COMMAND_TYPE_AUTH]    = "auth";
    m_operatorString[COMMAND_TYPE_LOGOUT]  = "logout";
}

void ICTMPluginInterface::start()
{
    connect(this, &ICTMPluginInterface::startPluginWork, this, &ICTMPluginInterface::startWork, Qt::DirectConnection);
    m_answerCheckTimer.start();
    auto pls = pluginIDs();
    m_answersCount = pls.count();
    requestPluginsForSupportedCommands(pls);
    if(pls.count()==0)
        emit startPluginWork();
}

void ICTMPluginInterface::onPluginsChanged()
{
    // нужно получить текущий список модулей, удалить из своего списка тех, кто пропал, а тех, кто появился нужно опросить, что они умеют обрабатывать
    auto newpls = pluginIDs();
    auto oldpls = m_remoteHandlers.values();
    // сначала найдем все модули, которые пропали
    for(auto id : newpls)
    {
        auto it = oldpls.begin();
        while(it != oldpls.end())
            if(it->addr == id)
                it = oldpls.erase(it);
            else
                it++;
    }
    // удалим пропавших
    auto keys = m_remoteHandlers.uniqueKeys();
    for(auto key : keys)
        for(auto handler : oldpls)
            m_remoteHandlers.remove(key, handler);
    // а теперь нужно запросить, кто что умеет из новых модулей
    oldpls = m_remoteHandlers.values();
    for(auto id : oldpls)
        newpls.removeOne(id.addr);
    // в newpls остались только новые модули, которые надо опросить
    m_answersCount = newpls.count();
    requestPluginsForSupportedCommands(newpls);
}

void ICTMPluginInterface::requestPluginsForSupportedCommands(QList<quint64> pls)
{
    ICCommand cmd = newCommand();
    cmd.type = COMMAND_TYPE_SELECT;
    cmd.object = "_supportedCommands";
    QByteArray arr;
    QDataStream str(&arr, QIODevice::ReadWrite);
    str<<cmd;
    qint64 curtime = QDateTime::currentMSecsSinceEpoch();
    for(auto id : pls)
    {
        m_answerWaiters.insert(cmd.id, {curtime+(qint64)m_defaultWaitTimeout, make_callback(&ICTMPluginInterface::commandsSelected), id, cmd});
        sendMessage(id, arr);
    }
}

void ICTMPluginInterface::commandsSelected(quint64 srcAddr, ICCommand& cmd)
{
    m_answersCount--;
    if(cmd.status == COMMAND_STATUS_OK)
    {
        // в атрибутах команды должен быть список поддерживаемых типов команд
        QDataStream str(&cmd.attrib, QIODevice::ReadOnly);
        COMMAND_TYPE_LIST list;
        str>>list;
        if(!list.isEmpty())
            for(auto ct : list) // добавим в список обрабатываемых команд
                if(!m_remoteHandlers.contains(ct, {srcAddr, ct.priority}))
                    m_remoteHandlers.insert(ct, {srcAddr, ct.priority}); // если есть - заменим, нет - добавим.
    }
    else
        issueWarning(trUtf8("Ошибка получения списка команд от модуля addr=%1 (error=%2)").arg(srcAddr).arg(cmd.status));
    if(m_answersCount==0) // от всех дождались ответы, можно и работу модуля начинать (если раньше не начинали)
    {
        emit startPluginWork();
        disconnect(this, &ICTMPluginInterface::startPluginWork, this, &ICTMPluginInterface::startWork); // чтобы не начинать работу модуля заново каждый раз, когда меняется состав модулей в системе
    }
}

int ICTMPluginInterface::request(const ICCommand &cmd, answerFunc f, uint mTimeout)
{
    // определим, есть ли кто-то, кто умеет обрабатывать такие команды
    CommandType t(cmd);
    if(!m_remoteHandlers.contains(t))
    {
        // сообщим функции, что нет обработчика (если ей это надо)
        if(f!=nullptr)
        {
            ICCommand newcmd = cmd;
            newcmd.status = COMMAND_STATUS_NO_HANDLER;
            f(0, newcmd);
        }
        return 0;
    }
    QByteArray arr;
    QDataStream ds(&arr, QIODevice::WriteOnly);
    ds<<cmd;
    qint64 curtime = QDateTime::currentMSecsSinceEpoch();
    if(mTimeout == 0)
        mTimeout = m_defaultWaitTimeout;
    auto handlers = m_remoteHandlers.values(t);
    // отсортируем, чтобы в начале были обработчики с высшим приоритетом
    std::sort(handlers.begin(), handlers.end(), [](const AddrPriority& v1, const AddrPriority& v2){return (v1.priority > v2.priority);});
    quint8 maxpr = handlers[0].priority;
    // создадим семафор, если он нужен
    int retcounter = 0;
    for(auto handler : handlers) // если обработчиков много, то множим команду среди обработчиков с одинаковым высшим приоритетом.
    {
        if(maxpr > handler.priority) // все остальные приоритеты ниже, им посылать ничего не надо
            break;
        if(f != nullptr) // ждем ответ
            m_answerWaiters.insert(cmd.id, {curtime+(qint64)mTimeout, f, handler.addr, cmd});
        sendMessage(handler.addr, arr);
        ++retcounter;
    }
    return retcounter;
}

void ICTMPluginInterface::reply(const ICCommand& cmd)
{
    QByteArray arr;
    QDataStream ds(&arr, QIODevice::WriteOnly);
    ds<<cmd;
    sendMessage(cmd.address, arr);
}

void ICTMPluginInterface::processMessage(quint64 srcAddr, const QByteArray& data)
{
    QDataStream str(data);
    ICCommand cmd;
    str>>cmd;
    if(!cmd.isValid())
        return;
    if(cmd.status == COMMAND_STATUS_REQUEST)
    {
        if(cmd.address!=srcAddr)
            issueWarning(trUtf8("ICTMPluginInterface: адрес отправителя в команде не совпадает с реальным отправителем! (srcAddr=%1, cmd.address=%2)").arg(srcAddr).arg(cmd.address));
        CommandType t(cmd);
        if(m_pluginQueryHandlerByType.contains(t))
        {
            pluginFunc f = m_pluginQueryHandlerByType[t];
            (this->*f)(cmd);
            if(cmd.isValid() && cmd.status != COMMAND_STATUS_REQUEST)
                reply(cmd);
        }
    }
    else // от кого-то пришел ответ
    {
        if(m_answerWaiters.contains(cmd.id))
        {
            auto vals = m_answerWaiters.values(cmd.id);
            for(auto val : vals)
                if(val.answerer == srcAddr)
                {
                    if(val.f)
                        val.f(srcAddr, cmd); // вызовем функцию обработки ответа
                    m_answerWaiters.remove(cmd.id, val);
                    break;
                }
        }
        else
        {
            //TODO: это несомненно надо делать, но пока глаз мозолит
//            issueWarning(trUtf8("ICTMPluginInterface: Пришел ответ, которого не ждали!(answerer=%1)").arg(srcAddr));
        }
    }
}

void ICTMPluginInterface::registerCommandHandler(CommandType ct, pluginFunc f, quint8 priority)
{
    if(priority == INVALID_PRIORITY)
        priority = readPriorityFromConfig(ct);
    ct.priority = priority;
    m_pluginQueryHandlerByType.insert(ct, f);
}

quint8 ICTMPluginInterface::readPriorityFromConfig(const CommandType& ct)
{
    int pr = setting("priority", DEFAULT_PRIORITY).toInt();
    pr = setting(trUtf8("%1/%2").arg(ct.object).arg(m_operatorString[ct.type]), pr).toInt();
    return (quint8)pr;
}

ICCommand ICTMPluginInterface::newCommand(OPERATION_TYPE operation,const QString& operandType)
{
    ICCommand cmd;
    cmd.type = operation;
    cmd.object = operandType;
    cmd.address = thisPluginID();
    cmd.id = generateUID();
    cmd.status = COMMAND_STATUS_REQUEST;
    cmd.time = QDateTime::currentMSecsSinceEpoch();
    cmd.sessionId = m_sessionId;
    return cmd;
}

ICRETCODE ICTMPluginInterface::init(const QString &pref)
{
    auto ret = ICPluginInterface::init(pref);
    m_defaultWaitTimeout = setting(trUtf8("waitAnswerTimeout"), DEFAULT_ANSWER_WAIT_TIME_MS).toUInt();
    m_answerCheckTimer.setInterval(setting(trUtf8("checkAnswerInterval"), DEFAULT_ANSWER_CHECK_INTERVAL).toInt());
    connect(&m_answerCheckTimer, &QTimer::timeout, this, &ICTMPluginInterface::onAnswerCheckTimer);
    registerCommandHandler({COMMAND_TYPE_SELECT, "_supportedCommands"}, (pluginFunc)&ICTMPluginInterface::onSelectCommandTypes, DEFAULT_PRIORITY);
    return ret;
}

void ICTMPluginInterface::onSelectCommandTypes(ICCommand& cmd)
{
    COMMAND_TYPE_LIST list = m_pluginQueryHandlerByType.uniqueKeys();
    cmd.status = COMMAND_STATUS_OK;
    QDataStream str(&cmd.attrib, QIODevice::WriteOnly);
    str<<list;
}

void ICTMPluginInterface::onAnswerCheckTimer()
{
    // пробежимся по всем ожидающим и проверим, не заждались ли?
    auto keys = m_answerWaiters.uniqueKeys();
    auto curtime = QDateTime::currentMSecsSinceEpoch();
    for(auto key : keys)
    {
        auto vals = m_answerWaiters.values(key);
        for(auto val : vals)
        {
            if(curtime >= val.time)
            {
                val.cmd.status = COMMAND_STATUS_TIMEOUT_ERROR;
                if(val.f)
                    val.f(val.answerer, val.cmd);
                m_answerWaiters.remove(key, val);
            }
        }
    }
}
