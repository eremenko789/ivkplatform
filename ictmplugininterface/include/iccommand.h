/**
* @file iccommand.h
* @brief Заголовочный файл описывает команду и необходимые константы
* @copyright ivk-center.ru
*/

#ifndef COMMAND_H
#define COMMAND_H

#include <QtCore/qglobal.h>
#include <QByteArray>
#include <QIODevice>
#include <QDataStream>
#include <QDateTime>
#include <QStack>
#include <ictmplugininterface_global.h>

#define COMMAND_STATUS_REQUEST           0 ///< Статус Запрос
#define COMMAND_STATUS_OK                1 ///< Cтатус Ок
#define COMMAND_STATUS_ERROR             2 ///< Cтатус Ошибка
#define COMMAND_STATUS_NO_HANDLER        3 ///< Cтатус не найден обработчик
#define COMMAND_STATUS_TIMEOUT_ERROR     4 ///< Cтатус за указанное время не получен ответ
#define COMMAND_STATUS_CONTINUE          5 ///< Статус обработка запроса продолжается
#define COMMAND_STATUS_USER              6 ///< Пользовательский статус. Начиная с него можно вводить какие-то свои статусы.
//#define COMMAND_TIMEOUT_BORDER         224 ///< Граница таймаута для команд, которые нужно выполнить, не важно когда

#define COMMAND_DEFAULT_ID      0xFFFFFFFF

/**
 * @brief Тип данных для типов (видов) команд.
 */
enum OPERATION_TYPE{
    COMMAND_TYPE_ERROR,     ///< Ошибка. Тип команды не определен.
    COMMAND_TYPE_SELECT,    ///< Тип команды Выбрать
    COMMAND_TYPE_INSERT,    ///< Тип команды Вставить
    COMMAND_TYPE_UPDATE,    ///< Тип команды Обновить
    COMMAND_TYPE_EXECUTE,   ///< Тип команды Выполнить
    COMMAND_TYPE_DELETE,    ///< Тип команды Удалить
    COMMAND_TYPE_SYNC,      ///< Тип команды синхронизировать данные
    COMMAND_TYPE_LOGIN,     ///< Тип команды создания сеанса
    COMMAND_TYPE_LOGOUT,    ///< Тип команды завершения сеанса
    COMMAND_TYPE_AUTH,      ///< Тип команды проверки аутентификации для действий над объектом
    COMMAND_TYPE_SET,        ///< Команада "установить"
    COMMAND_TYPE_UPDATE_GROUP ///< Групповое обновление
};

/**
 * @brief Класс, описывающий команду поступающую из менеджера подключений
 * в менеджер очереди команд.
 * @ingroup core
 */
class ICTMPLUGININTERFACESHARED_EXPORT ICCommand
{
public:
    ///< Идентификатор отправителя команды
    quint64 address=0;
    ///< Идентификатор команды на отправителе
    quint64 id;
    ///< Запрос = 0 / Статус ответа
    int status;
    ///< Тип команды
    OPERATION_TYPE type;
    ///< Тип объекта операции
    QString object;
    ///< Атрибуты операции (сериализованный экземпляр объекта)
    QByteArray attrib;
    ///< Время создания команды
    qint64 time;
    ///< Идентификатор сессии пользователя
    quint64 sessionId;

    ///< Конструктор копии
    ICCommand(const ICCommand& c)
    {
        address = c.address;
        id = c.id;
        status = c.status;
        type = c.type;
        object = c.object;
        attrib = c.attrib;
        time = c.time;
        sessionId = c.sessionId;
    }
    ICCommand() { id=COMMAND_DEFAULT_ID; type = COMMAND_TYPE_ERROR; status = 0; time = 0;}
    ~ICCommand() { }
    QString toString() const;
    bool isValid(){return id!=COMMAND_DEFAULT_ID;}
    ICCommand& operator=(const ICCommand& c)
    {
        address = c.address;
        id = c.id;
        status = c.status;
        type = c.type;
        object = c.object;
        attrib = c.attrib;
        time = c.time;
        sessionId = c.sessionId;
        return *this;
    }
};

/**
 * @brief Тип данных для перенаправления запросов к плагинам, содержащий тип команды
 * @ingroup core
 */
class ICTMPLUGININTERFACESHARED_EXPORT CommandType
{
public:
#define INVALID_PRIORITY (0)
#define DEFAULT_PRIORITY (1)
    OPERATION_TYPE type;
    QString object;
    quint8 priority = INVALID_PRIORITY;
    CommandType() {}
    CommandType(OPERATION_TYPE ot, const QString& obj, quint8 pr = INVALID_PRIORITY)
    {
        type = ot;
        object = obj;
        priority = pr;
    }
    CommandType(const ICCommand & cmd)
    {
        type = cmd.type;
        object = cmd.object;
    }
};

/**
 * @typedef QList<CommandType> COMMAND_TYPE_LIST
 * Тип данных для возвращения плагином списка поддерживаемых типов команд
 * @ingroup core
 */
typedef QList<CommandType> COMMAND_TYPE_LIST;

ICTMPLUGININTERFACESHARED_EXPORT QDataStream & operator>> ( QDataStream & in, OPERATION_TYPE & op);
QDataStream ICTMPLUGININTERFACESHARED_EXPORT & operator<< ( QDataStream & out, const ICCommand & c );
QDataStream ICTMPLUGININTERFACESHARED_EXPORT & operator>> ( QDataStream & in, ICCommand & c );

bool ICTMPLUGININTERFACESHARED_EXPORT operator < (const CommandType & c1, const CommandType & c2);
bool ICTMPLUGININTERFACESHARED_EXPORT operator==(const CommandType &e1, const CommandType &e2);
uint ICTMPLUGININTERFACESHARED_EXPORT qHash(const CommandType &key);

QDataStream ICTMPLUGININTERFACESHARED_EXPORT & operator<< ( QDataStream & out, const COMMAND_TYPE_LIST & c );
QDataStream ICTMPLUGININTERFACESHARED_EXPORT & operator>> ( QDataStream & in, COMMAND_TYPE_LIST & c );

Q_DECLARE_METATYPE(ICCommand)

#endif // COMMAND_H
