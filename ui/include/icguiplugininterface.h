#ifndef ICGUIPLUGININTERFACE_H
#define ICGUIPLUGININTERFACE_H

#include "icguiplugininterface_global.h"
#include <ictmplugininterface.h>

/**
 * @brief Класс ICGuiPluginInterface предоставляет интерфейс взаимодейсвия с платформой для ICDataModel и подобным классам.
 * @ingroup core gui
 */
class ICGUIPLUGININTERFACESHARED_EXPORT ICGuiPluginInterface : public ICTMPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ivk-center.icguiplugininterface")
    Q_INTERFACES(ICPluginInterface)

public:
    Q_INVOKABLE ICGuiPluginInterface(QObject* parent=nullptr);
public slots:
    void reply(const ICCommand &cmd) override;
    static ICGuiPluginInterface* instance() {return m_instance;}

private:
    static ICGuiPluginInterface* m_instance;
};

#endif // ICGUIPLUGININTERFACE_H
