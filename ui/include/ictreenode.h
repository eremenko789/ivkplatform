#ifndef ICTREENODE_H
#define ICTREENODE_H

#include <icobject.h>
#include <ictreemodel_global.h>
class ICTreeModel;

/**
 * @brief Класс, описывающий атрибуты элемента дерева
 * @ingroup gui
 */
class ICTREEMODELSHARED_EXPORT ICTreeNode
{
public:
    //constructors:
    explicit ICTreeNode(QObject* parent=nullptr) {Q_UNUSED(parent)}
    ICTreeNode(QObject* model,const QString& description);
    virtual ~ICTreeNode(){qDeleteAll(m_subnodes); m_subnodes.clear();}

    //setters/getters:
    /**
    * @brief Возвращает название узла дерева
    */
    inline virtual QString description() const{return m_description;}
    /**
    * @brief Устанавливает название узла дерева
    * @param val новое значение
    */
    inline virtual void setDescription(QString val){if(m_description!=val) {m_description=val;}}
    /**
    * @brief Возвращает иконку узла дерева
    */
    inline virtual QString icon() const{return m_icon;}
    /**
    * @brief Устанавливает иконку узла дерева
    * @param val новое значение
    */
    inline virtual void setIcon(QString val){if(m_icon!=val) {m_icon=val;}}

    /**
     * @brief Оператор добавления дочернего элемента
     * @param node дочерний элемент
     */
    virtual void append(ICTreeNode* node);
    /**
     * @brief Возвращает список дочерних элементов
     * @return
     */
    inline virtual QList<ICTreeNode*> subnodes() const{return m_subnodes;}
    /**
     * @brief Содержит ли дочерние элементы?
     * @return
     */
    virtual bool containsNodes(){return !m_subnodes.isEmpty();}

    virtual void setSubnodes(QList<ICTreeNode*> val) {m_subnodes = val;}

    /**
     * @brief Возвращает индекс строки в списке предка
     * @return
     */
    virtual int row();

    virtual ICTreeNode* parentNode() {return m_parent;}
    virtual void setParentNode(ICTreeNode* parent);

    virtual bool isRoot() {return m_isRoot;}
    virtual void setIsRoot(bool val) {m_isRoot = val;}

protected:
    ICTreeNode* m_parent = nullptr;
    bool m_isRoot = false;

private:
    // data members:
    QString m_description;
    QString m_icon;
    QList<ICTreeNode*> m_subnodes;
    ICTreeModel* m_model = nullptr;

};

Q_DECLARE_METATYPE(ICTreeNode*)

#endif // ICTREENODE_H
