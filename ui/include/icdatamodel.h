#ifndef ICDATAMODEL_H
#define ICDATAMODEL_H

#include "icdatamodel_global.h"
#include <icguiplugininterface.h>
#include <icerror.h>
#include <icrecord.h>
#include <icdballrequests.h>
#include <functional>
#include <QAbstractItemModel>
#include <functional>
#include <icdbresponse.h>
#include <icobjectrecord.h>
#include <icdb.h>

/**
  * @defgroup dataaccess Интерфейс доступа к данным
  */

#define INFINITE_DEPTH  -1
#define DEFAULT_DEPTH   200

typedef std::function<bool(const ICRecord*,const ICConditionSet&)> ICMatchFunction;

/**
 * @brief Интерфейс модели данных
 * @ingroup dataaccess
 */
class ICDATAMODELSHARED_EXPORT ICDataModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum Roles
    {
        RecordRole = Qt::UserRole,
        ValueRole
    };

    ICDataModel(ICGuiPluginInterface* plugin, QObject* parent=0);
    virtual ~ICDataModel();

    //properties:
    Q_PROPERTY(QStringList headerNames READ headerNames NOTIFY headerNamesChanged)
    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)
    Q_PROPERTY(QString typeName READ typeName WRITE setTypeName NOTIFY typeNameChanged)
    Q_PROPERTY(QString prefilter READ prefilter WRITE setPrefilter NOTIFY prefilterChanged) ///< предфильтр
//    Q_PROPERTY(int totalCount READ totalCount NOTIFY totalCountChanged)

    //getters/setters:

    inline QStringList headerNames() const{return m_headers;}
    inline bool loading() const{return m_isLoading;}
    inline const QString& databaseName() const{return m_databaseName;}
    inline void setDatabaseName(const QString& dbname) {m_databaseName = dbname;}
    inline const QString& operand() const{return m_operand;}
    inline void setOperand(const QString& operand){m_operand = operand;}
    inline const QString& typeName() const {return m_typeName;}
    inline void setTypeName(const QString& val){if(m_typeName==val) return;m_typeName = val;emit typeNameChanged();}
    inline void setMatchFunction(ICMatchFunction match){matching = match;}
    ICMatchFunction matching;       ///< функция сравнения
    inline int count() const{return m_records.count();}
    inline void setSortFieldName(const QString& fname){m_sortField=fname;}
    /**
    * @brief Возвращает предфильтр
    */
    inline QString prefilter() const{return m_prefilter;}
    /**
    * @brief Устанавливает предфильтр
    * @param val новое значение
    */
    void setPrefilter(QString val);
    /**
     * @brief Общее количество объектов в базе
     * @return
     */
    inline int totalCount(){return m_totalCount;}
    /**
     * @brief Запрашивает общее количество элементов
     */
    void queryCount(const ICConditionSet& cs);

    // QAbstractItemModel overriden methods:

    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex&) const override {return QModelIndex();}
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QHash<int,QByteArray> roleNames() const override;
    bool hasChildren(const QModelIndex&) const override{return false;}
    QModelIndexList find(const ICConditionSet& conds) const;

    // SYNC-packet handling:
    /**
     * @brief Обработать SYNC-и
     * @param cmds SYNC-пакеты.
     */
    virtual void handleSync(ICCommand& cmd);

    // user-oriented methods:
    /**
     * @brief Добавить записи в модель данных
     * @param recs набор записей
     */
    void appendRecords(const ICRecordSet& recs);
    /**
     * @brief Обновить записи в модели данных
     * @param updateReq запрос обновления из sync-а
     */
    void updateRecords(const ICDbUpdateRequest &updateReq);
    /**
     * @brief Удалить записи из модели данных
     * @param removeReq запрос удаления из sync-а
     */
    void removeRecords(const ICDbRemoveRequest &removeReq);
    /**
     * @brief Добавить записи в начало списка модели данных
     * @param recs набор записей
     */
    void prependRecords(const ICRecordSet& recSet);
    /**
     * @brief список имен полей
     * @return QStringList
     */
    virtual QStringList fieldNames() const;
public slots:
    /**
     * @brief очистить модель
     */
    void clear();
    /**
     * @brief загрузить данные из БД
     * @param cs условия выборки
     * @param count количество загружаемых элементов
     * @param offset номер записи, начиная с которой необходимо произвести выборку
     * @param toPrepend добавлять результат в начало списка
     */
    virtual void load(const ICConditionSet& cs=ICConditionSet(),int count = 0,int offset = 0,bool toPrepend = false);

    //signals:
signals:
    /**
     * @brief Сигнал изменения состояния загрузки
     */
    void loadingChanged();
    /**
     * @brief Сигнал завершения загрузки
     * @param cnt количество загруженных записей
     */
    void loaded(int cnt);
    /**
     * @brief Сигнал об изменении списка заголовков
     */
    void headerNamesChanged();
    /**
     * @brief Общее количество изменилось
     */
    void totalCountChanged();
    /**
     * @brief Сигнал об изменении типа выбираемых объектов
     */
    void typeNameChanged();
    /**
     * @brief сигнал о добавлениии новых записей
     */
    void appended();
    /**
     * @brief сигнал об удалении записей
     */
    void rowsDeleted();
    void prefilterChanged();   ///< Сообщает об изменении значения свойства "предфильтр"
    // response handling methods:

protected:
    /**
     * @brief Обработчик ответа на запрос
     * @param cmd контейнер команды
     */
    Q_INVOKABLE virtual void handleResponse(quint64 addr, const ICCommand &cmd);
//private:
//    static void staticHandleResponse(const ICCommand& cmd);
//    static ICDataModel* waiter(int cmdId);
//    void setWaiter(int cmdId);

    // database-frontend methods:
    /**
     * @brief запросить объекты из БД
     * @param conds условия выборки
     * @param count количество загружаемых элементов
     * @param offset номер записи, начиная с которой необходимо произвести выборку
     * @param sortByField имя поля, по которому производить сортировку
     */
    void select(const ICConditionSet& conds,int count = 0,int offset = 0);
    /**
     * @brief добавить набор записей в БД
     * @param recs набор записей
     */
    void insert(const ICRecordSet& recs);
    /**
     * @brief обновить объекты в БД
     * @param request запрос
     */
    void update(const ICDbUpdateRequest& request);
    /**
     * @brief удалить объекты, удовлетворяющие условиям
     * @param сonds условия выборки
     */
    void remove(const ICConditionSet& conds);
    /**
     * @brief обновить объект
     * @param rec обновляемый объект
     * @param cascade каскадно?
     */
    void update(ICObjectRecord* rec,bool cascade=true);
    /**
     * @brief удалить объект
     * @param rec удаляемый объект
     * @param cascade каскадно
     */
    void remove(ICObjectRecord* rec,bool cascade=true);
    virtual void handleSelect(const ICDbResponse& response);
    virtual void handleError(const ICError& err);
    inline void setTargetTypes(const QStringList& types){m_targetTypes=types;}
private:
    void execRequest(OPERATION_TYPE operation,ICDbRequest* req,ICTMPluginInterface::answerFunc answerCb=nullptr);
    inline void setLoading(bool val){if(m_isLoading==val) return; m_isLoading=val; emit loadingChanged();}
    // recordset operations:

protected:
    /**
     * @brief преобразовать записи
     * @param recs список записей
     * @return
     */
    virtual QList<ICRecord*> adaptRecords(const ICRecordSet& recs) const {return recs.records;}

    //overridable methods

    /**
     * @brief список заголовков
     * @return QStringList
     */
    virtual QStringList headers() const;
    inline ICDb* db() const{return m_db;}
private:
    //other methods:
    void invalidateHeaders();
    void handleCountResponse(quint64,ICCommand& cmd);
    void internalAppendRecords(const ICRecordSet& recs);

    //members:
protected:
    QList<ICRecord*> m_records;     ///< список записей
    int m_totalCount = 0;           ///< общее количество записей
private:
    static bool standartMatching(const ICRecord *rec, const ICConditionSet &conds);
    static QHash<int,ICDataModel*> g_handlerMap;
    static QMutex g_handlerMapMutex;

    ICGuiPluginInterface* m_plugin;    ///< плагин-отправитель запросов
    QString m_databaseName;         ///< имя БД
    QString m_operand;              ///< тип операнда команды
    QString m_typeName;             ///< тип объекта
    bool m_isLoading = false;       ///< флаг-индикатор загрузки
    QStringList m_headers;          ///< список заголовков
    QStringList m_fieldNames;       ///< список имен атрибутов, соответствующий списку заголовков
    int m_depth;                    ///< глубина таблицы
    QString m_sortField;            ///< имя атрибута, по которому нужно сортировать выборку
    QStringList m_targetTypes;      ///< целевые типы (список таблиц, из которых будет происходить выборка)
    bool m_toPrepend = false;       ///< результат добавлять в начало списка
    ICConditionSet m_prefilters;    ///< предфильтры
    QString m_prefilter;
    ICDb* m_db=nullptr;
};

#endif // ICDATAMODEL_H
