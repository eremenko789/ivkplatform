#ifndef ICSETTINGSMODEL_H
#define ICSETTINGSMODEL_H

#include "icsettingsmodel_global.h"
#include <ictreemodel.h>

/**
 * @brief Класс, описывающий абстрактную модель для редактирования настроек.
 * @ingroup settings gui dataaccess
 */
class ICSETTINGSMODELSHARED_EXPORT ICSettingsModel : public ICTreeModel
{
    Q_OBJECT
public:
    ICSettingsModel(QObject* parent=0);
    virtual ~ICSettingsModel(){qDeleteAll(m_nodes);}

    //enums
    enum SettingsRoles
    {
        TypeRole = DataRole+1,
        ValueRole
    };

    // overriden methods
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    QHash<int, QByteArray> roleNames() const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    /**
     * @brief Применить изменения
     */
    void apply();
    /**
     * @brief Сбросить значения настроек
     */
    void reset();
public slots:
    void updateValue();
};

#endif // ICSETTINGSMODEL_H
