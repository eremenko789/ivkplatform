#include "icguiplugininterface.h"
#include <functional>

ICGuiPluginInterface* ICGuiPluginInterface::m_instance = nullptr;

ICGuiPluginInterface::ICGuiPluginInterface(QObject* parent): ICTMPluginInterface(parent)
{
    m_instance = this;
}

void ICGuiPluginInterface::reply(const ICCommand &cmd)
{
    ICTMPluginInterface::reply(cmd);
}
