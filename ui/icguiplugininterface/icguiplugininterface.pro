#-------------------------------------------------
#
# Project created by QtCreator 2014-03-21T14:31:30
#
#-------------------------------------------------
include(../ui_common.pri)

QT       -= gui
CONFIG += plugin
TARGET = icguiplugininterface
TEMPLATE = lib

DEFINES += ICGUIPLUGININTERFACE_LIBRARY

SOURCES += icguiplugininterface.cpp

HEADERS += $$UI_INCLUDE/icguiplugininterface.h\
        $$UI_INCLUDE/icguiplugininterface_global.h

INCLUDEPATH += $$PLATFORM_DIR/ictmplugininterface/include

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS+= -L$$DESTDIR -lictmplugininterface -livkplatform -licobject
