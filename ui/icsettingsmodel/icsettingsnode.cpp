#include <icsettingsnode.h>
#include <QEvent>
#include <icsettingsmodel.h>

ICSettingsNode::ICSettingsNode(ICSettingsModel *model,const QString &description, const QString &path, ICSettingsNode::ICValueType type,QVariant defValue,bool advanced, bool local)
    :ICTreeNode(model,description) ,m_local(local),m_defValue(defValue), m_path(path),m_valueType(type), m_advanced(advanced)
{
    connect(this,&ICSettingsNode::valueChanged,model,&ICSettingsModel::updateValue);
}

void ICSettingsNode::subscribe()
{
    if(m_valueType==NoType)
        return;
    if(m_defValue.isValid())
        setValue(m_defValue);
    int index = m_path.lastIndexOf('/');
    if(index!=-1)
    {
        // парсинг строки
        auto prefix = m_path.left(index);
        // инициализировать диспетчер настроек
        init(prefix);
        m_settingName = m_path.right(m_path.length()-index-1);
        // регистрируем наблюдателя
        registerSettings({m_settingName},m_local);
    }
}

bool ICSettingsNode::event(QEvent *event)
{
    if(event->type() == QEvent::DynamicPropertyChange)
    {
        //если изменилась наблюдаемая настройка
        auto ev = dynamic_cast<QDynamicPropertyChangeEvent*>(event);
        Q_ASSERT(ev!=nullptr);
        auto pname = ev->propertyName();
        if(m_settingName == QString(pname))
        {
            auto val = property(pname.constData());
            setValue(val);
            m_syncValue = val;
        }
    }
    return ICObject::event(event);
}

ICSettingsNode& ICSettingsNode::operator <<(ICSettingsNode *node)
{
    node->subscribe();
    append(node);
    return *this;
}

bool ICSettingsNode::match(const QRegExp &rx, bool includeAdvanced)
{
    if(advanced() && !includeAdvanced)
        return false;
    // если текущий элемент удовлетворяет условиям фильтрации, вернуть true
    if(rx.indexIn(description())!=-1)
        return true;
    //иначе рекурсивно пробегаем по всем дочерним узлам
    for(ICTreeNode* nd: subnodes())
    {
        auto setnode = dynamic_cast<ICSettingsNode*>(nd);
        if(setnode->match(rx,includeAdvanced))
            return true;
    }
    auto parNode = dynamic_cast<ICSettingsNode*>(ICTreeNode::parentNode());
    if(parNode!=nullptr)
        return (rx.indexIn(parNode->description())!=-1) && (!parNode->advanced() || includeAdvanced);
    return false;
}

void ICSettingsNode::apply()
{
    //если узел редактируемый, применить новое значение
    bool modified = m_syncValue.isValid() ? (value()!=m_syncValue) : (value()!=m_defValue);
    if(valueType()!=NoType && modified)
        saveSetting(m_settingName,value(),m_local);
    //для всех потомков вызвать apply()
    for(auto nd: subnodes())
    {
        auto setnode = dynamic_cast<ICSettingsNode*>(nd);
        if(setnode!=nullptr)
            setnode->apply();
    }
}

void ICSettingsNode::reset()
{
    setValue(m_defValue);
    for(auto nd: subnodes())
    {
        auto setnode = dynamic_cast<ICSettingsNode*>(nd);
        if(setnode!=nullptr)
            setnode->reset();
    }
}
