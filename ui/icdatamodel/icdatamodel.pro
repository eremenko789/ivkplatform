#-------------------------------------------------
#
# Project created by QtCreator 2014-02-04T10:15:26
#
#-------------------------------------------------
include(../ui_common.pri)

QT       -= gui

CONFIG+=plugin
TARGET = icdatamodel
TEMPLATE = lib
DESTDIR = $$PLATFORM_DIR/bin

DEFINES += ICDATAMODEL_LIBRARY

SOURCES += icdatamodel.cpp

HEADERS += $$UI_INCLUDE/icdatamodel.h

INCLUDEPATH += $$PLATFORM_DIR/containers/include $$PLATFORM_DIR/database/include $$PLATFORM_DIR/ictmplugininterface/include

unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -L$$DESTDIR -licobject -liccontainers -livkplatform -licdbrequests -licsettings -licguiplugininterface
