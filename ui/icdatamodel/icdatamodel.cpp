#include "icdatamodel.h"
#include <QDebug>
#include <icsettings.h>
#include <QDebug>
#include <QElapsedTimer>
#include <icobjectrecord.h>
#include <icobjectmacro.h>
#include <icsharedrecord.h>
#include <QQmlEngine>
#include <QMutableListIterator>

ICDataModel::ICDataModel(ICGuiPluginInterface *plugin, QObject* parent)
    : QAbstractItemModel(parent)
    , m_plugin(plugin)
    ,m_db(new ICDb(plugin))
{
    m_depth = m_plugin->setting("gui/tableDepth",DEFAULT_DEPTH).toInt();
    setMatchFunction(&ICDataModel::standartMatching);
}

ICDataModel::~ICDataModel()
{
    qDeleteAll(m_records);
}

void ICDataModel::setPrefilter(QString val)
{
    if(m_prefilter==val)
        return;
    m_prefilter=val;
    m_prefilters = ICConditionSet::fromString(val);
    emit prefilterChanged();
}

void ICDataModel::queryCount(const ICConditionSet& cs)
{
    ICDbSelectRequest selReq;
    selReq.setConditions(m_prefilters && cs);
    selReq.setPrivateTypes(m_targetTypes);
    selReq.setOnlyCount(true);
    execRequest(COMMAND_TYPE_SELECT,&selReq,make_callback(&ICDataModel::handleCountResponse));
}

void ICDataModel::load(const ICConditionSet& cs,int count,int offset,bool toPrepend)
{
    if(m_isLoading)
        return;
    m_isLoading = true;
    m_toPrepend = toPrepend;
    select(cs,count,offset);
    emit loadingChanged();
}

void ICDataModel::select(const ICConditionSet& condSet,int count,int offset)
{
    ICDbSelectRequest selReq;
    selReq.setPrivateTypes(m_targetTypes);
    selReq.setCount(count);
    selReq.setOffset(offset);
    selReq.setConditions(m_prefilters && condSet);
    selReq.setSortByField(m_sortField);
    execRequest(COMMAND_TYPE_SELECT,&selReq);
}

void ICDataModel::insert(const ICRecordSet &recs)
{
    ICDbInsertRequest insReq;
    insReq.inserts = recs;
    execRequest(COMMAND_TYPE_INSERT,&insReq);
}

void ICDataModel::update(const ICDbUpdateRequest &request)
{
    ICDbUpdateRequest updReq = request;
    execRequest(COMMAND_TYPE_UPDATE,&updReq);
}

void ICDataModel::remove(const ICConditionSet &conds)
{
    ICDbRemoveRequest rmReq;
    rmReq.setConditions(conds);
    execRequest(COMMAND_TYPE_DELETE,&rmReq);
}

void ICDataModel::update(ICObjectRecord *rec, bool cascade)
{
    m_db->update(rec,cascade);
}

void ICDataModel::remove(ICObjectRecord *rec, bool cascade)
{
    connect(rec,&ICObjectRecord::removed,this,[&]{auto sndrRec = dynamic_cast<ICObjectRecord*>(sender());m_records.removeOne(sndrRec);sender()->deleteLater();});
    m_db->remove(rec,cascade);
}

void ICDataModel::execRequest(OPERATION_TYPE operation, ICDbRequest *req,ICTMPluginInterface::answerFunc answerCb)
{
    if(m_plugin==nullptr)
    {
        handleError(trUtf8("Не назначен плагин-отправитель"));
        return;
    }
    //дополнить контейнер запроса
    req->setTypeName(typeName());
//    req->setDatabaseName(databaseName());
    //сформировать команду
    ICCommand cmd = m_plugin->newCommand();
    cmd.type = operation;
    cmd.object = operand();
    QDataStream out(&cmd.attrib,QIODevice::WriteOnly);
    req->serialize(out);
    //поставить в очередь обработчиков
//    setWaiter(cmd.id);
    //отправить запрос на выполнение
    m_plugin->request(cmd, answerCb ? answerCb : make_callback(&ICDataModel::handleResponse));
}

void ICDataModel::clear()
{
    beginResetModel();
    qDeleteAll(m_records);
    m_records.clear();
    invalidateHeaders();
    endResetModel();
}

void ICDataModel::internalAppendRecords(const ICRecordSet &recSet)
{
    if (recSet.records.isEmpty())
        return;
    auto recs = adaptRecords(recSet);
    for(auto r: recs)
    {
        auto obj = dynamic_cast<QObject*>(r);
        if(obj)
            QQmlEngine::setObjectOwnership(obj,QQmlEngine::CppOwnership);
    }
    int curCnt = m_records.count();
    m_records += recs;
    if(m_depth != INFINITE_DEPTH)
    {
        // убираем лишнее
        int newCnt = m_records.count();
        if(newCnt > m_depth)
        {
            int delCnt = newCnt - m_depth;
            for(int i=0;i<delCnt;i++)
                delete m_records.takeFirst();
        }
    }
    invalidateHeaders();
    if(curCnt<m_depth)
    {
        beginInsertRows(QModelIndex(),curCnt,m_records.count()-1);
        endInsertRows();
    }
    else
        emit dataChanged(index(0,0,QModelIndex()),index(m_depth-1,0,QModelIndex()));
}

void ICDataModel::prependRecords(const ICRecordSet &recSet)
{
    auto recs = adaptRecords(recSet);
    if(recs.isEmpty())
        return;
    m_records = recs + m_records;
    beginInsertRows(QModelIndex(),0,recs.count()-1);
    endInsertRows();
}

//ICDataModel* ICDataModel::waiter(int cmdId)
//{
//    QMutexLocker lock(&g_handlerMapMutex);
//    Q_UNUSED(lock);
//    if(g_handlerMap.contains(cmdId))
//        return g_handlerMap.value(cmdId);
//    return nullptr;
//}

//void ICDataModel::staticHandleResponse(const ICCommand& cmd)
//{
//    Q_UNUSED(answerer);
//    auto wtr = waiter(cmd.id);
//    if(wtr==nullptr)
//        return;
//    wtr->handleResponse(cmd);
//}

void ICDataModel::handleResponse(quint64 addr, const ICCommand &cmd)
{
    Q_UNUSED(addr);
    if(cmd.status == COMMAND_STATUS_ERROR)
    {
        ICError err(cmd.attrib);
        handleError(err);
    }
    if(cmd.status!=COMMAND_STATUS_OK)
        return;
    QDataStream input(cmd.attrib);
    switch(cmd.type)
    {
    case COMMAND_TYPE_SELECT:
    {
        ICDbResponse selResponse;
        input >> selResponse;
        for(auto r: selResponse.records)
            r->commit();
        if(m_toPrepend)
            prependRecords(selResponse);
        else
            internalAppendRecords(selResponse);
        handleSelect(selResponse);
    }
        break;
    default:
        break;
    }
}

//void ICDataModel::setWaiter(int cmdId)
//{
//    QMutexLocker lock(&g_handlerMapMutex);
//    Q_UNUSED(lock);
//    g_handlerMap[cmdId] = this;
//}

void ICDataModel::handleError(const ICError &err)
{
    setLoading(false);
    auto msg = trUtf8("Ошибка при выполнении запроса.")+err.message();
    qDebug()<<msg;
}

void ICDataModel::handleSelect(const ICDbResponse &response)
{
    Q_UNUSED(response);
    emit loaded(response.records.count());
    setLoading(false);
}

QModelIndex ICDataModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if(row < m_records.count())
        return createIndex(row,column,m_records.value(row));
    return QModelIndex();
}

int ICDataModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_records.count();
}

int ICDataModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_headers.count();
}

QVariant ICDataModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    ICRecord* rec = reinterpret_cast<ICRecord*>(index.internalPointer());
    switch (role)
    {
    case Qt::DisplayRole:
    {
        int col = index.column();
        if(col>=m_fieldNames.count())
            break;
        return rec->value(m_fieldNames[col]);
    }
    case RecordRole:
    {
        auto objRec = dynamic_cast<ICObjectRecord*>(rec);
        if(!objRec)
        {
            auto shRec = dynamic_cast<ICSharedRecord*>(rec);
            if(shRec!=nullptr)
            {
                auto rec = shRec->get();
                if(rec)
                    objRec = dynamic_cast<ICObjectRecord*>(rec);
            }
        }
        return objRec ? QVariant::fromValue(objRec) : QVariant();
    }
    case ValueRole:
    {
        QVariantList vals;
        for(auto f: m_fieldNames)
        {
            if(rec->contains(f))
            {
                auto val = rec->value(f);
                if(val.type()==QVariant::DateTime)
                    val = val.toDateTime().toString("dd.MM.yyyy HH:mm:ss:z");
                vals << val;
            }
            else
                vals<<QVariant();
        }
        return vals;
    }
    default:
        break;
    }
    return QVariant();
}

QVariant ICDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation == Qt::Horizontal || section>=m_headers.count())
        return QVariant();
    switch (role)
    {
    case Qt::DisplayRole:
        return m_headers[section];
    default:
        break;
    }
    return QVariant();
}

QHash<int,QByteArray> ICDataModel::roleNames() const
{
    auto result = QAbstractItemModel::roleNames();
    result[ValueRole] = "values";
    result[RecordRole] = "record";
    return result;
}

void ICDataModel::handleSync(ICCommand &cmd)
{
    switch(cmd.type)
    {
    case COMMAND_TYPE_INSERT:
    {
        ICDbInsertRequest ireq;
        QDataStream str(&cmd.attrib, QIODevice::ReadOnly);
        ireq.deserialize(str);
        for(auto r: ireq.inserts.records)
            r->commit();
        if (ireq.typeName() == typeName())
            appendRecords(ireq.inserts);
        break;
    }
    case COMMAND_TYPE_UPDATE:
    {
        ICDbUpdateRequest ireq;
        QDataStream str(&cmd.attrib, QIODevice::ReadOnly);
        ireq.deserialize(str);
        if (ireq.typeName() == typeName())
            updateRecords(ireq);
        break;
    }
    case COMMAND_TYPE_DELETE:
    {
        ICDbRemoveRequest ireq;
        QDataStream str(&cmd.attrib, QIODevice::ReadOnly);
        ireq.deserialize(str);
        if (ireq.typeName() == typeName())
            removeRecords(ireq);
        break;
    }
    default:
        ;
    }
}

void ICDataModel::appendRecords(const ICRecordSet &recs)
{
    ICRecordSet rs=recs;
    QMutableListIterator<ICRecord*> it(rs.records);
    while(it.hasNext())
    {
        auto r = it.next();
        if(!matching(r,m_prefilters))
        {
//            auto o = dynamic_cast<QObject*>(r);
//            if(o)
//                deleteLater();
//            else
                delete r;
            it.remove();
        }
    }
    internalAppendRecords(rs);
    m_totalCount+=rs.records.count();
    emit appended();
}

void ICDataModel::updateRecords(const ICDbUpdateRequest &updateReq)
{
    // вытащить condition request
    ICConditionSet conditions = updateReq.conditions();
    // передать его в find
    QModelIndexList list = find(conditions);
    // сигнализировать об изменениях
    for(auto i:list)
    {
        emit dataChanged(i,i);
    }
}

void ICDataModel::removeRecords(const ICDbRemoveRequest &removeReq)
{
    ICConditionSet conds = removeReq.conditions();

    QMutableListIterator<ICRecord*> i(m_records);
    int rowId = 0;
    while (i.hasNext())
    {
        auto record = i.next();
        if(matching(record, conds))
        {
            beginRemoveRows(QModelIndex(), rowId, rowId);
            QObject * obj = dynamic_cast<QObject*>(i.value());
            if (obj)
                obj->deleteLater();
            else
                delete i.value();
            i.remove();
            m_totalCount--;
            endRemoveRows();
        }
        rowId++;
    }
    emit rowsDeleted();
}

QStringList ICDataModel::headers() const
{
    return fieldNames();
}

QStringList ICDataModel::fieldNames() const
{
    //стандартная реализация: вернуть список имен атрибутов 1-ой записи, если она есть
    if(m_records.isEmpty())
        return QStringList();
    return m_records.first()->names();
}

void ICDataModel::invalidateHeaders()
{
    if(m_fieldNames.isEmpty())
        m_fieldNames = fieldNames();
    if(!m_headers.isEmpty())
        return;
    m_headers= headers();
//    beginInsertColumns(QModelIndex(),0,m_headers.count()-1);
//    endInsertColumns();
    emit headerNamesChanged();
}

void ICDataModel::handleCountResponse(quint64, ICCommand &cmd)
{
    if(cmd.status == COMMAND_STATUS_ERROR)
    {
        handleError(QString(cmd.attrib));
        return;
    }
    if(cmd.status!=COMMAND_STATUS_OK)
    {
        handleError(QString("handleCountRespose() has unexpected status"));
        return;
    }
    ICDbResponse resp;
    QDataStream ds(cmd.attrib);
    ds >> resp;
    m_totalCount = 0;
    for(ICRecord* rec: resp.records)
        m_totalCount+=rec->value("count").toInt();
    emit totalCountChanged();
}

QModelIndexList ICDataModel::find(const ICConditionSet &conds) const
{
    if(conds.conditions().isEmpty())
        return QModelIndexList();
    QModelIndexList list;
    int cnt =m_records.count();
    for(int i=0;i<cnt;i++)
    {
        if(matching(m_records[i], conds))
            list << index(i,0,QModelIndex());
    }
    return list;
}

bool ICDataModel::standartMatching(const ICRecord *rec, const ICConditionSet &conds)
{
    bool result = true;
    ICLogicalPair::LogicalOperator op = ICLogicalPair::LO_NONE;
    for(auto cond: conds.conditions())
    {
        auto fname = cond.cond.field();
        if(!rec->contains(fname))
            return false;
        auto curVal = rec->value(fname);
        QVariant expVal = cond.cond.value();
        auto verify = [&](bool exp) mutable{
            if(op == ICLogicalPair::LO_AND)
                result &= exp;
            else if(op == ICLogicalPair::LO_OR)
                result |= exp;
            else
                result = exp;
        };
        switch(cond.cond.compareOperator())
        {
        case ICCondition::Less:
            verify(curVal<expVal);
            break;
        case ICCondition::LessOrEquals:
            verify(curVal<=expVal);
            break;
        case ICCondition::Greater:
            verify(curVal>expVal);
            break;
        case ICCondition::GreaterOrEquals:
            verify(curVal>=expVal);
            break;
        case ICCondition::Equals:
            verify(curVal==expVal);
            break;
        case ICCondition::NotEquals:
            verify(curVal!=expVal);
            break;
        case ICCondition::Like:
            if(expVal.type()==QVariant::String)
            {
                QRegExp rx(expVal.toString(),Qt::CaseInsensitive);
                verify(rx.indexIn(curVal.toString())!=-1);
            }
            else
                verify(curVal==expVal);
            break;
        default:
            break;
        }
        op = cond.op;
    }
    return result;
}
