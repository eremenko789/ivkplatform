#ifndef SCREENKBINPUT_H
#define SCREENKBINPUT_H

#include <QObject>

class ScreenKbInput : public QObject
{
    Q_OBJECT
public:
    ScreenKbInput(){m_visible=false;m_defLayout=0;}

    Q_PROPERTY(bool visible READ visible WRITE setVisible NOTIFY visibleChanged)
    Q_PROPERTY(int defaultLayout READ defaultLayout WRITE setDefaultLayout NOTIFY defaultLayoutChanged)

    inline bool visible(){return m_visible;}
    inline int defaultLayout(){ return m_defLayout;}
signals:
    void accepted();
    void visibleChanged();
    void charAppended(QString ch);
    void charRemoved();
    void defaultLayoutChanged();
public slots:
    void setVisible(bool val){if(m_visible!=val){m_visible = val;emit visibleChanged();}}
    void setDefaultLayout(int val){if(m_defLayout==val) return;m_defLayout = val;emit defaultLayoutChanged();}
    void accept(){emit accepted();}
private:
    int m_defLayout;
    bool m_visible;
};

#endif // SCREENKBINPUT_H
