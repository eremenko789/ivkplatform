#include "icmodelstatistics.h"

ICModelStatistics::ICModelStatistics(QObject *parent) :
    ICObject(parent)
{
}

void ICModelStatistics::setModel(QObject *val)
{
    auto m = qobject_cast<QAbstractItemModel*>(val);
    if(!m || m_model==m)
        return;
    m_model=m;
    connect(m_model,&QAbstractItemModel::rowsInserted,this,&ICModelStatistics::invalidate);
    connect(m_model,&QAbstractItemModel::rowsRemoved,this,&ICModelStatistics::invalidate);
    connect(m_model,&QAbstractItemModel::layoutChanged,this,&ICModelStatistics::invalidate);
    connect(m_model,&QAbstractItemModel::dataChanged,this,&ICModelStatistics::invalidate);
    connect(this,&ICModelStatistics::roleChanged,this,&ICModelStatistics::invalidate);
    emit modelChanged();
    invalidate();
}

void ICModelStatistics::invalidate()
{
    if(m_model->roleNames().key(m_role.toUtf8(),-1)==-1)
        return;
    m_min = select([=](double x1,double x2){return qMin(x1,x2);});
    m_max = select([=](double x1,double x2){return qMax(x1,x2);});
    emit statChanged();
}

double ICModelStatistics::valueAt(int index) const
{
    if(!m_model)
        return 0;
    if(m_model->rowCount()==0)
        return 0;
    int r = m_model->roleNames().key(role().toUtf8(),-1);
    if(r==-1)
    {
        IC_ERROR("Ошибка при получении статистики: не найдена роль");
        return 0;
    }
    if(index<0 || index>=m_model->rowCount())
    {
        IC_ERROR("Ошибка при получении статистики: индекс элемента некорректен");
        return 0;
    }
    return m_model->data(m_model->index(index,0),r).toDouble();
}

double ICModelStatistics::select(SelectFunction func) const
{
    if(!m_model)
        return 0;
    int cnt = m_model->rowCount();
    if(cnt==0)
        return 0;
    double res = valueAt(0);
    for(int i=1;i<cnt;i++)
        res = func(res,valueAt(i));
    return res;
}
