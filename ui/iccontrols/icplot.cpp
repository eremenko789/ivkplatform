#include "icplot.h"
#include <QPainter>
#include <icdatamodel.h>

ICPlot::ICPlot(QQuickItem *parent) :
    QQuickPaintedItem(parent)
{
}

void ICPlot::setModel(QObject *val)
{
    auto model = qobject_cast<QAbstractItemModel*>(val);
    if(!model || m_model==model)
        return;
    m_model=model;
    connect(m_model,&QAbstractItemModel::dataChanged,this,&ICPlot::invalidate);
    connect(m_model,&QAbstractItemModel::rowsInserted,this,&ICPlot::invalidate);
    connect(m_model,&QAbstractItemModel::rowsRemoved,this,&ICPlot::invalidate);
    connect(m_model,&QAbstractItemModel::layoutChanged,this,&ICPlot::invalidate);
    emit modelChanged();
}

void ICPlot::paint(QPainter *painter)
{
    if((!m_model || m_model->rowCount()==0) && m_vertex.isEmpty())
        return;
    QPainterPath path;
    double dx = 0;
    double firstX,lastX;
    int xr,yr;
    if(m_model)
    {
        xr = m_model->roleNames().key(xrole().toUtf8(),-1);
        if(xr==-1)
        {
            qDebug()<<"ICPlot: unknown x role";
            return;
        }
        yr = m_model->roleNames().key(yrole().toUtf8(),-1);
        if(yr==-1)
        {
            qDebug()<<"ICPlot: unknown y role";
            return;
        }
//        if(m_autoShift)
//        {
            bool ok = false;
            firstX = m_model->data(m_model->index(0,0),xr).toDouble(&ok);
            if(!ok)
                return;
            lastX = m_model->data(m_model->index(m_model->rowCount()-1,0),yr).toDouble(&ok);
            if(!ok)
                return;
//        }
    }
    else /*if(m_autoShift)*/
    {
        auto firstObj =  m_vertex.first().value<QObject*>();
        auto lastObj = m_vertex.last().value<QObject*>();
        Q_ASSERT(lastObj);
        firstX = firstObj->property(xrole().toUtf8().constData()).toDouble();
        lastX = lastObj->property(xrole().toUtf8().constData()).toDouble();
    }
    if(m_autoShift)
    {
        if(lastX>maxX())
            dx = lastX-maxX();
        if(firstX>dx)
            dx= firstX;
    }
    else
        dx = firstX;
    bool started = false;
    auto drawSegment = [&](double ox,double oy){
        auto rx = (ox - dx)/maxX();
        if(rx<0)
            return;
        auto ry = (oy-minY())/(maxY()-minY());
        auto x = width()*rx;
        auto y = height()*(1-ry);
        if(!started)
        {
            path.moveTo(x,y);
            started = true;
        }
        else
            path.lineTo(x,y);
    };
    if(m_model)
    {
        for(int i=0;i<m_model->rowCount();i++)
        {
            auto ox = m_model->data(m_model->index(i,0),xr).toDouble();
            auto oy = m_model->data(m_model->index(i,0),yr).toDouble();
            drawSegment(ox,oy);
        }
    }
    else
    {
        for(auto v: m_vertex)
        {
            auto vert = v.value<QObject*>();
            if(!vert)
                return;
            auto ox = vert->property(xrole().toUtf8().constData()).toDouble();
            auto oy = vert->property(yrole().toUtf8().constData()).toDouble();
            drawSegment(ox,oy);
        }
    }
    painter->setRenderHint(QPainter::Antialiasing);
    painter->strokePath(path,QPen(QBrush(color()),2));
}
