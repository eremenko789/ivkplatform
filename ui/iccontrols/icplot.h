#ifndef ICPLOT_H
#define ICPLOT_H

#include <QQuickPaintedItem>
#include <QAbstractItemModel>

/**
 * @brief Класс, реализующий QML-контрол для рисования графиков
 *
 * @defgroup controls Библиотек QML-компонентов
 * @ingroup controls
 */
class ICPlot : public QQuickPaintedItem
{
    Q_OBJECT
public:
    explicit ICPlot(QQuickItem *parent = 0);
    Q_PROPERTY(QObject* model READ model WRITE setModel NOTIFY modelChanged) ///< модель
    /**
    * @brief Возвращает модель
    */
    inline QObject* model() const{return m_model;}
    /**
    * @brief Устанавливает модель
    * @param val новое значение
    */
    void setModel(QObject* val);

    Q_PROPERTY(QVariantList vertex READ vertex WRITE setVertex NOTIFY vertexChanged) ///< вершины
    /**
    * @brief Возвращает вершины
    */
    inline QVariantList vertex() const{return m_vertex;}
    /**
    * @brief Устанавливает вершины
    * @param val новое значение
    */
    inline void setVertex(QVariantList val){if(m_vertex==val) return; m_vertex=val; emit vertexChanged();}

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged) ///< цвет линии
    /**
    * @brief Возвращает цвет линии
    */
    inline QColor color() const{return m_color;}
    /**
    * @brief Устанавливает цвет линии
    * @param val новое значение
    */
    inline void setColor(QColor val){if(m_color==val) return; m_color=val; emit colorChanged();}

    Q_PROPERTY(QString xrole READ xrole WRITE setXrole NOTIFY xroleChanged) ///< роль X
    /**
    * @brief Возвращает роль X
    */
    inline QString xrole() const{return m_xrole;}
    /**
    * @brief Устанавливает роль X
    * @param val новое значение
    */
    inline void setXrole(QString val){if(m_xrole==val) return; m_xrole=val; emit xroleChanged();}

    Q_PROPERTY(QString yrole READ yrole WRITE setYrole NOTIFY yroleChanged) ///< роль Y
    /**
    * @brief Возвращает роль Y
    */
    inline QString yrole() const{return m_yrole;}
    /**
    * @brief Устанавливает роль Y
    * @param val новое значение
    */
    inline void setYrole(QString val){if(m_yrole==val) return; m_yrole=val; emit yroleChanged();}

    Q_PROPERTY(double maxX READ maxX WRITE setMaxX NOTIFY maxXChanged) ///< максимум X
    /**
    * @brief Возвращает максимум X
    */
    inline double maxX() const{return m_maxX;}
    /**
    * @brief Устанавливает максимум X
    * @param val новое значение
    */
    inline void setMaxX(double val){if(m_maxX==val) return; m_maxX=val; emit maxXChanged();}

    Q_PROPERTY(double maxY READ maxY WRITE setMaxY NOTIFY maxYChanged) ///< максимум Y
    /**
    * @brief Возвращает максимум Y
    */
    inline double maxY() const{return m_maxY;}
    /**
    * @brief Устанавливает максимум Y
    * @param val новое значение
    */
    inline void setMaxY(double val){if(m_maxY==val) return; m_maxY=val; emit maxYChanged();}

    Q_PROPERTY(double minY READ minY WRITE setMinY NOTIFY minYChanged) ///< минимум Y
    /**
    * @brief Возвращает значение свойства "минимум Y"
    */
    inline double minY() const{return m_minY;}
    /**
    * @brief Устанавливает значение свойства "минимум Y"
    * @param val новое значение
    */
    inline void setMinY(double val){if(m_minY==val) return; m_minY=val; emit minYChanged();}



    Q_PROPERTY(bool autoShift READ autoShift WRITE setAutoShift NOTIFY autoShiftChanged) ///< авто-сдвиг
    /**
    * @brief Возвращает авто-сдвиг
    */
    inline bool autoShift() const{return m_autoShift;}
    /**
    * @brief Устанавливает авто-сдвиг
    * @param val новое значение
    */
    inline void setAutoShift(bool val){if(m_autoShift==val) return; m_autoShift=val; emit autoShiftChanged();}

    // QQuickPaintedItem interface
    void paint(QPainter *painter);
signals:
    void vertexChanged();   ///< Сообщает об изменении значения свойства "вершины"
    void colorChanged();   ///< Сообщает об изменении значения свойства "цвет линии"
    void xroleChanged();   ///< Сообщает об изменении значения свойства "роль X"
    void yroleChanged();   ///< Сообщает об изменении значения свойства "роль Y"
    void maxXChanged();   ///< Сообщает об изменении значения свойства "максимум X"
    void maxYChanged();   ///< Сообщает об изменении значения свойства "максимум Y"
    void modelChanged();   ///< Сообщает об изменении значения свойства "модель"
    void autoShiftChanged();   ///< Сообщает об изменении значения свойства "авто-сдвиг"
    void minYChanged();   ///< Сообщает об изменении значения свойства "минимум Y"
public slots:
    void invalidate(){update();}
private:
    QVariantList m_vertex;
    QColor m_color = Qt::red;
    QString m_xrole = "x";
    QString m_yrole = "y";
    double m_maxX=100;
    double m_maxY=100;
    QAbstractItemModel* m_model=nullptr;
    bool m_autoShift = true;
    double m_minY=0;
};

#endif // ICPLOT_H
