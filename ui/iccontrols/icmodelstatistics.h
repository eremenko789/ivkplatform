#ifndef ICMODELSTATISTICS_H
#define ICMODELSTATISTICS_H

#include <QAbstractItemModel>
#include <icobject.h>
#include <functional>

/**
 * @brief QML-интерфейс для доступа к статистике модели.
 *
 * @ingroup gui dataaccess
 */
class ICModelStatistics : public ICObject
{
    Q_OBJECT

    typedef std::function<double(double,double)> SelectFunction;
public:
    explicit ICModelStatistics(QObject *parent = 0);

    Q_PROPERTY(QObject* model READ model WRITE setModel NOTIFY modelChanged) ///< модель данных
    /**
    * @brief Возвращает модель данных
    */
    inline QObject* model() const{return m_model;}
    /**
    * @brief Устанавливает модель данных
    * @param val новое значение
    */
    void setModel(QObject* val);

    Q_PROPERTY(QString role READ role WRITE setRole NOTIFY roleChanged) ///< роль
    /**
    * @brief Возвращает роль
    */
    inline QString role() const{return m_role;}
    /**
    * @brief Устанавливает роль
    * @param val новое значение
    */
    inline void setRole(QString val){if(m_role==val) return; m_role=val; emit roleChanged();}

    Q_PROPERTY(double first READ first NOTIFY statChanged) ///< значение начального элемента
    /**
    * @brief Возвращает значение начального элемента
    */
    inline double first() const{return valueAt(0);}


    Q_PROPERTY(double last READ last NOTIFY statChanged) ///< значение конечного элемента
    /**
    * @brief Возвращает значение конечного элемента
    */
    double last() const{return m_model ? valueAt(m_model->rowCount()-1) : 0;}

    Q_PROPERTY(double max READ max NOTIFY statChanged) ///< максимум
    /**
    * @brief Возвращает максимум
    */
    inline double max() const{return m_max;}

    Q_PROPERTY(double min READ min NOTIFY statChanged) ///< минимум
    /**
    * @brief Возвращает минимум
    */
    inline double min() const{return m_min;}
signals:
    void modelChanged();   ///< Сообщает об изменении значения свойства "модель данных"
    void roleChanged();   ///< Сообщает об изменении значения свойства "роль"
    void statChanged();   ///< Сообщает об изменении статистики
public slots:
    /**
     * @brief Обновить статистику
     */
    void invalidate();
private:
    double valueAt(int index) const;
    double select(SelectFunction func) const;
    QAbstractItemModel *m_model=nullptr;
    QString m_role;
    double m_max=0;
    double m_min=0;
};

#endif // ICMODELSTATISTICS_H
