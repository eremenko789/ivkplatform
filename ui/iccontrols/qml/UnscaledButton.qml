import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import QtQuick.Layouts 1.0

Button {
    style: ButtonStyle {
        id: s
        label: Item{
            id: lbl
            Row{
                spacing: 5
                anchors.centerIn: parent
                Image {
                    id: img
                    asynchronous: true
                    source: s.control.iconSource
                }
                Text{
                    text: s.control.text
                    anchors.verticalCenter: img.verticalCenter
                    width: Math.min(contentWidth,lbl.width-img.width-5)
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                }
            }
        }
    }
}
