#ifndef SCREENKEYBOARDMODEL_H
#define SCREENKEYBOARDMODEL_H

#include <QtQuick/QQuickItem>
#include "screenkeyboardattachedtype.h"

class ScreenKeyboardModel : public QQuickItem
{
    Q_OBJECT
public:
    ScreenKeyboardModel();

    Q_PROPERTY(QStringList layout READ layout WRITE setlayout NOTIFY layoutChanged)

    inline QStringList layout() const{ return m_layout; }

    static ScreenKeyboardAttachedType *qmlAttachedProperties(QObject *p)
    {
        if(!m_attach)
        {
            m_attach = new ScreenKeyboardAttachedType(p);
        }
        return m_attach;
    }

public slots:
    void switchLayout(int index);
    void setlayout(QStringList arg)
    {
        if (m_layout != arg) {
            m_layout = arg;
            emit layoutChanged();
        }
    }

signals:
    void layoutChanged();
    void visibleChanged();

private:
    QStringList m_layouts;
    QStringList m_layout;
    static ScreenKeyboardAttachedType* m_attach;
};


QML_DECLARE_TYPEINFO(ScreenKeyboardModel, QML_HAS_ATTACHED_PROPERTIES)
#endif // SCREENKEYBOARDMODEL_H
