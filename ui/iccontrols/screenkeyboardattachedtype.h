#ifndef SCREENKEYBOARDATTACHEDTYPE_H
#define SCREENKEYBOARDATTACHEDTYPE_H

#include "screenkbinput.h"
#include "screenkboutput.h"

class ScreenKeyboardAttachedType : public QObject
{
    Q_OBJECT
public:
    ScreenKeyboardAttachedType(QObject* parent);

    Q_PROPERTY(ScreenKbInput* input READ input WRITE setInput NOTIFY inputChanged)
    Q_PROPERTY(ScreenKbOutput* output READ output WRITE setOutput NOTIFY outputChanged)

    inline ScreenKbInput* input(){return m_input;}
    inline ScreenKbOutput* output(){return m_output;}
signals:
    void inputChanged();
    void outputChanged();
public slots:
    void setInput(ScreenKbInput* input);
    void setOutput(ScreenKbOutput* output);
private:
    void bind();

    ScreenKbInput* m_input;
    ScreenKbOutput* m_output;
};

#endif // SCREENKEYBOARDATTACHEDTYPE_H
