#include "ictime.h"

ICTime::ICTime(QObject *parent) :
    ICObject(parent)
{
    connect(this,&ICTime::timeChanged,this,&ICTime::textChanged);
    connect(this,&ICTime::formatChanged,this,&ICTime::textChanged);
}
