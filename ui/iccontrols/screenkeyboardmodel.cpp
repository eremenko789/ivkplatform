#include "screenkeyboardmodel.h"
#include <icguimacro.h>

ScreenKeyboardAttachedType* ScreenKeyboardModel::m_attach=0;

ScreenKeyboardModel::ScreenKeyboardModel()
{
    m_layouts = QStringList({
        "qwertyuiopasdfghjklzxcvbnm",
        "QWERTYUIOPASDFGHJKLZXCVBNM",
        "йцукенгшщзхъфывапролджэячсмитьбю",
        "ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ",
        "0123456789.-",
        ".,:;?!-+='\\|/#%&@*()[]{}'\""
    });
    switchLayout(0);
}

void ScreenKeyboardModel::switchLayout(int index)
{
    if(index >= m_layouts.count())
        return;
    QString l = m_layouts[index];
    m_layout = l.split("",QString::SkipEmptyParts);
    emit layoutChanged();
}
