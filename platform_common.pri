#variables
PLATFORM_DIR = $$PWD
CORE_INCLUDE = $$PLATFORM_DIR/core/include

#directories
INCLUDEPATH += $$CORE_INCLUDE
DESTDIR = $$PLATFORM_DIR/../../lib

#qmake flags
QMAKE_CXXFLAGS += -std=c++0x
QMAKE_LFLAGS += -Wl,--no-undefined -Wall -Wextra

#defines
greaterThan(QT_MAJOR_VERSION, 4) {
  DEFINES += HAVE_QT5
}

CODECFORTR = UTF-8
CODECFORSRC = UTF-8



