#ifndef ICROUTINGCENTER_H
#define ICROUTINGCENTER_H

#include "icobject.h"
#include "icmessages.h"
#include "icplugintypeinfo.h"
#include "QMutex"
#include "icnode.h"

namespace icrouting {

class ICRoutingCenter: public ICObject
{
    Q_OBJECT
public:


    struct CollisionStatus
    {
        QStringList collidingNames;
        QList<QPair<QString, QString> > collidingHashes;
    };

    void addNode(ICNode*);
    void removeNode(quint32 nodeID);
    void modifyNode(quint32 nodeID, QString pluginName);
    QStringList nodePlugins(quint32 nodeID);


    void route(quint64 src, quint64 dest, const QByteArray& message);

   CollisionStatus checkNodeNameOrHashCollisions(QString nodeName);

    QString thisNodeName()const{return m_thisNodeName;}
    quint32 thisNodeID()const{return m_thisNodeID;}

    //функции обслуживания локальных запросов плагинов

    QList<quint64> pluginIDs(QString pluginName); ///< Получает идентификаторы плагинов с определенным именем со всех нодов.
    QList<quint64> pluginIDs();                   ///< Получает идентификаторы всех плагинов со всех нодов.
    QString nodeName(quint64 pluginID);           ///< Получает имя нода по идентификатору плагина.
    QString nodeName(quint32 nodeID);             ///< Получает имя нода по идентификатору нода.
    QString pluginName(quint64 pluginID);         ///< Получает имя плагина по идентификатору плагина.
    QString pluginName(quint32 pluginTypeId);     ///< Получает имя плагина по идентификатору типа плагина.


private:
    QHash<quint32, ICNode*> m_routeTable;

    QMutex m_tableMutex;

    quint32 m_thisNodeID;

    QString m_thisNodeName;
};

}

#endif // ICROUTINGCENTER_H
